<?php

require_once "../../controllers/kbControllers.php";
require_once "../../models/kbModels.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxKB{

	public function mostrarKBAjax(){

		$respuesta = KBController::mostrarKBController();
		print json_encode($respuesta);
	}


	public function referenciaKBAjax(){

		$respuesta = KBController::referenciaController();
		print json_encode($respuesta);
	}


	public function guardarKB(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = KBController::guardarKBController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "KB registrada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al registrar KB";
		}

		echo json_encode($salida);
	}

	public function getIDKBAjax(){

		$respuesta = KBController::getIDController();
		print json_encode($respuesta);
	}

	public function editarKB(){
		
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = KBController::editarKBController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Categoria actualizada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar categoria";
		}

		echo json_encode($salida);

	}

	public function desactivarKB(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = KBController::desactivarKBController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "KB eliminada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al eliminar categoria";
		}		

		echo json_encode($salida);
	}


	public function guardarPDF(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = KBController::guardarPDFController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "El PDF se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al subir PDF";
		}

		echo json_encode($salida);
	}


	/*
	public function categoriaPorID(){

		$respuesta = CategoriaController::mostrarCategoriaIDController();
		print json_encode($respuesta);	
	}


	


	public function editarCategoria(){
		
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CategoriaController::editarCategoriaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Categoria actualizada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar categoria";
		}

		echo json_encode($salida);

	}

*/	

}

if($opcion == 'pdf'){
	$pdf = new AjaxKB();
	$pdf -> guardarPDF();
}

if($opcion == 'mostrarkb'){
	$kb = new AjaxKB();
	$kb -> mostrarKBAjax();
}

if($opcion == 'eliminar'){
	$kb = new AjaxKB();
	$kb -> desactivarKB();
}

if($opcion == 'editarkb'){
	$editar = new AjaxKB();
	$editar -> editarKB();
}

if($opcion == 'getID'){
	$id = new AjaxKB();
	$id -> getIDKBAjax();
}

if($opcion == 'guardar'){
	$guardar = new AjaxKB();
	$guardar -> guardarKB();
}

if($opcion == 'referencia'){
	$kb = new AjaxKB();
	$kb -> referenciaKBAjax();
}
