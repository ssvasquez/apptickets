<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Recordatorio </title>
	<link rel="stylesheet" href="views/css/all.min.css">
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link href="views/css/bootstrap.css" rel="stylesheet">	
	<link href="views/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/responsive.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link rel="stylesheet" href="views/css/plugins/fileinput.min.css">
	<link rel="stylesheet" href="views/css/plugins/theme.min.css">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header  d-flex justify-content-between">
									<span><i class="fa fa-database"></i> Base de conocimiento <span class="ml-3 text-secondary font-italic"> Para agregar un archivo al <strong>KB</strong> es necesario completar el registro y luego clic en: <strong>Acciones/Agregar Manual</strong></span></span>
									<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#registrarKB" id="btnagregarKB">
										<i class="fa fa-plus-circle"></i> Base de conocimiento
									</button>
								</div>
								<div class="card-block">
									<div class="removeMessages"></div>
								    <table id="kb" class="table table-striped table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>No</th>
												<th>ID Ticket</th>
												<th>Subcategoria</th>
												<th>Caso</th>
												<th>Solucion</th>
												<th>Archivo</th>
												<th>Link</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>


		<!--Modal Registro-->
		<div class="modal fade" id="registrarKB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-primary" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle"></i> Crear base de conocimiento</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="post" id="frmregistrarKB" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="messages"></div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="input-group" id="referencia"> 
											<span class="input-group-addon">ID Ticket</span>
											<input type="text" id="regID" name="regID" class="form-control" placeholder="Referencia ticket">
											<button type="button" id="heredar" class="btn btn-primary btn-sm"><i class="fa fa-angle-double-right"></i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Requerimiento</span>
											<select class="form-control" name="regCategoria" id="regCategoria" required>
												<option value="">Seleccione...</option>
												<?php
													$categoria = new CategoriaController();
													$categoria -> categoriaSeleccionController();
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<div class="input-group" id="idSubcategoria_">
											<span class="input-group-addon">Subcategoria</span>
											<select class="form-control" name="regSubcategoria" id="regSubcategoria" required>
											</select>
										</div>
									</div>
								</div>
							</div>
								
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Caso</span>
									<input type="text" id="regCaso" name="regCaso" class="form-control" placeholder="Titulo tema" required>
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Descripcion Caso</span>
									<textarea rows="2" id="regDescripcion" name="regDescripcion" class="form-control" placeholder="Descripcion" required></textarea>									
								</div>
							</div>
									
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Solucion</span>
									<textarea rows="5" id="regSolucion" name="regSolucion" class="form-control" placeholder="Descripcion" required></textarea>
								</div>
							</div>
							
							<!--
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Archivo (Manual) * Solo PDF</span>
									<input type="text" id="regArchivo" name="regArchivo" class="form-control" placeholder="Link de referencia">
								</div>
							</div>
							-->

							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">link</span>
									<input type="text" id="regLink" name="regLink" class="form-control" placeholder="Link de referencia">
								</div>
							</div>
							
							<!--
							<hr>
							<label class="input-group-addon">Archivo(s)</label>
							<input class="file-loading" id="imagen" type="file" multiple name="image[]">
							<div id="errorBlock" class="help-block"></div>--> 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!--Fin Modal -->


		<div class="modal fade" id="modaleditarKB" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-primary" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Editar base de conocimiento</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="" id="frmeditarKB">
						<div class="modal-body">
							<div class="edit-messages"></div>
							
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Solucion</span>
									<textarea rows="15" id="editSolucion" name="editSolucion" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer modaleditarKB">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div>


		<div id="registrarPDF" class="modal fade">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<p class="lead">PDF</p>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="frmRegistrarPDF" enctype="multipart/form-data">
							<div class="row">
								<div class="col-12">
									<input type="hidden" id="idKBPDF" name="idKBPDF">
									<!--<input type="text" id="marcaProductoPDF" name="marcaProductoPDF">-->
									<div class="form-group">
										<label for="pdf" class="m-0">PDF del KB*</label>
										<input type="file" name="pdf" class="form-control" id="pdfKB">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="text-right">
									<button type="submit" class="btn btn-primary"> Subir PDF	</button>
								</div>
							</div>
						</form>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		




	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/jquery.dataTables.min.js"></script>	
	<script src="views/js/libs/dataTables.bootstrap4.min.js"></script>
	<script src="views/js/libs/dataTables.responsive.min.js"></script>
	<script src="views/js/libs/responsive.bootstrap4.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>

	<script src="views/js/libs/fileinput.min.js"></script>
	<script src="views/js/libs/theme.js"></script>
	<script src="views/js/libs/themefas.js"></script>
	<script src="views/js/libs/locales/es.js"></script>
	

	<script src="views/js/app.js"></script>
	<script src="views/js/kb.js"></script>

</body>
</html>

