<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Iniciar Sesion</title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">

</head>
<body class="app flex-row align-items-center">
	<div class="container">
		<div class="row justify-content-center ">
			<div class="col-md-8 align-items-end">
				<form action="" id="formIngreso" method="POST" onsubmit="return validarIngreso()">
					<div class="card-group mb-0">
						<div class="card p-2">
							<div class="card-block" id="iniciarSesion">
								<h2>Iniciar sesion</h2>
								<p class="text-muted">Inicia sesion con tu cuenta</p>
			
								<div class="input-group mb-1">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
									<input type="text" name="usuarioIngreso" id="usuarioIngreso" class="form-control" placeholder="Usuario">
								</div>
								<div class="input-group mb-2">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" name="passwordIngreso" id="passwordIngreso" class="form-control" placeholder="Contraseña">
								</div>

								<?php
									$ingreso = new Ingreso();
									$ingreso -> ingresoController();
								?>

								<div class="row">
									<div class="col-6 text-center">
										<button type="submit" class="btn btn-primary btn-block formIngreso">Iniciar Sesion</button>
										
									</div>	
								</div>
							</div>
						</div>
						<div class="card card-inverse card-primary py-3 hidden-md-down">
							<div class="card-block text-center">
								<div>	
									<p>Ingresa tu NOMBRE y CONTRASEÑA para acceder a este sistema.</p>
								</div>
							</div>
							<div class="text-center">
								<a href="http://www.cdcmx.com/">
									<img src="views/images/cdc_inicio.png" class="rounded" alt="...">
								</a>
							</div>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/app.js"></script>
	<script src="views/js/validarIngreso.js"></script>
</body>
</html>