<?php
if(!isset($_SESSION)){
session_start();
}

if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Inicio </title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">

	<style>
		.progress{
			background-color:#e9ecef;
		}
	</style>
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn mt-1">
					<?php 
					if($_SESSION["rol"] == 1){
					?>
					<div class="row">
						<div class="col-md-6 col-lg-6 col-xl-3">
							<div class="mini-stat clearfix">
								<span class="mini-stat-icon bg-info mr-0"><i class="fas fa-ticket-alt"></i></span>
								<div class="mini-stat-info float-right">
									<span>
										<?php
											$tickets = new GraficaController();
											$tickets -> contarTicketsController();
										?>
									</span> TICKETS
								</div>
							</div>
						</div>

						<div class="col-md-6 col-lg-6 col-xl-3">
							<div class="mini-stat clearfix">
								<span class="mini-stat-icon bg-info mr-0"><i class="fa fa-user"></i></span>
								<div class="mini-stat-info float-right">
									<span>
										<?php
											$usuario = new GraficaController();
											$usuario -> contarUsuarioController();
										?>
									</span> USUARIOS
								</div>
							</div>
						</div>

						<div class="col-md-6 col-lg-6 col-xl-3">
							<div class="mini-stat clearfix">
								<span class="mini-stat-icon bg-info mr-0"><i class="fa fa-list-alt"></i></span>
								<div class="mini-stat-info float-right">
									<span>
										<?php
											$categoria = new GraficaController();
											$categoria -> contarCategoriaController();
										?>
									</span> CATEGORIAS
								</div>
							</div>
						</div>

						<div class="col-md-6 col-lg-6 col-xl-3">
							<div class="mini-stat clearfix">
								<span class="mini-stat-icon bg-info mr-0"><i class="fa fa-sticky-note"></i></span>
								<div class="mini-stat-info float-right">
									<span>
										<?php
											$evento = new GraficaController();
											$evento -> contarEventoController();
										?>
									</span> EVENTOS
								</div>
							</div>
						</div>
					</div>
					<?php 
					}
					?>
				
					<!-- Fila Lista de sucursales -->
					<div class="row ">
						<?php
						
							$ubicacion = new UbicacionController();
							$ubicacion -> dashboardUbicacion();
						
						?>
					</div>


					<?php
					if($_SESSION["rol"] == 1){
					?>

					

					<div class="row"> <!-- Fila para imprimir estadisticas de tecnicos -->
						<?php
							$tecnicos = new GraficaController();
							$tecnicos -> estadisticasTecnicoController();
						?>
					</div> <!-- Fin fila estadisticas tecnico -->


					<!-- Graficas -->
					<div class="row">
						<div class="col-md-6">
							<div class="card">
								<div id="graficames"></div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="card">
								<div id="graficaestatus"></div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="card">
								<div id="graficatipo"></div>
							</div>
						</div>

						<!--
						<div class="col-md-6">
							<div class="card">
								<div id="graficaprioridad"></div>
							</div>
						</div>-->


						<div class="col-md-6">
							<div class="card">
								<div id="graficasubcategoria"></div>
							</div>
						</div>
					</div> <!-- Fin area de graficas -->
					

					<!-- Area de Datatables --
					<div class="row">
						<div class="col-md-6 col-lg-4">
							<div class="card">
								<div class="card-block">
									<h6 class="ml-2 text-primary text-center">Tickets creados por usuario</h6>
									<table id="" class="table table-striped dt-responsive nowrap">
										<thead>
										<tr>
											<th>Nombre</th>
											<th>Total</th>
										</tr>
										</thead>
										<tbody>
											<?php
											//$prioridad = new GraficaController();
											//$prioridad -> ticketsPorUsuarioController();
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-lg-4">
							<div class="card">
								<div class="card-block">
									<h6 class="ml-2 text-primary text-center">Tickets creados por sucursal</h6>
									<table id="" class="table table-striped dt-responsive nowrap">
										<thead>
										<tr>
											<th>Nombre</th>
											<th>Total</th>
										</tr>
										</thead>
										<tbody>
											<?php
											//$prioridad = new GraficaController();
											//$prioridad -> ticketsPorSucursalController();
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>


						<div class="col-md-6 col-lg-4">
							<div class="card">
								<div class="card-block">
									<h6 class="ml-2 text-primary text-center">Tickets creados por categoria</h6>
									<table id="" class="table table-striped dt-responsive nowrap">
										<thead>
										<tr>
											<th>Nombre</th>
											<th>Total</th>
										</tr>
										</thead>
										<tbody>
											<?php
											//$prioridad = new GraficaController();
											//$prioridad -> ticketsPorCategoriaController();
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div> <!- Fin area de Datatables -->
					<?php 
					}
					?>
				</div>
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/highcharts.js"></script>
    <script src="views/js/libs/highchartsexport.js"></script>
	<script src="views/js/app.js"></script>
	<script src="views/js/graficas.js"></script>	 
</body>
</html>