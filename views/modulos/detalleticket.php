<?php 
if(!isset($_SESSION)){
session_start();
}
error_reporting(E_ALL);

$id = $_GET["id"];

if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Detalle </title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel="stylesheet">	
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-header">
								<a href="javascript:history.back()" type="button" class="btn btn-primary btn-sm pull-right"><i class="fa fa-ticket"></i> Volver</a>
							</div>
							<div class="card-block">	
								<div class="row">
									<?php 
										$detalle = new TicketsController();
										$detalle -> detalleTicketController($id);
									?>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>	
	<script src="views/js/app.js"></script> 
	<script>
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox();
		});
	</script>
</body>
</html>