<?php 

require_once "db.php";

class RecordatorioModel{

	public static function mostrarRecordatorioModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE status=1 AND FK_idUsuario = :idUsuario");
		
		$stmt -> bindParam(":idUsuario", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		$stmt -> close();
	}


	public static function mostrarRecordatorio2Model($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE status=1 AND FK_idUsuario = :idUsuario ORDER BY fechaVencimiento ASC LIMIT :offset, :rowsPerPage");
		
		$stmt -> bindParam(":idUsuario", $datosModel['id'], PDO::PARAM_INT);
		$stmt -> bindParam(":offset", $datosModel['offset'], PDO::PARAM_INT);
		$stmt -> bindParam(":rowsPerPage", $datosModel['rowsPerPage'], PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchAll(PDO::FETCH_ASSOC);

		$stmt -> close();
	}


	public static function crearRecordatorioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (fechaVencimiento, descripcion, `status`, color, FK_idUsuario) VALUES (:fechaVencimiento, :descripcion, :estatus, :color, :FK_idUsuario)");

		$stmt -> bindParam(":fechaVencimiento", $datosModel["ubicacion"]);
		$stmt -> bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt -> bindParam(":estatus", $datosModel["estatus"], PDO::PARAM_INT);
		$stmt -> bindParam(":color", $datosModel["color"], PDO::PARAM_STR);
		$stmt -> bindParam(":FK_idUsuario", $datosModel["idUsuario"], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}


	public static function editarRecordatorioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET descripcion = :descripcion WHERE recordatorio.PK_idRecordatorio = :idRecordatorio)");

		$stmt -> bindParam(":fechaVencimiento", $datosModel["ubicacion"]);
		$stmt -> bindParam(":idRecordatorio", $datosModel["idRecordatorio"], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}

	public static function eliminarRecordatorioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE recordatorio SET `status` = 0  WHERE recordatorio.PK_idRecordatorio = :idRecordatorio)");

		$stmt -> bindParam(":idRecordatorio", $datosModel["idRecordatorio"], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}


	public static function recordatorioIDModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM recordatorio WHERE recordatorio.PK_idRecordatorio =  :idRecordatorio)");

		$stmt -> bindParam(":idRecordatorio", $datosModel["idRecordatorio"], PDO::PARAM_INT);

		$stmt->execute();

		return $stmt -> fetch();

		$stmt->close();
	}
}