<?php

require_once "../../controllers/reportesControllers.php";
require_once "../../models/reportesModels.php";
include "../includes/funciones.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxReportes{


	public function reporte1(){

		$respuesta = ReportesController::reporte1Controller();
		
		$str = '<div class="alert alert-warning text-center">Reporte de la fecha INICIO a FINAL </div>
		<table id="reportesData" class="table table-striped" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Usuario</th>
					<th>Telefono</th>
					<th>Rol</th>
					<th>Registro</th>
					<th>Estatus</th>
				</tr>
			</thead>
			<tbody>';
		$x = 1;
		foreach ($respuesta as $row => $item){
			$str.='<tr>
				<td>'.$x.'</td>
				<td>'.$item['nombre']. ' '.$item['apellidos'].'</td>
				<td>'.$item['usuario'].'</td>
				<td>'.$item['telefono'].'</td>
				<td>'.TipoUsuario($item['rol']).'</td>
				<td>'.$item['fechaRegistro'].'</td>
				<td>'.$item['activo'].'</td>
			</tr>';
			$x++;
		}

		$str.= '</tbody>
		</table>';

		echo $str;
	}

	public function reporte2(){

		$respuesta = ReportesController::reporte2Controller();

		$str = '<div class="alert alert-warning text-center">Reporte de la fecha INICIO a FINAL </div>
		<table id="reportes" class="table table-striped" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>#</th>
					<th>ID</th>
					<th>Ref</th>
					<th>Categoria</th>
					<th>Subcategoria</th>
					<th>Requerimiento</th>
					<th>Status</th>
					<th>Prioridad</th>
					<th>Creado</th>
					<th>Cerrado</th>
				</tr>
			</thead>
			<tbody>';
			$x = 1;
			foreach ($respuesta as $row => $item){
				$str.='	<tr>
					<td>'.$x.'</td>
					<td><span class="badge badge-primary">'.$item['PK_idTicket'].'</span></td>
					<td>'.$item['refTicket'].'</td>
					<td>'.$item['categoriaDesc'].'</td>
					<td>'.$item['subcategoriaDesc'].'</td>
					<td><li class="ticket"><span style="font-weight: bold">'.$item['tituloTicket'].': </span><span>'.$item['descripcionTicket'].'</span<li></td>
					<td>'.Estatus($item['statusTicket']).'</td>
					<td>'.Prioridad($item['prioridadTicket']).'</td>
					<td>'.$item['creado'].'</td>
					<td>'.$item['cierre'].'</td>
				</tr>';
				$x++;
			}
			$str.= '</tbody>
		</table>';
		echo $str;
	}


	

}

if($opcion == 'reporte1'){
	$reporte = new AjaxReportes();
	$reporte -> reporte1();
}

if($opcion == 'reporte2'){
	$reporte = new AjaxReportes();
	$reporte -> reporte2();
}


