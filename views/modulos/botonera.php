		<!--=====================================
		Columna Izquierda de la aplicacion.
		=====================================-->

		<div class="sidebar">
		<nav class="sidebar-nav">
			<ul class="nav">
				<br>
				<p></p>
				<li class="nav-title text-center">
					<span>Principal</span>
				</li>
				
				<li class="nav-item">
					<a class="nav-link" href="inicio"><i class="fas fa-home"></i> Inicio </a>
				</li>

				<li class="nav-item nav-dropdown">
					<a class="nav-link nav-dropdown-toggle" href="#"><i class="fas fa-ticket-alt"></i> Tickets</a>
					
					<ul class="nav-dropdown-items">
						<li class="nav-item nav-dropdown">
							<a class="nav-link aaa" href="tickets"><i class="fas fa-ticket-alt" style="font-size:14px"></i> Mis tickets </a>
						</li>
						<li class="nav-item">			  
							<a class="nav-link aaa" href="tickresueltos"><i class="fas fa-check-circle" style="font-size:14px"></i> Resueltos</a>
						</li>
						<li class="nav-item">
							<a class="nav-link aaa" href="tickcerrados"><i class="fas fa-lock" style="font-size:14px"></i> Cerrados</a>
						</li>
						<li class="nav-item">
							<a class="nav-link aaa" href="tickcancelados"><i class="fas fa-ban" style="font-size:14px"></i> Cancelados</a>
						</li>
					</ul>
				</li>
				
				<?php
				if($_SESSION["rol"] == 1){
				?>
				
				<!--
				<li class="nav-item">
					<a class="nav-link" href="reportes"><i class="fa fa-file"></i> Reportes </a>
				</li>
				-->

				<li class="nav-item">
					<a class="nav-link" href="calendario"><i class="fas fa-calendar-alt"></i> Calendario </a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="usuario"><i class="fas fa-user"></i> Usuarios</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="categoria"><i class="fas fa-list-alt"></i> Categorias</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="ubicacion"><i class="fas fa-map-marker-alt"></i> Ubicacion</a>
				</li>

				<li class="divider"></li>
				<li class="nav-title text-center">
					<span>Mantenimiento</span>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="sistemas"><i class="fas fa-lock"></i> Contraseñas</a>
				</li>
				
				<?php
				}
				?>
					
				<li class="divider"></li>
				<li class="nav-title text-center mt-3">
					<span>Extras</span>
				</li>
				<!--
				<li class="nav-item">
					<a class="nav-link" href="recordatorio"><i class="fa fa-list-alt"></i> Mis Recordatorios</a>
				</li>
				-->
				<li class="nav-item">
					<a class="nav-link" href="kb"><i class="fas fa-database"></i> Base de conocimiento</a>
				</li>
				<p></p>
				<li class="nav-item">
					<a class="nav-link" href="doc"><i class="fas fa-file"></i> Acerca de...</a>
				</li>
			</ul>
		</nav>
		</div>