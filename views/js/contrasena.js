$('document').ready(function() {
	$("#frmcambioContrasena")[0].reset();
$("#frmcambioContrasena").on("submit", function(e){
		e.preventDefault();
		var formData = new FormData(document.getElementById("frmcambioContrasena"));
        var ruta = "views/ajax/usuarioAjax.php?op=password";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
			contentType: false,
            processData: false,
			dataType : 'json',
			beforeSend: function(){
				$("#cambiarPsw").html('<span class="fa fa-spinner fa-pulse fa-fw"></span> &nbsp; Procesando');				
			},
            success: function(respuesta)
            {
                if(respuesta.success == true) {
					$("#frmcambioContrasena")[0].reset();					
					$("#cambiarPsw").html('<span class="fa fa-exchange"></span> &nbsp; Aceptar');
					//$("#cambiarContrasena").hide();
					//$("#modalcancelarTicket").modal('hide');
					$("#cambiarContrasena").modal('hide');
					successSwal(respuesta.messages);
					setTimeout(function() {
					// Despues de 4 segundos, recargar pagina.
					location.reload();
					}, 4000);
				}
				else{					
					//$("#frmregistrarTicket")[0].reset();					
					$("#frmcambioContrasena")[0].reset();					
					$("#cambiarPsw").html('<span class="fa fa-exchange"></span> &nbsp; Aceptar');
					$("#cambiarContrasena").modal('hide');
					errorSwal(respuesta.messages);
				}
            }
        });
    });
});

function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}