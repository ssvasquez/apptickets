<?php

    require_once "../../controllers/graficaControllers.php";
    require_once "../../models/graficaModels.php";

    $opcion = isset($_GET['op'])?$_GET['op']:'';

    class AjaxGrafica{

        public function mesAjax(){

            $respuesta = GraficaController::ticketsPorAnio();

            print json_encode($respuesta, JSON_NUMERIC_CHECK);
        }

        
        public function estatusAjax(){

            $respuesta = GraficaController::estatusTickets();

            print json_encode($respuesta, JSON_NUMERIC_CHECK);
        }


        public function subcategoriaAjax(){

            $respuesta = GraficaController::ContarTicketsSubcategoriaController();

            $fila['type'] = 'pie';
            $fila['name'] = 'Total';

            foreach ($respuesta as $row => $item){
                $fila['data'][] = array($item['categoriaDesc'], $item['CantidadTickes']);    
            }

            $datos = array();

            array_push($datos, $fila);

            print json_encode($datos, JSON_NUMERIC_CHECK);

        }

 
        public function prioridadAjax(){

            $respuesta = GraficaController::prioridadTickets();

            $fila['type'] = 'pie';
            $fila['name'] = 'Total';

            $fila['data'][] = array('Critica', $respuesta[0]);
            $fila['data'][] = array('Urgente', $respuesta[1]);
            $fila['data'][] = array('Alto', $respuesta[2]);
            $fila['data'][] = array('Medio', $respuesta[3]);
            $fila['data'][] = array('Bajo', $respuesta[4]);
           
            $datos = array();

            array_push($datos, $fila);

            print json_encode($datos, JSON_NUMERIC_CHECK);

        }


        public function tipoAjax(){

            $respuesta = GraficaController::TipoTickets();

            $fila['type'] = 'pie';
            $fila['name'] = 'Total';

            $fila['data'][] = array('Servicio', $respuesta[0]);
            $fila['data'][] = array('Incidente', $respuesta[1]);
            $fila['data'][] = array('Requerimiento', $respuesta[2]);
           
            $datos = array();

            array_push($datos, $fila);

            print json_encode($datos, JSON_NUMERIC_CHECK);

        }
    }

    // Imprimir objetos.

    #Tickets por Mes
    if($opcion == 'mes'){
        $mes = new AjaxGrafica();
        $mes -> mesAjax();
    }

    # Tickets por Estatus
    if ($opcion == 'estatus'){
        $estatus = new AjaxGrafica();
        $estatus -> estatusAjax();
    }

    # Tickets por Tipo
    if ($opcion == 'tipo'){
        $tipo = new AjaxGrafica();
        $tipo -> tipoAjax();
    }

    # Tickets por Prioridad
    if ($opcion == 'prioridad'){

        $prio = new AjaxGrafica();
        $prio -> prioridadAjax();
    }

    # Tickets por subcategoria
    if ($opcion == 'subcategoria'){

        $sub = new AjaxGrafica();
        $sub -> subcategoriaAjax();
    }
?>