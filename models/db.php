<?php 

Class conexion{

	public static function Conectar(){

		$options = array(
			PDO::ATTR_PERSISTENT => true, 
			PDO::ATTR_EMULATE_PREPARES => true, 
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, 
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"
		);


		if (isset($link)) return $link;


		try{
			$link = new PDO("mysql:host=localhost;dbname=tickets_v2","root", "", $options);
			return $link;
		}catch (PDOException $e) {
			echo "Error : " . $e->getMessage(); 
		}

		

		/**
		*	El array $options es muy importante para tener un PDO bien configurado

		*	1. PDO::ATTR_PERSISTENT => false: sirve para usar conexiones persistentes
		*	2. PDO::ATTR_EMULATE_PREPARES => false: Se usa para desactivar emulación de consultas preparadas 
		*      forzando el uso real de consultas preparadas. 
		*	3. PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION importante para un correcto manejo de las excepciones. 
		*      Si no se usa bien, cuando hay algún error este se podría escribir en el log revelando datos como la contraseña !!!
		*	4. PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'": establece el juego de caracteres a utf8.
		*/
		
		
		return $link;
	}
}
