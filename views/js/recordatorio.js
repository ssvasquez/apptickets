//Variable para manipular la tabla Tickets
var tblRecordatorio;
var recargar;
$(document).ready(function() {
	//Personalizar tablas.
	
	recargar = $('#recargar');
	tblRecordatorio = $('#tickets').DataTable({		
		"ordering": false,
		"ajax": "crudRecordatorios.php?op=read",
		"order": [],
		"bInfo": false,
		responsive: true,
		"language":
		{
			"sProcessing":     "Procesando...",
			"lengthMenu": "Mostrar _MENU_ registros",
			"zeroRecords": "No hay Recordatorios relacionados con la búsqueda",
			"info": "Mostrando página _PAGE_ de _PAGES_",
			"infoEmpty": "No hay datos disponibles",
			"infoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sSearch": "Buscar:",
			"oPaginate": 
			{
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			}
		},		
		columnDefs: [
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: 0 }
		]	
		
	});
	
	
	/**	Función para agregar un recordatorio**/
	$("#frmregistrarRecordatorio").on("submit", function(e){		
		e.preventDefault();
		var formData = new FormData(document.getElementById("frmregistrarRecordatorio"));
		var ruta = "crudRecordatorios.php?op=create";
		$.ajax({
			type: "POST",
			url: ruta,				
			data: formData,
			contentType: false,
			processData: false,				
			dataType : 'json',
			success: function(response)
			{
				if(response.success == true) {
					// Recargar la tabla, Limpiar los campos, Cerrar modal y Mostrar mensaje de éxito.
					//$("#recargar").load(pagina);
					$("#content").load(location.href + " #content");
					$("#frmregistrarRecordatorio")[0].reset();
					$("#registrarRecordatorio").modal('hide');
					swal({
						title: "Éxito!",
						text: response.messages,
						type: "success",
						timer: 3000,
						showConfirmButton: true  
					});

				} else {
					// Mostrar mensaje de error.
					$("#registrarRecordatorio").modal('hide');
					swal({
						title: "Error!",
						text: response.messages,
						type: "error",
						showConfirmButton: true  
					});					
				}
			}
		});			
	});	
});




/**	Función para el botón Eliminar Recordatorio.	**/
function eliminarRecordatorio(PK_idRecordatorio = null) {
	if(PK_idRecordatorio) {
		$("#btnEliminar").unbind('click').bind('click', function() {
			$.ajax({
				url:"crudRecordatorios.php?op=delete",
				type: 'POST',
				data: {idRecordatorio : PK_idRecordatorio},
				dataType: 'json',
				success:function(response) {
					if(response.success == true) {
						/**	Actualizar la tabla, Cerrar modal y mostrar mensaje de éxito.	**/
						$("#content").load(location.href + " #content");
						$("#modaleliminarRecordatorio").modal('hide');
						swal({
							title: "Éxito!",
							text: response.messages,
							type: "success",
							timer: 3000,
							showConfirmButton: true  
						});						

					} else {
						/**	Cerrar modal y mostrar mensaje de error.	**/
						$("#modalcerrarTicket").modal('hide');
						swal({
							title: "Error!",
							text: response.messages,
							type: "error",							
							showConfirmButton: true  
						});						
					}
				}
			});
		});
	} else {
		alert('Error: Recarga la página de nuevo');
	}
}



/**	Función Editar Recordatorio 	**/
function editarRecordatorio(PK_idRecordatorio = null) {
	if(PK_idRecordatorio) {
		// Restaurar valores por default
		$(".form-group").removeClass('has-error').removeClass('has-success');
		$(".text-danger").remove();
		$(".edit-messages").html("");
		$("#idRecordatorio").remove();

		$.ajax({
			url:"crudRecordatorios.php?op=getID",
			type: 'POST',
			data: {idRecordatorio : PK_idRecordatorio},
			dataType: 'json',
			success:function(response) {
				$("#editDescripcion").val(response.descripcion);				

				$(".modaleditarRecordatorio").append('<input type="hidden" name="idRecordatorio" id="idRecordatorio" value="'+response.PK_idRecordatorio+'"/>');
				$("#frmeditarRecordatorio").unbind('submit').bind('submit', function() {
					$(".text-danger").remove();
					var form = $(this);					
					
					var editDescripcion = $("#editDescripcion").val();					
					
					if(editDescripcion == "") {
						$("#editDescripcion").closest('.form-group').addClass('has-error');
						$("#editDescripcion").after('<p class="text-danger">Campo necesario</p>');
					} else {
						$("#editDescripcion").closest('.form-group').removeClass('has-error');
						$("#editDescripcion").closest('.form-group').addClass('has-success');
					}

					if(editDescripcion){
						$.ajax({
							url:"crudRecordatorios.php?op=update",
							type: 'POST',
							data: form.serialize(),
							dataType: 'json',
							success:function(response) {
								if(response.success == true) {									
									// Recargar area de contenidos, cerrar modal y mostrar mensaje de éxito
									//tblRecordatorio.ajax.reload(null, false);
									$("#content").load(location.href + " #content");
									$(".form-group").removeClass('has-success').removeClass('has-error');
									$(".text-danger").remove();
									$("#modaleditarRecordatorio").modal('hide');
									swal({
										title: "Éxito!",
										text: response.messages,
										type: "success",
										timer: 3000,
										showConfirmButton: true  
									});									
								} else {
									swal({
										title: "Error!",
										text: response.messages,
										type: "error",
										timer: 3000,
										showConfirmButton: true  
									});
								}
							} 
						}); 
					}				
					return false;
				});		
			} 
		}); 

	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


