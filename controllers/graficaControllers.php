<?php 

class GraficaController{

    public static function contarTicketsController(){

        $respuesta = GraficaModels::ContarRegistrosTabla("ticket");
        
        echo $respuesta;
    }


    public static function contarUsuarioController(){

        $respuesta = GraficaModels::ContarRegistrosTabla("usuario");
        
        echo $respuesta;
    }


    public static function contarCategoriaController(){

        $respuesta = GraficaModels::ContarRegistrosTabla("categoria");
        
        echo $respuesta;
    }


    public static function contarEventoController(){

        $respuesta = GraficaModels::ContarRegistrosTabla("evento");
        
        echo $respuesta;
    }


	// GRAFICAS    
	public static function ticketsPorAnio(){
		// De 1 a 12 - Enero a Diciembre
		$mes = array();

		for ($i=1; $i <= 12; $i++) { 
			# code...
			$respuesta = GraficaModels::ContarTicketsPorAnio($i, "ticket");

			$mes[] =  $respuesta;
		}

		return $mes;
	}


    // 1 - Abierto, 2 - Asignado, 3 - Pendiente, 4 - Cerrado, 5 - Cancelado, 6 - Resuelto, 7 - Reabierto
    public static function estatusTickets(){

        $estatus = array();

        for ($i=1; $i <= 7; $i++) {

            $respuesta = GraficaModels::ContarTicketsEstatus($i, "ticket");

            $estatus[] = $respuesta;
        }

        return $estatus;
    }


    // 1 - Critica, 2 - Urgente, 3 - Alto, 4 - Medio, 5 - Bajo
    public static function prioridadTickets(){

        $prioridad = array();

        for ($i=1; $i <= 5; $i++) { 

            $respuesta = GraficaModels::ContarTicketsPrioridad($i, "ticket");

            $prioridad[] =  $respuesta;
            
        }

       return $prioridad;
    }


    public static function TipoTickets(){

        $tipo = array();

        for ($i=1; $i <= 3; $i++) { 

            $respuesta = GraficaModels::ContarTicketsTipo($i, "ticket");

            $tipo[] =  $respuesta;
            
        }

      return $tipo;
      /*echo '<pre>';
      var_dump($tipo);
      echo '</pre>';*/
    }



    // Pendiente Graficas...
    public static function ContarTicketsSubcategoriaController(){

       return GraficaModels::ContarTicketsSubcategoria();

    }


    public static function estadisticasTecnicoController(){
        
        $respuesta = GraficaModels::estadisticasTecnicoModel();

        foreach ($respuesta as $row => $item){
            echo '
            <div class="col-sm-6 col-md-4 col-xl-3 mb-1">
            <div class="tick-div">
                <p class="tick-avatarLink">
                    <img src="views/images/6.png" alt="user-image" class="thumb-sm rounded-circle"/>
                </p>
                <div class="tick-divUser">
                    <div class="tick-divName">
                        <p class="text-right pr-2">'.$item['nombre'].' '.$item['apellidos'].'</p>
                    </div>
                </div>
                <div class="tick-divStats">
                    <ul class="tick-Arrange">
                        <li class="tick-ArrangeSizeFit">
                            <div class="tick-StatLabel"><i class="fas fa-ticket-alt"></i> Total</div>
                            <div class="tick-StatValue">'.$item['NT'].'</div>
                        </li>
                        <li class="tick-ArrangeSizeFit text-success">
                            <div class="tick-StatLabel text-success"><i class="fa fa-check"></i> Atendidos</div>
                            <div class="tick-StatValue">';
                                $respuesta = GraficaModels::atendidosTecnicoModel($item['PK_idUsuario']);
                                echo $respuesta;
                            echo '</div>
                        </li>
                        <li class="tick-ArrangeSizeFit text-danger">
                            <div class="tick-StatLabel text-danger"><i class="fas fa-clock"></i> Pendientes</div>
                            <div class="tick-StatValue">';
                                echo $item['NT'] - $respuesta = GraficaModels::atendidosTecnicoModel($item['PK_idUsuario']);
                            echo '</div>
                        </li>	
                    </ul>
                </div>
            </div>
        </div>';
        }
    }


    public static function ticketsPorUsuarioController(){
        $respuesta = GraficaModels::ticketsPorUsuario();

        foreach ($respuesta as $row => $item){

			echo '<tr>
			    <td>'.$item["nombre"]. " ". $item["apellidos"].'</td>
			    <td>'.$item["Total"].'</td>
			</tr>';

		}
    }


    public static function ticketsPorSucursalController(){

        $respuesta = GraficaModels::ticketsPorSucursal();

        foreach ($respuesta as $row => $item){

			echo '<tr>
			    <td>'.$item["ubicacionDesc"].'</td>
			    <td>'.$item["Total"].'</td>
			</tr>';

		}
    }


    public static function ticketsPorCategoriaController(){

        $respuesta = GraficaModels::ticketsPorCategoria();

        foreach ($respuesta as $row => $item){

			echo '<tr>
			    <td>'.$item["categoriaDesc"].'</td>
			    <td>'.$item["CantidadTickes"].'</td>
			</tr>';

		}
    }
}