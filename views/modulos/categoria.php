<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}

if($_SESSION["rol"] != 1){
	echo'<script type="text/javascript"> window.location.href="inicio";</script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Inicio </title>
	<link rel="stylesheet" href="views/css/all.min.css">
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/responsive.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
			<div class="animated fadeIn">
					<div class="row">
						<div class="col-lg-6">
							<div class="card mt-1">
								<div class="card-header d-flex justify-content-between">
									<span><i class="fas fa-list-alt"></i> Categorias</span>
									<div class="pull-right addCat">
										<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#agregarCategoria" id="btnAgregarCategoria">
											<i class="fas fa-plus-circle"></i> Categoria
										</button>
									</div>
								</div>
								<div class="card-block">
									<table id="categorias" class="table table-striped" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th data-priority="1">No</th>
												<th data-priority="2">Descripción</th>
												<th></th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>


						<!--Modal Registro Categoria-->
				<div class="modal fade" id="agregarCategoria" tabindex="-1" role="dialog" aria-labelledby="categoria" aria-hidden="true">
					<div class="modal-dialog modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="categoria">Agregar Categoria</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="POST" id="frmRegistrarCategoria">
								<div class="modal-body">
									<div class="form-group">
										<div class="input-group" id="referencia">
											<span class="input-group-addon">Descripcion</span>
											<input type="text" id="altaCategoria" name="altaCategoria" class="form-control" placeholder="Nombre de la categoria">
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>-->
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- / Fin Modal Registro Categoria-->

				<!--Inicio Modal Editar Categoria-->
		<div class="modal fade" tabindex="-1" role="dialog" id="editar_categoria">
			<div class="modal-dialog modal-primary" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"><span class="fa fa-edit"></span> Editar categoria</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="POST" id="frmEditarCategoria">
						<div class="modal-body">
							<div class="form-group">
								<div class="input-group" id="referencia">
									<span class="input-group-addon">Descripcion</span>
									<input type="text" id="categoriaEditar" name="categoriaEditar" class="form-control">
									<input type="hidden" id="idCategoriaEditar" name="idCategoriaEditar" class="form-control">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Fin Modal Editar Categoria-->


				<!--	============================================
					Area para controlar subcategorias
				============================================	-->

				<div class="col-lg-6">
					<div class="card mt-1" id="areade_">
						<div class="card-header d-flex justify-content-between">
							<span><i class="fa fa-tag"></i> Subcategorias</span>	
							<div class="pull-right addCat">
								<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#agregarSubcategoria" id="btnAgregarSubcategoria">
									<i class="fa fa-plus-circle"></i> Subcategoria
								</button>
							</div>
						</div>
						<div class="card-block">
							<table id="subCategorias" class="table table-striped table-vertical" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th data-priority="1">No</th>
										<th>Categoria</th>
										<th data-priority="2">Descripción</th>
										<th>Acciones</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

				
				<div class="modal fade" id="agregarSubcategoria" tabindex="-1" role="dialog" aria-labelledby="gregarSubcategoriaModal" aria-hidden="true">
					<div class="modal-dialog modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="gregarSubcategoriaModal">Agregar subcategoria</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							
							<form action="" method="POST" id="frmRegistrarSubcategoria">
								<div class="modal-body">
									<div class="form-group">
										<div class="input-group" id="referencia">
											<span class="input-group-addon">Descripcion</span>
											<input type="text" id="altaSubcategoria" name="altaSubcategoria" class="form-control" placeholder="Nombre de la subcategoria">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group" id="referencia">
											<span class="input-group-addon">Categoria</span>
											<select class="form-control" name="deCategoria" id="deCategoria">
												<option value="">Seleccione...</option>
													<?php
														$categoria = new CategoriaController();
														$categoria -> categoriaSeleccionController();
													?>	
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!--Fin modal Registro subcategoria -->


				<!--Modal Editar Subcategoria-->
		<div class="modal fade" tabindex="-1" role="dialog" id="editar_subcategoria">
			<div class="modal-dialog modal-primary" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"><span class="fa fa-edit"></span> Editar subcategoria</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="POST" id="frmEditarSubcategoria">
						<div class="modal-body">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Descripcion</span>
									<input type="text" id="subcategoriaEditar" name="subcategoriaEditar" class="form-control">
									<input type="hidden" id="idSubcategoriaEditar" name="idSubcategoriaEditar" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Categoria</span>
									<select class="form-control" name="categoriaSubEditar" id="categoriaSubEditar">
										<option value="">Seleccione...</option>
											<?php
													$categoria = new CategoriaController();
													$categoria -> categoriaSeleccionController();
											?>
									</select>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Fin Modal Editar Subcategoria-->
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>
	<script src="views/js/libs/jquery.dataTables.min.js"></script>	
	<script src="views/js/libs/dataTables.bootstrap4.min.js"></script>
	<script src="views/js/libs/dataTables.responsive.min.js"></script>
	<script src="views/js/libs/responsive.bootstrap4.min.js"></script>

	<script src="views/js/app.js"></script>
	<script src="views/js/subcategoria.js"></script>	 
</body>
</html>


				