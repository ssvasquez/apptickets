
/*	=======	M O S T R A R Tickets Cerrados	=======*/
var tblTicketCerrados;
$(document).ready(function() {
	tblTicketCerrados = $('#tickcerrados').DataTable({
		//"ordering": false,
		responsive: true,
		"columns": [
			{"orderable": false },
			null,
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
		],
		"ajax": "views/ajax/ticketsAjax.php?op=cerrados",
		"order": [],
		"bInfo": false,
		"language": {
			"url": "views/js/json/Spanish.json"
		},
		columnDefs: [
			{
				responsivePriority: 1,
				targets: 0
			}
		],

		/**	Función para filtrar el nombre del tecnico en la tabla Tickets.	**/
		initComplete: function () {
			tblTicketCerrados.columns([8]).every(function () {
				var column = this;
				var select = $('<select><option value="" >Default</option></select>')
					.appendTo($(column.header()).empty())
					.on('change', function () {
						column
							.search(this.value)
							.draw();
					});
				column.data().unique().sort().each(function (d) {
					select.append('<option value="' + d + '">' + d + '</option>')
				});
			});
		}
	});
});