    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>
        <a class="navbar-brand" href="#"></a>
        <ul class="nav navbar-nav hidden-md-down b-r-1">
            <li class="nav-item">
                <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
            </li>
        </ul>

        <ul class="nav navbar-nav ml-auto">
			<li class="nav-item ">
                <div class="text-center">
                    <span class="usuario"><?php echo $_SESSION["nombre"]?></span>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link nav-pill avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="views/images/6.png" class="img-avatar" alt="usuario">
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-center">
                        <strong>Configuraciones</strong>
                    </div>
                    <a class="dropdown-item" href="perfil"><i class="fa fa-user"></i> Perfil</a>
                    <a class="dropdown-item" href="salir"><i class="fa fa-lock"></i> Salir</a>
                </div>
            </li>
			<li class="nav-item hidden-md-down">
                <a class="" href="#"></a>
            </li>
		</ul>
    </header>