<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}

if($_SESSION["rol"] != 1){
	echo'<script type="text/javascript"> window.location.href="inicio";</script>';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Inicio </title>
	<link rel="stylesheet" href="views/css/all.min.css">
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/responsive.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-lg-6">
							<div class="card">
								<div class="card-header d-flex justify-content-between">
									<span><i class="fa fa-map-marker"></i> Ubicación</span>
									<div class="pull-right addCat">
										<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#agregarUbicacion" id="btnAgregarUbicacion">
											<i class="fa fa-plus-circle"></i> Ubicación
										</button>
									</div>
								</div>
								<div class="card-block">
									<table id="ubicacion" class="table table-striped table-vertical" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th data-priority="1">No</th>
												<th data-priority="2">Descripción</th>
												<th>Acciones</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!--Modal Registro-->
				<div class="modal fade" id="agregarUbicacion" tabindex="-1" role="dialog" aria-labelledby="registrarUbicacionModal" aria-hidden="true">
					<div class="modal-dialog modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="registrarUbicacionModal">Agregar Ubicacion</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="POST" id="frmRegistrarUbicacion">
								<div class="modal-body">
									<div class="form-group">
										<div class="input-group"> 
											<span class="input-group-addon">Descripcion</span>
											<input type="text" id="altaUbicacion" name="altaUbicacion" class="form-control" placeholder="Ubicacion">
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div><!--Fin Modal Registro-->


				<!--Inicio Modal Editar-->
				<div class="modal fade" tabindex="-1" role="dialog" id="modalEditarUbicacion">
					<div class="modal-dialog modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><span class="fa fa-edit"></span> Editar ubicacion</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="POST" id="frmEditarUbicacion">
								<div class="modal-body">
									<div class="form-group">
										<div class="input-group"> 
											<span class="input-group-addon">Descripción</span>
											<input type="text" id="ubicacionEditar" name="ubicacionEditar" class="form-control">
											<input type="hidden" id="idUbicacionEditar" name="idUbicacionEditar" class="form-control">
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Guardar cambios</button>
								</div>
							</form>
						</div>
					</div>
				</div><!-- Fin Modal Editar -->
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>
	<script src="views/js/libs/jquery.dataTables.min.js"></script>	
	<script src="views/js/libs/dataTables.bootstrap4.min.js"></script>
	<script src="views/js/libs/dataTables.responsive.min.js"></script>
	<script src="views/js/libs/responsive.bootstrap4.min.js"></script>

	<script src="views/js/app.js"></script>
	<script src="views/js/ubicacion.js"></script> 
</body>
</html>
