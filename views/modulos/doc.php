<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Inicio </title>
	<link rel="stylesheet" href="views/css/all.min.css">
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
			<div class="animated fadeIn">
					<section id="reglas">
						<div class="card">
							<div class="card-header">
								<div class="card-text">Reglas de operación.</div>
							</div>
							<div class="card-block">

								<?php  
								if($_SESSION["rol"] == 1)
								{
								?>

								<div class="alert alert-success">ADMINISTRADORES TIENEN PERMISO DE:</div>
								<ul>
									<li>Dar de alta un Ticket y asignarlos al Técnico correspondiente o Autoasignarse.</li>
									<li>Ver los Tickets RESUELTOS, CERRADOS Y CANCELADOS de todo el sistema.</li>
									<li>Editar un ticket en sus campos (<i>tema, descripción, tipo de ticket, escalado, status y prioridad</i>), así como de <span class="badge badge-success">REASIGNAR</span> un ticket a otro técnico.</li>
									<li>Crear eventos (<i>respuestas</i>) a todos los tickets que se han generado dentro del sistema.</li>
									<li>CANCELAR, CERRAR y/o RESOLVER cualquier ticket creado en el sistema.</li>
									<li>Dar de alta y editar las sucursales.</li>
									<li>Agregar, modificar y cambiar el rol a los usuarios.</li>
									<li>Añadir y modificar las categorías y subcategorías.</li>
									<li>Cambiar su contraseña.</li>
									<li>Observar su perfil [Total de Pendientes], [Tickets Creados], [Tickets Resueltos], [Tickets Cerrados].</li>
									<li>Acceso a graficas y estadisticas del sistema de Tickets.</li>
									<!--<li>Generar reportes.</li>-->
									<li>Observar el calendario de Tickets creados.</li>
									<li>Resetear contraseña en el área <code>Contraseñas </code>.</li>
									<li>Activar / Desactivar usuario.</li>
									<li>Activar / Desactivar sucursal.</li>
									<li>Activar / Desactivar categoria</li>
									<li>Activar / Desactivar subcategoria</li>
								</ul>

								<?php } ?>
								
								<div class="alert alert-success">TÉCNICOS TIENEN PERMISO DE:</div>
								<ul>
									<li>Dar de alta un Ticket y asignarlos al Técnico correspondiente o Autoasignarse.</li>
									<li>Ver los Tickets RESUELTOS, CERRADOS Y CANCELADOS que tiene asignado.</li>
									<li>Crear eventos (<i>respuestas</i>) a los tickets que tiene asignado.</li>
									<li>CANCELAR, CERRAR y/o RESOLVER solamente los tickets que tiene asignado.</li>
									<li>Cambiar su contraseña.</li>
									<li>Observar su perfil [Total de Pendientes], [Tickets Creados], [Tickets Resueltos], [Tickets Cerrados].</li>
									<!--<li>Crear, editar y eliminar sus propios recordatorios.</li>-->
								</ul>
								<div class="alert alert-success">CAMBIO DE STATUS PARA TICKETS<small> aplica para ADMINISTRADORES y TÉCNICOS</small></div>
								<ul>
									<li>Un Ticket puede cambiar sin complicaciones su status a RESUELTO, CERRADO O CANCELADO si es un ticket padre.</li>
									<li>Un Ticket padre puede cambiar su status a RESUELTO si todos sus tickets hijos están en status RESUELTO, CERRADO Y/O CANCELADO.</li>
									<li>Un Ticket padre puede cambiar su status a CERRADO si todos sus tickets hijos están en status CERRADOS Y/O CANCELADOS.</li>
									<li>Un Ticket solamente se podrá reabrir cuando está en status RESUELTO.</li>
									<li>Un Ticket hijo se puede reabrir siempre y cuando el ticket padre tambien esté REABIERTO ó ABIERTO, ASIGNADO y PENDIENTE.</li>
									<li>En los Tickets en status CERRADO Y CANCELADO ya no se podrá hacer ninguna operacion.</li>										
								</ul>
							</div>
						</div>	
					</section>
				</div>
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>	
	<script src="views/js/app.js"></script>	 
</body>
</html>