<?php 

class UbicacionController{

	public static function mostrarUbicacionController(){

		$respuesta = UbicacionModel::mostrarUbicacionTodosModel("ubicacion");
		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['activo'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEditarUbicacion" onclick="editarUbicacion('.$item['PK_idUbicacion'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDesactivarUbicacion" onclick="desactivarUbicacion('.$item['PK_idUbicacion'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
            }else{
				
				$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalActivarUbicacion" onclick="activarUbicacion('.$item['PK_idUbicacion'].')"> <span class="fa fa-toggle-off"></span> A c t i v a r</button>';
			}
			
			$datos['data'][] = array(
				$i, 
				$item['ubicacionDesc'],
				$eventos
			);
			$i++;
		}
		
		//Mandar un array a AJAX;
		return $datos;
	}
	
	// Arreglo de sucursales para indicadores
	public static function dashboardUbicacion(){
		$sucursales = array(
			"MOR" => 100,
			"REV" => 38,
			"NAU" => 33,
			"VIC" => 28,
			"VLL" => 27,
			"LOP" => 16,
			"MTY" => 13,
			"URU" => 10,
			"GUA" => 8,
			"PUE" => 8,
			"HER" => 6,
			"LEO" => 6,
			"VER" => 6,
			"MER" => 5,
			"QRO" => 4
		);

		foreach ($sucursales as $key => $item){
			echo'
			<div class="col-12 col-sm-4 col-md-2 col-lg-1">
				<div class="card" style="margin-bottom:3px">
					<div class="card-block">
						<div class="h4 text-muted text-center mb-0">
							<i class="fa fa-map-marker"></i>
						</div>
						<div class="h6 mb-0 text-center">'.$key.'</div>
						<div class="progress progress-md mt-1">
							<div class="progress-bar bg-danger" role="progressbar" style="width:'.($item > 20 ? $item : 20) .'%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">'.$item.'%</div>

						</div>
					</div>
				</div>
			</div>';
		}
	}


	// Esta funcion esta deshabilitada dentro de la aplicacion 
	public static function dashboardUbicacionController(){

		$respuesta = UbicacionModel::mostrarUbicacionTodosModel("ubicacion");

		foreach ($respuesta as $row => $item){
			if ($item['PK_idUbicacion'] == 1 || $item['PK_idUbicacion'] == 2 || $item['PK_idUbicacion'] == 3 || $item['PK_idUbicacion'] == 4 || $item['PK_idUbicacion'] == 5 || $item['PK_idUbicacion'] == 6 || $item['PK_idUbicacion'] == 7 || $item['PK_idUbicacion'] == 9 || $item['PK_idUbicacion'] == 10 || $item['PK_idUbicacion'] == 11 || $item['PK_idUbicacion'] == 12 || $item['PK_idUbicacion'] == 13 || $item['PK_idUbicacion'] == 14 || $item['PK_idUbicacion'] == 19 || $item['PK_idUbicacion'] == 21){
				echo'
				<div class="col-6 col-sm-3 col-md-3 col-lg-2">
					<div class="card" style="margin-bottom:3px">
						<div class="card-block">
							<div class="h4 text-muted text-center mb-0">
								<i class="fa fa-map-marker"></i>
							</div>
							<div class="h6 mb-0 text-center">'.$item['ubicacionDesc'].'</div>
							<div class="progress progress-md mt-1">';
								if($item['PK_idUbicacion'] == 13 || $item['PK_idUbicacion'] == 5){
									echo '<div class="progress-bar bg-danger" role="progressbar" style="width: 100%"></div>';
								}
								else{
									echo '<div class=" progress-bar bg-success " role="progressbar" style="width: 100%"></div>';
								}
								echo'
							</div>
						</div>
					</div>
				</div>';
			}
		}
	}


    public static function mostrarIDUbicacionController(){

		$idUbicacion = $_POST['idUbicacion'];

		$respuesta = UbicacionModel::mostrarIDUbicacionModel('ubicacion', $idUbicacion);

		return $respuesta;
    }


	public static function guardarUbicacionController(){

		if(isset($_POST["altaUbicacion"])){

			$datosController = array("ubicacion"=>$_POST["altaUbicacion"],
								"activo"=> '1' );

			$respuesta = UbicacionModel::guardarUbicacionModel($datosController, "ubicacion");
		
			return $respuesta;

		}		
    }


	public static function editarUbicacionController(){

		$datosController = array("id" => $_POST["idUbicacionEditar"],
								"ubicacion" => $_POST["ubicacionEditar"]);

		$respuesta = UbicacionModel::editarUbicacionModel($datosController, "ubicacion");

		return $respuesta;
	}


	public static function desactivarUbicacionController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idUbicacion'],
									 "estatus" => '0');

            $respuesta = UbicacionModel::editarEstatusUbicacionModel($datosController, "ubicacion");

			return $respuesta; 

        //}

    }
	
	
    public static function tieneUsuariosController(){

		$datosController = array("id" => $_POST['idUbicacion']);

		$respuesta = UbicacionModel::tieneUsuariosModel($datosController, "usuario");

		return ($respuesta);

	}
	

	public static function activarUbicacionController(){

		$datosController = array(
			"id" => $_POST['idUbicacion'],
			"estatus" => '1');

		$respuesta = UbicacionModel::editarEstatusUbicacionModel($datosController, "ubicacion");

		return $respuesta;
	}

	public static function ubicacionSeleccionController(){

        $respuesta = UbicacionModel::mostrarUbicacionModel("ubicacion");

        foreach ($respuesta as $row => $item){

            echo '<option value="'.$item["PK_idUbicacion"].'">'.$item["ubicacionDesc"].'</option>';

        }

	}
	
	public static function comboBoxUbicacionController(){

		$respuesta = UbicacionModel::comboboxUbicacionModel("ubicacion", $_POST['PK_idUsuario']);

		foreach ($respuesta as $row => $item){

			$html = '<option value="'.$item["PK_idUbicacion"].'">'.$item["ubicacionDesc"].'</option>';

		}
		echo $html;
	}

}