<?php

require_once "../../controllers/usuarioControllers.php";
require_once "../../models/usuarioModels.php";
include "../includes/correo.php";
include "../includes/funciones.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxUsuario{


	public function mostrarUsuario(){

		$respuesta = UsuarioController::mostrarUsuarioController();

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['activo'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEditarUsuario" onclick="editarUsuario('.$item['PK_idUsuario'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modaldesactivarUsuario" onclick="desactivarUsuario('.$item['PK_idUsuario'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
            }else{
				
				$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalactivarUsuario" onclick="activarUsuario('.$item['PK_idUsuario'].')"> <span class="fa fa-toggle-on"></span> A c t i v a r</button>';
			}
			
			$datos['data'][] = array(
				$i, 
				$item['nombre'].' '.$item['apellidos'],
				date("d-m-Y", strtotime($item['fechaRegistro'])),
				Cifrado::decifrar($item['correo']),
				$item['telefono'],
				TipoUsuario($item['rol']),
				$item['ubicacionDesc'],
				$eventos
			);
			$i++;
		}

		print json_encode($datos);
	}


	public function sistemas(){

		$respuesta = UsuarioController::areaSistemasController();

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			$buttonResetear ='
				<button type="button" class="btn btn-info btn-sm" onclick="resetearPassword('.$item['PK_idUsuario'].')"> 
					<span class="fa fa-edit"></span> Reset Password
				</button>';

			$datos['data'][] = array(
				$i, 
				$item['nombre'].' '.$item['apellidos'],
				date("d-m-Y", strtotime($item['fechaRegistro'])),
				$item['usuario'],
				Cifrado::decifrar($item['correo']),
				TipoUsuario($item['rol']),
				$buttonResetear
			);
			$i++;
		}
		print json_encode($datos);
	}

	public function usuarioPorID(){

		$respuesta = UsuarioController::mostrarIDUsuarioController();

		$datos = array(
			"idUsuario"=> $respuesta["PK_idUsuario"],
			"nombre"=> $respuesta["nombre"],
			"apellidos"=> $respuesta["apellidos"],
			"correo"=> Cifrado::decifrar($respuesta["correo"]),
			"idUbicacion"=> $respuesta["FK_idUbicacion"],
			"telefono"=> $respuesta["telefono"],
			"rol" => $respuesta["rol"]);

		print json_encode($datos);
	}

	
	public function duplicadoUsuario(){

		$datosAjax = array("usuario"	=>	 $_POST["regUsuario"]);

		$respuesta = UsuarioController::duplicadoUsuarioController($datosAjax);

		$alerta = "";

		if ($respuesta == 0){


		} else{

			$alerta = "Usuario NO disponible";

		}

		print json_encode($alerta);

	}

	public function duplicadoCorreo(){

		$datosAjax = array("correo"	=>	 Cifrado::cifrar($_POST["regCorreo"]));

		$respuesta = UsuarioController::duplicadoCorreoController($datosAjax);

		$alerta = "";

		if ($respuesta == 0){
			
		}else{

			$alerta = "Correo NO disponible";

		}

		print json_encode($alerta);

	}


	public function guardarUsuario(){

		$salida = array('success' => false, 'mensaje' => array());

		// Verificar en el servidor que no exista usuarios ni correos duplicados.
		$usuario = array("usuario"	=>	 $_POST["regUsuario"]);
		$respuestaUsuario = UsuarioController::duplicadoUsuarioController($usuario);

		$correo = array("correo"	=>	 Cifrado::cifrar($_POST["regCorreo"]));
		$respuestaCorreo = UsuarioController::duplicadoCorreoController($correo);

		if ($respuestaUsuario == 0 AND $respuestaCorreo == 0){

			$datosAjax = array("ubicacion"	=>	$_POST["regUbicacion"],
							"nombre"	=>	$_POST["regNombre"],
							"apellido"	=>	$_POST["regApellido"],
							"usuario"	=>	$_POST["regUsuario"],
							"correo"	=>	Cifrado::cifrar($_POST["regCorreo"]),
							"telefono"	=>	$_POST["regTelefono"],
							"contrasena"	=>	password_hash("cdc2017", PASSWORD_DEFAULT),
							"rol"	=>	$_POST["regRol"],
							"activo"	=>	"1" );
		
			$respuesta = UsuarioController::guardarUsuarioController($datosAjax );

			if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "El usuario se ha registrado correctamente";

			}else{

				$salida['success'] = false;
				$salida['mensaje'] = "Error al registrar usuario";

			}

		} else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al registrar usuario, correo o nombre de usuario ya existe en el sistema";
		}		

		echo json_encode($salida);
	}


	public function editarUsuario(){
		
		$salida = array('success' => false, 'mensaje' => array());

		$datosAjax = array("id"		=> $_POST["editID"], 
						"nombre" 	=> $_POST["editNombre"],
						"apellidos" => $_POST["editApellido"],
						"correo" 	=> Cifrado::cifrar($_POST["editCorreo"]),
						"telefono" 	=> $_POST["editTelefono"],
						"rol" 		=> $_POST["editRol"],
						"idUbicacion" => $_POST["editUbicacion"]);

		$respuesta = UsuarioController::editarUsuarioController($datosAjax);

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Usuario actualizado correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar usuario";
		}

		echo json_encode($salida);

	}


	public function cambiarContrasena(){
		
		$salida = array('success' => false, 'mensaje' => array());

		$datosAjax = array("id"		=> $_POST["idUsuario"], 
							"contrasena"	=>	password_hash("cdc2017", PASSWORD_DEFAULT));

		$respuesta = UsuarioController::cambiarContrasenaController($datosAjax);

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Contraseña reseteada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al resetear la contraseña";
		}
		echo json_encode($salida);
	}

	public function changePassword(){
		
		//$salida = array('success' => false, 'mensaje' => array());

		//$datosAjax = array("id"		=> $_POST["idUsuario"], "contrasena"	=>	password_hash("cdc2017", PASSWORD_DEFAULT));

		$respuesta = UsuarioController::changePasswordController();

		/*
		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Contraseña actualizado correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error, verifica la contraseña actual";
		}*/
		echo json_encode($respuesta);
	}


	public function desactivarUsuario(){

		$salida = array('success' => false, 'mensaje' => array());
		
		$ticketsPendientes = UsuarioController::ticketsPendientesController();

		if ($ticketsPendientes == 0){

			$respuesta = UsuarioController::desactivarUsuarioController();

			if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Usuario desactivado correctamente";
			}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al desactivar usuario";
			}

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar usuario, verifica que no tenga tickets sin atender";
		}	

		echo json_encode($salida);
	}


	public function activarUsuario(){

		$salida = array('success' => false, 'mensaje' => array());		

		$respuesta = UsuarioController::activarUsuarioController();

		if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Se ha activado el usuario correctamente";
		}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al activar usuario";
		}		

		echo json_encode($salida);
	}

}


if($opcion == 'usuario'){
	$usuario = new AjaxUsuario();
	$usuario -> duplicadoUsuario();	
}


if($opcion == 'password'){
	$contrasena = new AjaxUsuario();
	$contrasena -> changePassword();
}

if($opcion == 'contrasena'){
	$contrasena = new AjaxUsuario();
	$contrasena -> cambiarContrasena();
}

if($opcion == 'sistemas'){
	$sistemas = new AjaxUsuario();
	$sistemas -> sistemas();	
}

if($opcion == 'correo'){
	$correo = new AjaxUsuario();
	$correo -> duplicadoCorreo();	
}

if($opcion == 'mostrar'){
	$mostrar = new AjaxUsuario();
	$mostrar -> mostrarUsuario();
}

if($opcion == 'getID'){
	$id = new AjaxUsuario();
	$id -> usuarioPorID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxUsuario();
	$guardar -> guardarUsuario();
}

if ($opcion == 'editar'){
	$editar = new AjaxUsuario();
	$editar -> editarUsuario();
}

if ($opcion == 'desactivar'){
	$desactivar = new AjaxUsuario();
	$desactivar -> desactivarUsuario();
}

if ($opcion == 'activar'){
	$activar = new AjaxUsuario();
	$activar -> activarUsuario();
}