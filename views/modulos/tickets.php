<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Inicio </title>
	<link rel="stylesheet" href="views/css/all.min.css">
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/responsive.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link rel="stylesheet" href="views/css/plugins/fileinput.min.css">
	<link rel="stylesheet" href="views/css/plugins/theme.min.css">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
			<div class="animated fadeIn">
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header d-flex justify-content-between">
									<span><i class="fa fa-ticket"></i> Tickets <span class="ml-3 text-secondary font-italic"> Para agregar archivo(s) al <strong>Ticket</strong> es necesario completar el registro y luego clic en: <strong><?php echo ($_SESSION['rol'] == 1) ? "Acciones/Subir Archivos... " : "Boton Agregar Archivo (Color Naranja) " ?></strong>Máx 3 archivos por registro</span></span>
									<div class="pull-right">
										<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#agregarTicket" id="btnAgregarTicket">
											<i class="fa fa-plus-circle"></i> Ticket
										</button>
									</div>
								</div>
								<div class="card-block">
									<table id="tickets" class="table table-striped table-vertical" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>No</th>
												<th>Actualizado</th>
												<th>Ref</th>
												<th>Categoria</th>
												<th data-priority="1">Requerimiento</th>
												<th>Status</th>
												<th>Prioridad</th>
												<th>Creado</th>
												<th>Asignado</th>
												<th>Acciones</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

<?php
				$tickets = new TicketsController();
				$tickets -> guardarArchivoController();
?>

				<!--Modal Registro-->
				<div class="modal fade" id="agregarTicket" tabindex="-1" role="dialog" aria-labelledby="registrarTicketModal" aria-hidden="true">
					<div class="modal-dialog modal-lg modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="registrarTicketModal">Agregar Ticket</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="post" id="frmregistrarTicket" enctype="multipart/form-data">
								<div class="modal-body">
									<!--
									<div class="alert alert-info" id="botonAF">
										<a class="" data-toggle="collapse" href="#btnactividadesFrecuentes" aria-expanded="false" aria-controls="btnactividadesFrecuentes">
										Actividades frecuentes <i class="fa fa-angle-double-right"></i>
										</a>
									</div>
									<div class="collapse" id="btnactividadesFrecuentes">
										<div class="card-block" id="btnActFre">
											<div class="row">
												<div class="col-md-8">
													<button type="button" class="btn btn-success btn-sm" id="cambioDomicilio"> <i class="fa fa-exchange"></i> Cambio </button>
													<button type="button" class="btn btn-warning btn-sm" id="facturas" > <i class="fa fa-file-text"></i> Timbrado </button>
													<button type="button" class="btn btn-primary btn-sm" id="toner" > <i class="fa fa-print"></i> Toner </button>
												</div>
												<div class="col-md-4">
													<button type="button" class="btn btn-danger btn-sm pull-right" id="limpiarCampos" ><i class="fa fa-close"></i> Cancelar </button>
												</div>
											</div>
										</div>
									</div>

									-->
									<hr>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">¿Es hijo?</span>
													<select class="form-control" id="regEshijo" name="regEshijo">
														<option value="0">No</option>
														<option value="1">Si</option>
													</select>
													&nbsp;&nbsp;
													<button type="button" class="btn btn-tool btn-sm" data-toggle="tooltip" data-placement="right" title="Si el ticket a generar depende de otro ya creado">
													<i class="fa fa-question"></i>
													</button>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group" id="referencia"> 
													<span class="input-group-addon">ID Referencia</span>
													<input type="text" id="regReferencia" name="regReferencia" class="form-control" placeholder="Referencia ticket padre">
													<button type="button" id="heredar" class="btn btn-primary btn-sm"><i class="fa fa-angle-double-right"></i></button>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Requerimiento</span>
													<select class="form-control" name="regCategoria" id="regCategoria" required>
														<option value="">Seleccione...</option>
														<?php
															$categoria = new CategoriaController();
															$categoria -> categoriaSeleccionController();
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group" id="idSubcategoria_">
													<span class="input-group-addon">Subcategoria</span>
													<select class="form-control" name="regSubcategoria" id="regSubcategoria" required>
													</select>
												</div>
											</div>
										</div>
									</div>
										
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Tema</span>
											<input type="text" id="regTema" name="regTema" class="form-control" placeholder="Titulo tema" required>
										</div>
									</div>
											
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Descripción</span>
											<textarea rows="3" id="regDescripcion" name="regDescripcion" class="form-control" placeholder="Nombre de usuario: Descripcion:" required></textarea>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Cliente</span>
													<select class="form-control" name="regCliente" id="regCliente" required>
														<option value="">Seleccione...</option>
														<?php
															$usuario = new UsuarioController();
															$usuario -> usuarioSeleccionController();
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group" id="idSucursal_">
													<span class="input-group-addon">Sucursal</span>
													<select class="form-control" name="regSucursal" id="regSucursal" required>
														<?php
															$tecnico = new UbicacionController();
															$tecnico -> ubicacionSeleccionController();
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Tipo de ticket</span>
													<select class="form-control" name="regTipo" id="regTipo" required>
														<option value="">Seleccione...</option>
														<option value="1">Servicio</option>
														<option value="2">Incidente</option>
														<option value="3">Requerimiento</option>
													</select>
												</div>
											</div>
										</div>
											
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Asignar a</span>
													<select class="form-control" name="regTecnico" id="regTecnico" required>
														<option value="">Seleccione...</option>
														<?php
															$tecnico = new UsuarioController();
															$tecnico -> tecnicoSeleccionController();
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Status</span>
													<select class="form-control" name="regStatus" id="regStatus" required>
														<option value="">Seleccione...</option>
														<option value="1">Abierto</option>
														<option value="2">Asignado</option>
														<option value="3">Pendiente</option>
														<option value="4">Cerrado</option>
														<option value="5">Cancelado</option>
														<option value="6">Resuelto</option>
														<!--<option value="7">Reabierto</option>-->
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Prioridad</span>
													<select class="form-control" name="regPrioridad" id="regPrioridad" required>
														<option value="">Seleccione...</option>
														<option value="1">Critica (1 hr)</option>
														<option value="2">Urgente (4 hr)</option>
														<option value="3">Alto (2 dias)</option>
														<option value="4">Medio (1 semana)</option>
														<option value="5">Bajo (3 meses)</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<!--
									<hr>									
									<label class="input-group-addon">Archivo(s)</label>
									<input class="file-loading" id="imagen" type="file" multiple name="image[]">
									<div id="errorBlock" class="help-block"></div> 
									-->
								</div>
								<div class="modal-footer">
									<button type="reset" class="btn btn-secondary" data-dismiss="modal" onclick="resetear()">Cerrar</button>
									<button type="submit" class="btn btn-primary" id="btnGuardar_">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div><!--Fin Modal Registro-->


				<!--Inicio Modal Editar-->
				<div class="modal fade" tabindex="-1" role="dialog" id="modalEditarTicket">
					<div class="modal-dialog modal-lg modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><span class="fa fa-edit"></span> Editar ticket</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="" id="frmEditarTicket">
								<div class="modal-body">
									<div class="form-group">
										<input type="hidden" id="editID" name="editID" class="form-control">
										<div class="input-group">
											<span class="input-group-addon">Tema</span>
											<input type="text" id="editTema" name="editTema" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Descripción</span>
											<textarea rows="3" id="editDescripcion" name="editDescripcion" class="form-control"></textarea>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Tipo de ticket</span>
													<select class="form-control" name="editTipo" id="editTipo">
														<option value="">Seleccione...</option>
														<option value="1">Servicio</option>
														<option value="2">Incidente</option>
														<option value="3">Requerimiento</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Escalado</span>
													<select class="form-control" name="editEscalado" id="editEscalado">
														<option value="">Seleccione...</option>
														<option value="1">Nivel 1</option>
														<option value="2">Nivel 2</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Status</span>
													<select class="form-control" name="editStatus" id="editStatus">
														<option value="">Seleccione...</option>
														<option value="1">Abierto</option>
														<option value="2">Asignado</option>
														<option value="3">Pendiente</option>
														<option value="7">Reabierto </option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Prioridad</span>
													<select class="form-control" name="editPrioridad" id="editPrioridad">
														<option value="">Seleccione...</option>
														<option value="1">Critica (1 hr)</option>
														<option value="2">Urgente (4 hr)</option>
														<option value="3">Alto (2 dias)</option>
														<option value="4">Medio (1 semana)</option>
														<option value="5">Bajo (3 meses)</option>
													</select>
												</div>
											</div>
										</div>
										
										<!-- -->
										<div class="col-md-6">
											<div class="form-group">
												<div class="input-group">
													<span class="input-group-addon">Asignado</span>
													<select class="form-control" name="editAsignado" id="editAsignado">
														<!-- TECNICO -->
														<?php
															$tecnico = new UsuarioController();
															$tecnico -> tecnicoSeleccionController();
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer modaleditarTicket">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary">Guardar cambios</button>
								</div>
							</form>
						</div>
					</div>
				</div><!-- Fin Modal Editar -->


				<!-- Eventos -->
				<div class="modal fade" id="registrarEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg modal-warning" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle"></i> Crear Evento</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="post" id="frmRegistrarEvento" enctype="multipart/form-data">
								<div class="modal-body">
									<div class="messages"></div>
									<div class="pull-right nnn" id="nxy"></div>
									<br>
									<div id="requerimiento"></div>
									
									<div class="form-group" style="display:none">
										<div class="input-group">
											<span class="input-group-addon">Ticket No</span>
											<input type="text" id="NoTicket" name="NoTicket" class="form-control" placeholder="NoTicket" required readonly>
										</div>
									</div>
									
									<div class="form-group" style="display:none">
										<div class="input-group">
											<span class="input-group-addon">Tema</span>
											<input type="text" id="eTema" name="eTema" class="form-control" placeholder="Titulo tema" required readonly>
										</div>
									</div>
											
									<div class="form-group" style="display:none">
										<div class="input-group">
											<span class="input-group-addon">Descripción</span>
											<textarea rows="2" id="eDescripcion" name="eDescripcion" class="form-control" placeholder="Descripcion" required readonly></textarea>
										</div>
									</div>
								
									<div class="form-group">
										<textarea rows="5" id="eEvento" name="eEvento" class="form-control" placeholder="Describe tu evento aquí..." required></textarea>
									</div>
								
									<!--
									<br>
							
									<label class="input-group-addon">Archivo(s)</label>
									<input class="file-loading" id="imagenE" type="file" multiple name="image[]">
									<div id="errorBlockE" class="help-block"></div> 
									-->
								
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary" id="btnGuardarEvento_">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- Fin Eventos -->

				<!-- Registrar archivos -->
				<div id="registrarArchivo" class="modal fade">
					<div class="modal-dialog modal-md" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Archivos del Ticket  (Solo .pdf, .jpg y/o .png)</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form id="frmRegistrarArchivos" enctype="multipart/form-data">
									<div class="row">
										<div class="col-12">
											<input type="hidden" id="idTicket" name="idTicket">
											<!--<input type="text" id="marcaProductoPDF" name="marcaProductoPDF">-->
											<div class="form-group">
												
												<input type="file" name="archivosTicket" class="form-control" id="archivosTicket" multiple="multiple">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="text-right">
											<button type="submit" class="btn btn-primary"> Subir Archivo(s)	</button>
										</div>
									</div>
								</form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>
	<script src="views/js/libs/jquery.dataTables.min.js"></script>	
	<script src="views/js/libs/dataTables.bootstrap4.min.js"></script>
	<script src="views/js/libs/dataTables.responsive.min.js"></script>
	<script src="views/js/libs/responsive.bootstrap4.min.js"></script>

	<script src="views/js/libs/piexif.min.js"></script>
	<script src="views/js/libs/fileinput.min.js"></script>
	<script src="views/js/libs/theme.js"></script>
	<script src="views/js/libs/themefas.js"></script>
	<script src="views/js/libs/locales/es.js"></script>

	<script src="views/js/app.js"></script>
	<script src="views/js/tickets.js"></script> 
</body>
</html>

