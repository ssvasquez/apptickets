var tblUbicacion;

/*	=======	M O S T R A R Ubicacion	=======*/ 
tblUbicacion = $("#ubicacion").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/ubicacionAjax.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{	responsivePriority: 1, targets: 0	},
		{	responsivePriority: 2, targets: 0	}
	]
});


/*	=======	A G R E G A R  Ubicacion	=======*/
$("#btnAgregarUbicacion").on('click', function () {

	$("#frmRegistrarUbicacion")[0].reset();

	$("#frmRegistrarUbicacion").unbind('submit').bind('submit', function () {

		var form = $(this);
		var nuevaUbicacion = $("#altaUbicacion").val();

		if (nuevaUbicacion){
			$.ajax({
				url: "views/ajax/ubicacionAjax.php?op=guardar",
                type: form.attr('method'),
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
					if (response.success == true){
						$("#frmRegistrarUbicacion")[0].reset();
						tblUbicacion.ajax.reload(null, false);
						$("#agregarUbicacion").modal('hide');
						successSwal(response.mensaje);
					}else{
						$("#agregarUbicacion").modal('hide');
						errorSwal(response.mensaje);
					}
				}
			});
		}
		return false;
	});
});


/*	=======	E D I T A R Ubicacion	=======*/
function editarUbicacion(idUbicacion) {
	if (idUbicacion) {
		
		// Buscar datos de la categoria
		$.ajax({
			url: "views/ajax/ubicacionAjax.php?op=getID",
			type: 'POST',
			data: {
				idUbicacion: idUbicacion
			},
			dataType: 'json',
			success: function (response) {

				// Se almacena el valor del registro en input e // ID Categoria para tomar referencia 
				$("#ubicacionEditar").val(response.ubicacionDesc);
				$("#idUbicacionEditar").val(response.PK_idUbicacion);

				$("#frmEditarUbicacion").unbind('submit').bind('submit', function () {
					var form = $(this);
					// Capturamos lo que tiene la caja de texto.
					var ubicacionEdicion = $("#ubicacionEditar").val();

					if (ubicacionEdicion) {
						$.ajax({
							url: "views/ajax/ubicacionAjax.php?op=editar",
							type: form.attr('method'), // Es lo mismo a poner POST
							data: form.serialize(),
							dataType: 'json',
							success: function (response) {

								if (response.success == true) {

									tblUbicacion.ajax.reload(null, false);
									$("#modalEditarUbicacion").modal('hide');
									successSwal(response.mensaje);

								} else {
									//Cerrar ventana y mostrar mensaje de error.
									$("#modalEditarUbicacion").modal('hide');
									errorSwal(response.mensaje);
								}
							}
						});
					}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	A C T I V A R  Ubicacion	=======*/
function activarUbicacion(idUbicacion = null){
	if(idUbicacion){
		swal({
			title: "¿Realmente desea A C T I V A R esta ubicacion?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ubicacionAjax.php?op=activar",
					type: 'post',
					data: {idUbicacion : idUbicacion},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblUbicacion.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Ubicacion	=======*/
function desactivarUbicacion(idUbicacion = null){
	if(idUbicacion){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta ubicación?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ubicacionAjax.php?op=desactivar",
					type: 'post',
					data: {idUbicacion : idUbicacion},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblUbicacion.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}

/** Mensajes de Acierto y Error. Aplica area de Categoria y Subcategoria */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}