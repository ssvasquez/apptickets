<?php 

class KBController{


	public static function referenciaController(){

		$respuesta = KBModel::referenciaModel("ticket", $_POST['idTicket']);

		return $respuesta;
	}

	public static function getIDController(){

		$respuesta = KBModel::getIDModel("kb", $_POST['idKB']);

		return $respuesta;
	}


	public static function mostrarKBController(){

		$respuesta = KBModel::mostrarKBModel("kb");
		$datos = array('data' => array());
		
		$i = 1;
		foreach ($respuesta as $row => $item){

			$actionButton ='
				<div class="btn-group">
					<button type="button" class="btn btn-secondary">Acciones</button>
					<button type="button" class="btn btn-secondary btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">    
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#modaleditarKB"  onclick="editarKB('.$item['PK_idKB'].')"> <span class="fa fa-check"></span> Editar Solucion</a>
						<a class="dropdown-item" href="#" onclick="desactivarKB('.$item['PK_idKB'].')"> <span class="fa fa-trash"></span> Eliminar</a>

						
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#registrarPDF"  onclick="agregarPDF('.$item['PK_idKB'].')"> <span class="fas fa-file-pdf"></span> Agregar Manual</a>
					</div>
				</div>';
			

			$categoria = '<li><strong>'.$item['categoriaDesc'].'</strong><ul><li style="list-style-type:square">'.$item['subcategoriaDesc'].'</li></ul></li>';

			$pdf = "";

			if (trim($item['archivo']) != ""){

				$pdf = '<a class="lead" style="font-size:2rem" href="views/manualesKB/'.$item['archivo'].'" target="_blank"><i class="fas fa-file-pdf" aria-hidden="true"></i> 
				</a>';
			}

			$link = "";
			if (trim($item['link']) != ""){

				$link = '<a class="lead" style="font-size:2rem" href="'.$item['link'].'" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>
				</a>';
			}

			$datos['data'][] = array(
				$i, 
				'<span class="badge badge-primary">'.$item['FK_idTicket'].'</span>',
				$categoria,
				$item['casoKB'],
				$item['solucionKB'],
				$pdf,
				$link,
				$actionButton
			);

			$i++;
		}
		
		//Mandar un array a AJAX;
		return $datos;
	}
	

	static function MostrarModal(){
		// $item['solucionKB']
		return '<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				</div>
			</div>
		</div>';
	}


	public static function guardarKBController(){

		if(isset($_POST["regSubcategoria"])){

			$ab = $_POST["regCaso"].": ".$_POST["regDescripcion"];

			$datosController = array(
				"idTicket"=>	$_POST['regID'],
				"subcategoriaKB" => $_POST["regSubcategoria"],
				"casoKB" => 	$ab,
				"solucionKB" => $_POST["regSolucion"],
				"link" => 	$_POST["regLink"]
			);

			$respuesta = KBModel::guardarKBModel($datosController, "kb");
		
			return $respuesta;

		}		
    }

	public static function editarKBController(){

		$datosController = array("id" => $_POST['idKB'],
								"solucion" => $_POST["editSolucion"]);

		$respuesta = KBModel::editarKBModel($datosController, "kb");

		return $respuesta;
	}


	public static function desactivarKBController(){
			
		$datosController = array("id" => $_POST['idKB']);

		$respuesta = KBModel::desactivarKBModel($datosController, "kb");

		return $respuesta; 

	}
	

	public static function guardarPDFController(){
		if (isset($_POST["idKBPDF"])){

			if(isset($_FILES["pdf"]["tmp_name"])){

				$pdf = $_FILES["pdf"]["tmp_name"];

				$directorio = '../manualesKB/';

				// Borrar Lineas (directorio creado por defecto)
				if (!file_exists($directorio)) {
					mkdir($directorio, 0777, true);
				}
				
				$archivo = $directorio.'/'. $_FILES['pdf']['name'];

				move_uploaded_file($pdf, $archivo);

				$nombreArchivo = basename($archivo);

				$datosController = array("id" => $_POST['idKBPDF'],
								"archivo" => $nombreArchivo);

				$respuesta = KBModel::guardarPDF($datosController, "kb");

				return $respuesta;
			}
		}
	}
}