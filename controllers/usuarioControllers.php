<?php 
if(!isset($_SESSION)){
session_start();
}
class UsuarioController{


	public static function mostrarUsuarioController(){

		$respuesta = UsuarioModel::mostrarUsuarioTodosModel("usuario");

		return $respuesta;
	}
	
	public static function areaSistemasController(){

		$respuesta = UsuarioModel::areaSistemasModel("usuario");

		return $respuesta;
	}


    public static function mostrarIDUsuarioController(){

		$idUsuario = $_POST['idUsuario'];
		
		$respuesta = UsuarioModel::mostrarIDUsuarioModel('usuario', $idUsuario);
		
        return $respuesta;
    }


	public static function guardarUsuarioController($datosController){

			$respuesta = UsuarioModel::guardarUsuarioModel($datosController, "usuario");		
			
			return $respuesta;
	}
	

	public static function duplicadoUsuarioController($datosController){

		$respuesta = UsuarioModel::duplicadoUsuarioModel($datosController, "usuario");

		return $respuesta;

	}

	public static function duplicadoCorreoController($datosController){

		$respuesta = UsuarioModel::duplicadoCorreoModel($datosController, "usuario");

		return $respuesta;

	}


	public static function editarUsuarioController($datosController){

		$respuesta = UsuarioModel::editarUsuarioModel($datosController, "usuario");

		return $respuesta;
	}


	public static function cambiarContrasenaController($datosController){

		$respuesta = UsuarioModel::cambiarContrasenaModel($datosController, "usuario");

		return $respuesta;
	}


	public static function desactivarUsuarioController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idUsuario'],
									"estatus" => '0');

            $respuesta = UsuarioModel::editarEstatusUsuarioModel($datosController, "usuario");

			return $respuesta; 

        //}

    }
	
	
    public static function ticketsPendientesController(){

		$datosController = array("id" => $_POST['idUsuario']);

		$respuesta = UsuarioModel::ticketsPendientesModel($datosController, "ticket_usuario");

		return $respuesta;
	}


	public static function totalAdminController(){

		$datosController = array("id" => $_POST['idUsuario']);

		$respuesta = UsuarioModel::totalAdminModel($datosController, "usuario");

		return $respuesta;
	}
	

	public static function activarUsuarioController(){

		$datosController = array("id" => $_POST['idUsuario'],
								 "estatus" => '1');

		$respuesta = UsuarioModel::editarEstatusUsuarioModel($datosController, "usuario");

		return $respuesta;
	}

	
	public static function tecnicoSeleccionController(){

		$respuesta = UsuarioModel::mostrarTecnicosModel("usuario");

		foreach ($respuesta as $row => $item){

			echo '<option value="'.$item["PK_idUsuario"].'">'.$item["nombre"].' '.$item["apellidos"].'</option>';

		}

	}
	

	public static function usuarioSeleccionController(){

		$respuesta = UsuarioModel::mostrarUsuariosModel("usuario");

		foreach ($respuesta as $row => $item){

			echo '<option value="'.$item["PK_idUsuario"].'">'.$item["nombre"].' '.$item["apellidos"].'</option>';

		}
		
	}


	public static function changePasswordController(){

		$validator = array('success' => false, 'messages' => array());

		$id = $_SESSION['id'];

		$respuesta = UsuarioModel::mostrarIDUsuarioModel("usuario", $id);

		if(password_verify($_POST['pswdActual'], $respuesta['contrasena'])){

			if($_POST['pswdNueva'] == $_POST['pswdConfirmarNueva']){

				$nuevaContrasena = $regContrasena = password_hash($_POST['pswdNueva'], PASSWORD_DEFAULT);

				$datosController = [
					"contrasena" => $nuevaContrasena,
					"id" => $id
				];

				$resultado = UsuarioModel::cambiarContrasenaModel($datosController, "usuario");

				$validator['success'] = true;
				$validator['messages'] = "Contraseña actualizado correctamente.";
				sleep(1);
				session_destroy();
			}else{
				$validator['success'] = false;
				$validator['messages'] = "Las contraseñas no coinciden!!";
			}

		}else{

			$validator['success'] = false;
			$validator['messages'] = "Error, verifica la contraseña actual";
		}
		
		return $validator;
		
	}

}