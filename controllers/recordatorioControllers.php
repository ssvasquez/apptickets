<?php 

class RecordatorioController{

    public static function mostrarRecordatorioController(){
        
        $res = RecordatorioModel::mostrarRecordatorioModel( "recordatorio", $_SESSION['id']);

        $totalRegistros = count($res);

        if($totalRegistros > 0)
        {

			$pageNum = 1;

			$rowsPerPage = 10;

			if(isset($_GET['page'])) {

				$pageNum = $_GET['page'];

			}

			$offset = ($pageNum - 1) * $rowsPerPage;

			$total_paginas = ceil($totalRegistros / $rowsPerPage);

			$datosController = [
				"id" => $_SESSION['id'],
				"offset" => $offset,
				"rowsPerPage" => $rowsPerPage 
			];

			$respuesta = RecordatorioModel::mostrarRecordatorio2Model($datosController, "recordatorio");

			$x=1;

			foreach ($respuesta as $row => $item)
			{
				if($x%2==0){
					echo '
					<ul class="notes">
						<li>
							<div class="rotate-2" style="background:'.$item['color'].'">
							<p style="border:dotted 1px">'.$item['fechaVencimiento'].'</p>
							<p>'.$item['descripcion'].'</p>
							<p class="creado">Creado: '.$item['fechaInicio'].'</p>												
							<a href="#" data-toggle="modal" data-target="#modaleliminarRecordatorio" type="button" title="Eliminar" onclick="eliminarRecordatorio('.$item['PK_idRecordatorio'].')" class="pull-right"><i class="fa fa-trash "></i></a>											
							<a href="#" data-toggle="modal" data-target="#modaleditarRecordatorio" type="button" title="Editar" onclick="editarRecordatorio('.$item['PK_idRecordatorio'].')" class="pull-right"><i class="fa fa-pencil "></i></a>											
							<br>
							</div>
						</li>
					</ul>';

				}else{

					echo'	
					<ul class="notes">
						<li>
							<div class="rotate-1" style="background:'. $item['color'].'">
							<p style="border:dotted 1px">'.$item['fechaVencimiento'].'</p>
							<p class="test">'.$item['descripcion'].'</p>
							<p class="creado">Creado: '.$item['fechaInicio'].'</p>												
							<a href="#" data-toggle="modal" data-target="#modaleliminarRecordatorio" type="button" title="Eliminar" onclick="eliminarRecordatorio('.$item['PK_idRecordatorio'].')" class="pull-right"><i class="fa fa-trash "></i></a>
							<a href="#" data-toggle="modal" data-target="#modaleditarRecordatorio" type="button" title="Editar" onclick="editarRecordatorio('.$item['PK_idRecordatorio'].')" class="pull-right"><i class="fa fa-pencil "></i></a>
							<br>
							</div>
						</li>
					</ul>';

				}

				$x++;
			}

			if($total_paginas > 1){	
				echo '<div class="col-lg-12" >';
				echo '<div class="float-right">';
				echo '<nav>';
					echo '<ul class="pagination">';
						if($pageNum != 1)
							echo '<li class="page-item"><a class="page-link" data="'.($pageNum-1).'">
									<span aria-hidden="true">&laquo;</span>
									Anterior
								</a></li>';
						for($i=1; $i<=$total_paginas; $i++){
							if($pageNum == $i){
								echo '<li class="page-item active"><a class="page-link">'.$i.'</a></li>';
							}
							else{
								echo '<li class="page-item"><a class="page-link" data="'.$i.'">'.$i.'</a></li>';
							}
						}
						if($pageNum != $total_paginas)
							echo '<li class="page-item"><a class="page-link" data="'.($pageNum+1).'">Siguiente
								<span aria-hidden="true">&raquo;</span>
								</a></li>';
					echo '</ul>';
				echo '</nav>';
				echo '</div>';
				echo '</div>';
			}	

        }

        

        
    }    
}