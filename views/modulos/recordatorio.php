<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Recordatorio </title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<i class="fa fa-list-alt "></i> Recordatorios
									<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#registrarRecordatorio" id="btnagregarRecordatorio">
										<i class="fa fa-plus-circle"></i> Recordatorio
									</button>
								</div>
								<div class="card-block" id="recargar">
									<div class="row" id="content">
										<?php //require("../includes/recordatorios.php");
											$recordatorio = new RecordatorioController();
											$recordatorio -> mostrarRecordatorioController();
										?>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>


		<!--Modal Registro-->
		<div class="modal fade" id="registrarRecordatorio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-primary" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle"></i> Crear recordatorio</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="post" id="frmregistrarRecordatorio" enctype="multipart/form-data">
						<div class="modal-body">
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Descripción</span>
								<textarea rows="3" id="regDescripcion" name="regDescripcion" class="form-control" placeholder="Descripcion" required></textarea>
							</div>
						</div>
						
						<div class="row">	
						<!--
							<div class="col-md-6" style="display:none">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<span class="fa fa-calendar"> </span> Inicio 
										</span>
										<input type="text" class="form-control" id="datetimepicker4" name="fechaInicial" required>								
									</div>
								</div>
							</div>-->
							
							<div class="col-md-6">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<span class="fa fa-calendar"> </span> Vencimiento 
										</span>
										<input type="text" class="form-control" id="datetimepicker5" name="fechaFinal" required>										
									</div>
								</div>
							</div>					
						</div>
						<!--
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Color</span>
								<select class="form-control" name="colorNota" id="colorNota" required>
									<option value="#FEF3B5">Amarillo</option>
									<option value="#C6EFC3">Verde</option>
									<option value="#CAE8FF">Azul</option>
									<option value="#FFC7F6">Rosa</option>
								</select>
							</div>
						</div>	
						-->		
						
						<div class="form-group row">
						<span class="input-group-addon">Color</span>							
							<div class="col-md-9">
								<label class="radio-inline" for="inline-radio1">
									<input type="radio" id="inline-radio1" name="colorNota" value="#FEF3B5" checked>
									<i class="fa fa-circle" style="color:#FEF3B5; font-size: 2rem;"></i>&nbsp;&nbsp;
								</label>&nbsp;&nbsp;
								<label class="radio-inline" for="inline-radio2">
									<input type="radio" id="inline-radio2" name="colorNota" value="#CAE8FF">
									<i class="fa fa-circle" style="color:#CAE8FF; font-size: 2rem;"></i>&nbsp;&nbsp;
								</label>&nbsp;&nbsp;
								<label class="radio-inline" for="inline-radio3">
									<input type="radio" id="inline-radio3" name="colorNota" value="#C6EFC3">
									<i class="fa fa-circle" style="color:#C6EFC3; font-size: 2rem;"></i>&nbsp;&nbsp;
								</label>&nbsp;&nbsp;
								<label class="radio-inline" for="inline-radio4">
									<input type="radio" id="inline-radio4" name="colorNota" value="#FFC7F6">
									<i class="fa fa-circle" style="color:#FFC7F6; font-size: 2rem;"></i>&nbsp;&nbsp;
								</label>
							</div>
						</div>
					
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary"> Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div><!--Fin Modal -->	

		<div class="modal fade" id="modaleditarRecordatorio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-primary" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-edit"></i> Editar Recordatorio</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="" id="frmeditarRecordatorio">
						<div class="modal-body">							
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon">Descripción</span>
									<textarea rows="3" id="editDescripcion" name="editDescripcion" class="form-control"></textarea>
								</div>
							</div>							
						</div>
						<div class="modal-footer modaleditarRecordatorio">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<button type="submit" class="btn btn-primary">Guardar cambios</button>
						</div>
					</form>
				</div>
			</div>
		</div> <!-- Fin modal Editar Recordatorio-->




	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>
	

	<script src="views/js/app.js"></script>
	<script src="views/js/recordatorio.js"></script>

</body>
</html>

