<?php
/*
* Referencia: http://php.net/manual/es/function.time.php
* Una función de diferencia de tiempo que genera el tiempo transcurrido al estilo de facebook: 
* hace 20 segundos, hace 1 día, hace 4 meses, ect...
* Calcula la diferencia en el tiempo entre una fecha pasada y una fecha futura.*
*/

function nicetime($date)
	{
		if(empty($date)) {
			return "No hay fecha proporcionada";
		} 
		
		$periods         = array("segundo", "minuto", "hora", "día", "semana", "mes", "año", "decada");
		$lengths         = array("60","60","24","7","4.35","12","10");   
		$now             = time();
		$unix_date         = strtotime($date);   
		#Checar que la fecha sea válida, sino que mande un string de error
		if(empty($unix_date)) {   
			return "Error";
		}

		// Es una fecha futura o pasada
		if($now > $unix_date) {   
			$difference     = $now - $unix_date;
			$tense         = "hace";
		} else {
			$difference     = $unix_date - $now;
			$tense         = "hace";
		} 
		
		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}   
			$difference = round($difference);
			   
		if($difference > 1  and $difference < 5256000) {
			$periods[$j].= "s";
		}else{
			$periods[$j].= "es";
		}		
		return "{$tense} $difference $periods[$j] ";
	}
	
	
	// Estatus
	function Estatus($arg){
		switch($arg){
			case 1: 
				$estatus = 'Abierto';
				break;
			case 2: 
				$estatus = 'Asignado';
				break;
			case 3: 
				$estatus = 'Pendiente';
				break;
			case 4: 
				$estatus =  'Cerrado';
				break;
			case 5: 
				$estatus = 'Cancelado';
				break;
			case 6: 
				$estatus = 'Resuelto';
				break;
			case 7: 
				$estatus = 'Reabierto';
				break;
		}
		return $estatus;
	}

	// Tipo
	function Tipo($arg){
		switch($arg){
			case 1:
				$tipoTicket =  "Servicio";
				break;			
			case 2:
				$tipoTicket =  "Incidente";
				break;
			case 3:
				$tipoTicket =  "Problema";
				break;			
		}
		return $tipoTicket;
	}
	
	// Nivel
	function Nivel($arg){
		switch($arg){
			case 1:
				$nivel = "Nivel 1";
				break;				
			case 2:
				$nivel = "Nivel 2";
				break;
		}
		return $nivel;
	}
		
	// Prioridad
	function Prioridad($arg)
	{
		switch($arg){
			case 1: 
				$prioridad = 'Critica';
				break;
			case 2: 
				$prioridad = 'Urgente';
				break;
			case 3: 
				$prioridad = 'Alto';
				break;
			case 4: 
				$prioridad = 'Medio';
				break;
			case 5: 
				$prioridad = 'Bajo';
				break;
		}
		return $prioridad;
	}

	/* Prioridades con etiquetas
	function PrioridadBadge($arg)
	{
		switch($arg){
			case 1: 
				$prioridadb = '<span class="badge badge-critica">Critica</span>';
				break;
			case 2: 
				$prioridadb = '<span class="badge badge-urgente">Urgente</span>'; 
				break;
			case 3: 
				$prioridadb = '<span class="badge badge-alto">Alto</span>';
				break;
			case 4: 
				$prioridadb = '<span class="badge badge-medio">Medio</span>'; 
				break;
			case 5: 
				$prioridadb = '<span class="badge badge-bajo">Bajo</span>';
				break;
		}
		return $prioridadb;
	}
	*/

	function TipoUsuario($arg){
		$usuario ="";
		switch($arg){
			case 1:
				$usuario = 'Administrador';
				break;
			case 2:
				$usuario = 'Técnico';
				break;
			case 3:
				$usuario = 'Usuario';
				break;
		}
		return $usuario;
	}
	

	function FechaCreado($arg){
		return date_format($arg, "d/m/Y") . " &nbsp;&nbsp; ". date_format($arg, "g:i A");
	}
	
	
	function Fecha($arg){
		$date = date_create($arg);
		$fechaResta = date_format($date,'Y-m-d H:i:s');
		$result = nicetime($fechaResta);
		$date1= date_format($date, 'Y-m-d');
		$hora= date_format($date, 'H:i:s');
		
		$strFecha ='<li style = "font-weight: bold">'.$date1.'</li>
		<li style="font-style:italic">'.$hora.'</li>
		<small class="text-muted pull-right">'.$result.'</small>';		
		return $strFecha;		
	}
?>