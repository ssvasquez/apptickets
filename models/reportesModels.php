<?php

	require_once "db.php";

	class ReporteModels{

		public static function reporte1Model($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT nombre, apellidos, usuario, telefono, rol, fechaRegistro, activo from $tabla where fechaRegistro between :fechaInicio and :fechaFinal");
			
			$stmt -> bindParam(":fechaInicio", $datosModel["inicio"], PDO::PARAM_STR);
			$stmt -> bindParam(":fechaFinal", $datosModel["final"], PDO::PARAM_STR);

            $stmt -> execute();
            
            return $stmt -> fetchAll();

            $stmt -> close();
		}
		

		public static function reporte2Model($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT ticket.PK_idTicket, ticket.cierre, ticket.tituloTicket, ticket.descripcionTicket, ticket.creado, ticket.statusTicket, ticket.prioridadTicket, ticket.alertaTicket, ticket.escaladoTicket, ticket.refTicket, ticket.FK_idSubcategoria, ticket.FK_idUbicacion, subcategoria.subcategoriaDesc, categoria.categoriaDesc, ubicacion.PK_idUbicacion, ubicacion.ubicacionDesc FROM $tabla INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN ubicacion ON ticket.FK_idUbicacion = ubicacion.PK_idUbicacion INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria where creado between :fechaInicio and :fechaFinal");
			
			$stmt -> bindParam(":fechaInicio", $datosModel["inicio"], PDO::PARAM_STR);
			$stmt -> bindParam(":fechaFinal", $datosModel["final"], PDO::PARAM_STR);

            $stmt -> execute();
            
            return $stmt -> fetchAll();

            $stmt -> close();
		}
    }
?>