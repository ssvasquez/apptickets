<?php 

class ReportesController{

    public static function reporte1Controller(){

		$datosController = array ("inicio"	=>	date('Y-m-d', strtotime($_POST['start_date'])),
								  "final"	=>	date('Y-m-d', strtotime($_POST['end_date'])));

        $respuesta = ReporteModels::reporte1Model($datosController, "usuario");
        
        return $respuesta;
	}

	public static function reporte2Controller(){

		$datosController = array ("inicio"	=>	date('Y-m-d', strtotime($_POST['start_date'])),
								  "final"	=>	date('Y-m-d', strtotime($_POST['end_date'])));
								  
        $respuesta = ReporteModels::reporte2Model($datosController, "ticket");
        
        return $respuesta;
	}
}