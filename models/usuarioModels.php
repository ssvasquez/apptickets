<?php 
//session_start();
require_once "db.php";

class UsuarioModel{

	public static function mostrarUsuarioTodosModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT usuario.PK_idUsuario,usuario.nombre,usuario.apellidos, usuario.fechaRegistro, usuario.correo, usuario.telefono,usuario.rol, usuario.activo, ubicacion.ubicacionDesc FROM $tabla INNER JOIN ubicacion ON usuario.FK_idUbicacion = ubicacion.PK_idUbicacion ORDER BY usuario.activo DESC");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}

	public static function areaSistemasModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT usuario.PK_idUsuario,usuario.nombre,usuario.apellidos,usuario.usuario,usuario.fechaRegistro, usuario.correo,usuario.telefono,usuario.rol FROM $tabla WHERE activo = 1 AND(usuario.rol = 1 or usuario.rol = 2 )");
	
		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function cambiarContrasenaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET contrasena = :contrasena WHERE PK_idUsuario = :id");

		$stmt -> bindParam(":contrasena", $datosModel["contrasena"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
		
		return $stmt->execute();

		$stmt->close();
	}


	public static function mostrarUsuariosModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT usuario.PK_idUsuario, usuario.nombre, usuario.apellidos FROM $tabla WHERE activo = 1 ORDER BY usuario.nombre");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

	}


	public static function mostrarTecnicosModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT usuario.PK_idUsuario, usuario.nombre, usuario.apellidos FROM usuario WHERE usuario.rol <> 3  AND usuario.activo = 1");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

	}


	public static function mostrarIDUsuarioModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE PK_idUsuario = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();
	}


	public static function duplicadoUsuarioModel($datosModel, $tabla){
		
		$stmt = Conexion::conectar()->prepare("SELECT COUNT(usuario) FROM $tabla WHERE usuario = :usuario");

		$stmt -> bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchColumn();

		$stmt -> close();
	}

	public static function duplicadoCorreoModel($datosModel, $tabla){
		
		$stmt = Conexion::conectar()->prepare("SELECT  COUNT(correo) FROM $tabla WHERE correo = :correo");

		$stmt -> bindParam(":correo", $datosModel["correo"], PDO::PARAM_STR);
		
		$stmt -> execute();

		return $stmt -> fetchColumn();

		$stmt -> close();
	}


	public static function guardarUsuarioModel($datosModel, $tabla){

		//$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (ubicacionDesc, activo) VALUES (:ubicacion, :activo)");
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (FK_idUbicacion, nombre, apellidos, usuario, correo, telefono, contrasena, rol, activo) VALUES (:ubicacion, :nombre, :apellido, :usuario, :correo, :telefono, :contrasena, :rol, :activo)");

		$stmt -> bindParam(":ubicacion", $datosModel["ubicacion"], PDO::PARAM_INT);
		$stmt -> bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datosModel["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":correo", $datosModel["correo"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono", $datosModel["telefono"], PDO::PARAM_STR);
		$stmt -> bindParam(":contrasena", $datosModel["contrasena"], PDO::PARAM_STR);
		$stmt -> bindParam(":rol", $datosModel["rol"], PDO::PARAM_INT);
		$stmt -> bindParam(":activo", $datosModel["activo"], PDO::PARAM_INT);		

		return $stmt->execute();

		$stmt->close();
	}


	public static function editarUsuarioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, apellidos = :apellidos, correo = :correo, telefono = :telefono, rol = :rol, FK_idUbicacion = :idUbicacion WHERE usuario.PK_idUsuario = :id");
		
		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
		$stmt -> bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellidos", $datosModel["apellidos"], PDO::PARAM_STR);
		$stmt -> bindParam(":correo", $datosModel["correo"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono", $datosModel["telefono"], PDO::PARAM_STR);
		$stmt -> bindParam(":rol", $datosModel["rol"], PDO::PARAM_INT);
		$stmt -> bindParam(":idUbicacion", $datosModel["idUbicacion"], PDO::PARAM_INT);
		
		return $stmt->execute();

		$stmt->close();
	}



	public static function totalAdminModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT COUNT(PK_idUsuario) FROM usuario WHERE PK_idUsuario <> :id and rol = 1 and activo = 1 ");

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetchColumn();

		$stmt->close();
	}


	public static function ticketsPendientesModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT COUNT(FK_idTicket) FROM $tabla INNER JOIN ticket ON ticket_usuario.FK_idTicket = ticket.PK_idTicket WHERE FK_idUsuario = :id AND ticket_usuario.status = 'ATENCION' and (ticket.statusTicket = 1 OR ticket.statusTicket = 2 OR ticket.statusTicket = 3 OR ticket.statusTicket = 7)");

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> execute();
            
		return $stmt -> fetchColumn();

		$stmt->close();
	}


	public static function editarEstatusUsuarioModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET activo = :estatus WHERE PK_idUsuario = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
		$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}



}