<?php

	require_once "db.php";

	class IngresoModels{

		public static function ingresoModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT PK_idUsuario, nombre, apellidos, telefono, usuario, correo, contrasena, rol FROM $tabla WHERE usuario = :usuario and activo = 1");

			$stmt -> bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR);
			
			$stmt -> execute();

			return $stmt -> fetch();

			$stmt -> close();
		}
	}
?>