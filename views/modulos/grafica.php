        <div class="animated fadeIn">
            <div class="row mt-1">
                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon bg-info mr-0"><i class="fa fa-ticket"></i></span>
                        <div class="mini-stat-info float-right">
                            <span>
                                <?php
									$tickets = new GraficaController();
									$tickets -> contarTicketsController();
								?>
                            </span> TICKETS
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon bg-danger mr-0"><i class="fa fa-user"></i></span>
                        <div class="mini-stat-info float-right">
                            <span>
                                <?php
									$usuario = new GraficaController();
									$usuario -> contarUsuarioController();
								?>
                            </span> USUARIOS
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon bg-success mr-0"><i class="fa fa-list-alt"></i></span>
                        <div class="mini-stat-info float-right">
                            <span>
                                <?php
									$categoria = new GraficaController();
									$categoria -> contarCategoriaController();
								?>
                            </span> CATEGORIAS
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-6 col-xl-3">
                    <div class="mini-stat clearfix">
                        <span class="mini-stat-icon bg-primary mr-0"><i class="fa fa-sticky-note"></i></span>
                        <div class="mini-stat-info float-right">
                            <span>
                                <?php
									$evento = new GraficaController();
									$evento -> contarEventoController();
								?>
                            </span> EVENTOS
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> <!-- Fila para impirmir estadisticas de tecnicos -->
                <?php
                    $prioridad = new GraficaController();
                    $prioridad -> estadisticasTecnicoController();
                ?>
            </div> <!-- Fin fila estadisticas tecnico -->


            <!-- Graficas -->
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div id="graficames"></div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div id="graficaestatus"></div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div id="graficatipo"></div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="card">
                        <div id="graficasubcategoria"></div>
                    </div>
                </div>
            </div> <!-- Fin area de graficas -->


            <!-- Area de Datatables -->
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-block">
                            <h6 class="ml-2 text-primary text-center">Tickets creados por usuario</h6>
                            <table id="" class="table table-striped dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $prioridad = new GraficaController();
                                    $prioridad -> ticketsPorUsuarioController();
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-block">
                            <h6 class="ml-2 text-primary text-center">Tickets creados por sucursal</h6>
                            <table id="" class="table table-striped dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $prioridad = new GraficaController();
                                    $prioridad -> ticketsPorSucursalController();
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-block">
                            <h6 class="ml-2 text-primary text-center">Tickets creados por categoria</h6>
                            <table id="" class="table table-striped dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $prioridad = new GraficaController();
                                    $prioridad -> ticketsPorCategoriaController();
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- Fin area de Datatables -->
        </div>