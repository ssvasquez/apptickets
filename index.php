<?php 
    /*error_reporting(E_ALL);
    ini_set('display_errors', '1');*/

    // Modelos
    require_once "models/enlaces.php";
    require_once "models/ingresoModels.php";
    require_once "models/categoriaModels.php";
    require_once "models/graficaModels.php";
    require_once "models/calendarioModels.php";
    require_once "models/ubicacionModels.php";
    require_once "models/usuarioModels.php";
    require_once "models/reportesModels.php";
    require_once "models/ticketsModels.php";
    require_once "models/recordatoriosModels.php";
    require_once "models/kbModels.php";

    //  Controllers
    require_once "controllers/templateControllers.php";
    require_once "controllers/enlaces.php";
    require_once "controllers/ingresoControllers.php";
    require_once "controllers/categoriaControllers.php";
    require_once "controllers/graficaControllers.php";
    require_once "controllers/calendarioControllers.php";
    require_once "controllers/ubicacionControllers.php";
    require_once "controllers/usuarioControllers.php";
    require_once "controllers/reportesControllers.php";
    require_once "controllers/ticketsControllers.php";
    require_once "controllers/recordatorioControllers.php";
    require_once "controllers/kbControllers.php";
    

    $template = new TemplateController();
    $template -> template();
?>