<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}

if($_SESSION["rol"] != 1){
	echo'<script type="text/javascript"> window.location.href="inicio";</script>';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Reportes</title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/responsive.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/bootstrap-datepicker.css" rel="stylesheet"/>
	<link href="views/css/style.css" rel="stylesheet">

</head>
<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<i class="fa fa-file"></i> Reportes
								</div>
								<p></p>
								<div class="card-block">
									<form action="" method="post">
										<div class="rango-fechas">
											<div class="input-daterange form-inline">
												<div class="input-group">
													<span class="input-group-addon">De</span>
													<input name="start_date" id="start_date" class="form-control" type="text">
												</div>
												<p></p>
												<div class="input-group">
													<span class="input-group-addon">A</span>
													<input name="end_date" id="end_date" class="form-control" type="text">
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-8">
												<table class="table table-striped" cellspacing="0">
													<thead>
														<tr>
															<th>No</th>
															<th>Reporte</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Usuarios registrados</td>
															<td><small class="text-muted pull-right breporte" id="search">Ver reporte</small></td>
														</tr>
														<tr>
															<td>2</td>
															<td>Tickets Creados</td>
															<td><small class="text-muted pull-right breporte" id="search1">Ver reporte</small></td>
														</tr>
														<tr>
															<td>3</td>
															<td>Tickes Cerrados</td>
															<td><small class="text-muted pull-right breporte" id="search2">Ver reporte</small></td>
														</tr>
														
														<tr>
															<td>4</td>
															<td>Tickes Resueltos</td>
															<td><small class="text-muted pull-right breporte" id="search3">Ver reporte</small></td>
														</tr>
														
														<tr>
															<td>5</td>
															<td>Tickes Cancelados</td>
															<td><small class="text-muted pull-right breporte" id="search4">Ver reporte</small></td>
														</tr>
														<tr>
															<td>6</td>
															<td>Tickes por usuarios <small class="text-danger pull-right"> No requiere rango de fechas.</small></td>
															<td><small class="text-muted pull-right breporte" id="search5">Ver reporte</small></td>
														</tr>
														<tr>
															<td>7</td>
															<td>Tickes por sucursal <small class="text-danger pull-right"> No requiere rango de fechas.</small></td>
															<td><small class="text-muted pull-right breporte" id="search6">Ver reporte</small></td>
														</tr>
														<tr>
															<td>8</td>
															<td>Tickes por categoria <small class="text-danger pull-right"> No requiere rango de fechas.</small></td>
															<td><small class="text-muted pull-right breporte" id="search7">Ver reporte</small></td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="col-md-4" id="info-reporte">
												<div class="alert alert-success text-center" role="alert"><i class="fa fa-warning" style="font-size:1.5rem"></i> 
													<strong class="">Importante!</strong> <p>Para visualizar un reporte, en algunos casos primero se tiene que seleccionar el rango de fechas y luego dar clic en "Ver reporte".</p>
												</div>
											</div>
										</div>
										<p></p>
									</form>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="response"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/bootstrap-datepicker.js"></script>
	<script src="views/js/libs/jquery.dataTables.min.js"></script>
	<script src="views/js/libs/dataTables.buttons.min.js"></script>
	

	<script src="views/js/libs/pdfmake.min.js"></script>
	<script src="views/js/libs/jszip.min.js"></script>
	<script src="views/js/libs/vfs_fonts.js"></script>
	<script src="views/js/libs/buttons.html5.min.js"></script>	
	<script src="views/js/app.js"></script>
	<script src="views/js/reportes.js"></script>
 
</body>
</html>

				