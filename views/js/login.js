$('document').ready(function() {
	$("#login-form")[0].reset();	
	
$("#login-form").on("submit", function(e){
		e.preventDefault();
		var formData = new FormData(document.getElementById("login-form"));
        var ruta = "datosSesion.php";
        $.ajax({
            url: ruta,
            type: "POST",			
            data: formData,
			contentType: false,
            processData: false,	
			beforeSend: function(){				
				$("#error").fadeOut();
				$("#login_button").html('<span class="fa fa-exchange"></span> &nbsp; Verificando');
			},
            success: function(respuesta)
            {
				//Si es Administrador entrar a la pagina admin.php
                if(respuesta==1){
					$("#login_button").html('<i class="fa fa-spinner fa-spin fa-fw"></i><span class="sr-only"></span>&nbsp; Iniciando');
					setTimeout('location.href = "./admin/"; ',2000);
					//$(location).attr('href','tblDashboard.php');
				}
				else if(respuesta==2){
					$("#login_button").html('<i class="fa fa-spinner fa-spin fa-fw"></i><span class="sr-only"></span>&nbsp; Iniciando');
					setTimeout('location.href = "./user/"; ',2000);
				}				
				//Si insertó datos erroneos mostrar mensaje de error.
				else{					
					$("#error").fadeIn(1000, function(){						
						$("#error").html('<div class="alert alert-warning alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong> <span class="fa fa-exclamation-triangle"></span> </strong>Credenciales incorrectas</div>');
						$("#login_button").html('<span class="fa fa-sign-in"></span> &nbsp; Iniciar sesion');
					});					
				}
            }
        });
    });
});