
// Inicia Grafica 1 Por Mes..
$.getJSON('views/ajax/graficaAjax.php?op=mes', function(datos) {

    Highcharts.chart('graficames', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Tickets'
        },
        subtitle: {
            text: 'Tickets generados por mes' 
        },
        xAxis: {
            categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Tickets / Mes',
            data: datos
        }]
    });
});


// Inicia Grafica 2: Estatus.
$.getJSON('views/ajax/graficaAjax.php?op=estatus', function(datos) {

    Highcharts.chart('graficaestatus', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Status Tickets'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Abierto', 'Asignado', 'Pendiente', 'Cerrado', 'Cancelado', 'Resuelto', 'Reabierto'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{"Total"} </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Status',
            data: datos
        }]
    });
});


// Inicia Grafica 3: Tipo:
//var lowchart = new Highcharts.Chart

var GrafTipo = {
    chart: {
        renderTo: 'graficatipo',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
    },
    title: {
        text: 'Tipo de tickets'
    },
    subtitle: {
        text: '[ Servicio, Incidente y Requerimiento ]'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                color: '#000000',
                connectorColor: '#000000',
                formatter: function() {
                    return '<b>' + this.point.name + '</b>: ' + this.y;
                }
            },
            showInLegend: true
        }
    },
    series: []
};

$.getJSON("views/ajax/graficaAjax.php?op=tipo", function(json) {
    GrafTipo.series = json;
    chart = new Highcharts.Chart(GrafTipo);
});

// Inicia Grafica 3: Prioridad.

/*
var GrafPrioridad = {
    chart: {
        renderTo: 'graficaprioridad',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
    },
    title: {
        text: 'Prioridad de tickets'
    },
    subtitle: {
        text: '[ Abiertos, Asignados, Pendientes y Reabiertos ]'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                color: '#000000',
                connectorColor: '#000000',
                formatter: function() {
                    return '<b>' + this.point.name + '</b>: ' + this.y;
                }
            },
            showInLegend: true
        }
    },
    series: []
};

$.getJSON("views/ajax/graficaAjax.php?op=prioridad", function(json) {
    GrafPrioridad.series = json;
    chart = new Highcharts.Chart(GrafPrioridad);
});*/


// Inicia Grafica No 4. -> Tickets por categorias.
var GrafSubcategoria = {
    chart: {
        renderTo: 'graficasubcategoria',
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false
    },
    title: {
        text: 'Tickets por Categorias'
    },
    subtitle: {
        text: '[ Abiertos, Asignados, Pendientes y Reabiertos ]'
    },
    tooltip: {
        formatter: function() {
            return '<b>' + this.point.name + '</b>: ' + this.y;
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                color: '#000000',
                connectorColor: '#000000',
                formatter: function() {
                    return '<b>' + this.point.name + '</b>: ' + this.y;
                }
            },
            showInLegend: true
        }
    },
    series: []
};

$.getJSON("views/ajax/graficaAjax.php?op=subcategoria", function(json) {
    GrafSubcategoria.series = json;
    chart = new Highcharts.Chart(GrafSubcategoria);
});