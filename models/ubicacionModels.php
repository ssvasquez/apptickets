<?php 
require_once "db.php";

class UbicacionModel{

	public static function mostrarUbicacionModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE activo = 1");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function mostrarUbicacionTodosModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY activo DESC");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function mostrarIDUbicacionModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE PK_idUbicacion = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();
	}


	public static function guardarUbicacionModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (ubicacionDesc, activo) VALUES (:ubicacion, :activo)");

		$stmt -> bindParam(":ubicacion", $datosModel["ubicacion"], PDO::PARAM_STR);
		$stmt -> bindParam(":activo", $datosModel["activo"], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}


	public static function editarUbicacionModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET ubicacionDesc = :ubicacion WHERE PK_idUbicacion = :id");	

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
		$stmt -> bindParam(":ubicacion", $datosModel["ubicacion"], PDO::PARAM_STR);
		
		return $stmt->execute();

		$stmt->close();
	}


	public static function tieneUsuariosModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT COUNT(nombre) FROM $tabla WHERE FK_idUbicacion = :id and activo = 1");

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> execute();
            
		return $stmt -> fetchColumn();

		$stmt->close();
	}


	public static function editarEstatusUbicacionModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET activo = :estatus WHERE PK_idUbicacion = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
		$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}


	public static function comboboxUbicacionModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT ubicacion.PK_idUbicacion, ubicacion.ubicacionDesc from $tabla INNER JOIN usuario on ubicacion.PK_idUbicacion = usuario.FK_idUbicacion WHERE usuario.PK_idUsuario = :idUsuario");

		$stmt->bindParam(":idUsuario", $id, PDO::PARAM_INT);

		$stmt->execute();

		return $stmt -> fetchAll();

		$stmt->close();
		
	}

}