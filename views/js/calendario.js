
$(document).ready(function() {
    $('#calendar').fullCalendar({
        lang: 'es',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: moment(),
        editable: true,
        eventLimit: true, //Permitir más links, cuando son muchos eventos

        events: 'views/ajax/calendarioAjax.php'
    });
});
