
// Comprobar usuario
function comprobarUsuario() {
	$( ".usuarioComprobar" ).html("");
	var usuarioForm = $("#regUsuario").val();
	$.ajax({
		url: "views/ajax/usuarioAjax.php?op=usuario",
		type: 'POST',
		data: {	regUsuario: usuarioForm},
		dataType: 'json',
		success: function (response) {
			$( ".usuarioComprobar" ).html(   response  );
		}
	});
}

// Comprobar correo
function comprobarCorreo() {
	$( ".correoComprobar" ).html("");
	var correoForm = $("#regCorreo").val();
	$.ajax({
		url: "views/ajax/usuarioAjax.php?op=correo",
		type: 'POST',
		data: {	regCorreo: correoForm},
		dataType: 'json',
		success: function (response) {
			$( ".correoComprobar" ).html(response);
		}
	});
}




/*	=======	M O S T R A R Ubicacion	=======*/ 
var tblUsuario;
tblUsuario = $("#usuarios").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/usuarioAjax.php?op=mostrar",
	"order": [],
	"columns": [
		{"orderable": false },
		{"orderable": false},
		{"orderable": false},
		{"orderable": false},
		{"orderable": false},
		null,
		{"orderable": false},
		{"orderable": false}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{	responsivePriority: 1, targets: 0	}
	]
});


/*	=======	A G R E G A R  Usuario	=======*/
$("#btnAgregarUsuario").on('click', function () {

	$("#frmRegistrarUsuario")[0].reset();

	$("#frmRegistrarUsuario").unbind('submit').bind('submit', function () {

		var form = $(this);
		var altaNombre = $("#regNombre").val();
		var altaApellido = $("#regApellido").val();
		var altaUsuario = $("#regUsuario").val();
		var altaCorreo = $("#regCorreo").val();
		var altaUbicacion = $("#regUbicacion").val();
		var altaTelefono = $("#regTelefono").val();
		var altaRol = $("#regRol").val();

		if (altaNombre && altaApellido && altaUsuario && altaCorreo && altaUbicacion && altaTelefono && altaRol ){
			$.ajax({
				url: "views/ajax/usuarioAjax.php?op=guardar",
                type: form.attr('method'),
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
					if (response.success == true){
						$("#frmRegistrarUsuario")[0].reset();
						tblUsuario.ajax.reload(null, false);
						$("#registrarUsuario").modal('hide');
						successSwal(response.mensaje);
					}else{
						$("#registrarUsuario").modal('hide');
						errorSwal(response.mensaje);
					}
				}
			});
		}
		errorSwal("Todos los campos son obligatorios");
		return false;		
	});
});


/*	=======	E D I T A R Usuario	=======*/
function editarUsuario(idUsuario) {
	if (idUsuario) {
		
		// Buscar datos del usuario
		$.ajax({
			url: "views/ajax/usuarioAjax.php?op=getID",
			type: 'POST',
			data: {
				idUsuario: idUsuario
			},
			dataType: 'json',
			success: function (response) {

				$("#editNombre").val(response.nombre);
				$("#editApellido").val(response.apellidos);
				$("#editCorreo").val(response.correo);
				$("#editUbicacion").val(response.idUbicacion);
				$("#editTelefono").val(response.telefono);
				$("#editRol").val(response.rol);
				$("#editID").val(response.idUsuario);

				$("#frmEditarUsuario").unbind('submit').bind('submit', function () {
					var form = $(this);
					// Capturamos lo que tiene la caja de texto.
					var usuarioNombre = $("#editNombre").val();
					var usuarioApellido = $("#editApellido").val();
					var usuarioCorreo = $("#editCorreo").val();
					var usuariTelefono = $("#editTelefono").val();
					var usuarioRol = $("#editRol").val();
					var usuarioUbicacion = $("#editUbicacion").val();

					if (usuarioNombre && usuarioApellido && usuarioCorreo && usuariTelefono && usuarioRol && usuarioUbicacion) {
						$.ajax({
							url: "views/ajax/usuarioAjax.php?op=editar",
							type: form.attr('method'), // Es lo mismo a poner POST
							data: form.serialize(),
							dataType: 'json',
							success: function (response) {

								if (response.success == true) {

									tblUsuario.ajax.reload(null, false);
									$("#modalEditarUsuario").modal('hide');
									successSwal(response.mensaje);

								} else {
									//Cerrar ventana y mostrar mensaje de error.
									$("#modalEditarUsuario").modal('hide');
									errorSwal(response.mensaje);
								}
							}
						});
					}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	A C T I V A R  Usuario	=======*/
function activarUbicacion(idUbicacion = null){
	if(idUbicacion){
		swal({
			title: "¿Realmente desea A C T I V A R esta ubicacion?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ubicacionAjax.php?op=activar",
					type: 'post',
					data: {idUbicacion : idUbicacion},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblUsuario.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Usuario	=======*/
function desactivarUsuario(idUsuario = null){
	if(idUsuario){
		swal({
			title: "¿Realmente desea D E S A C T I V A R este usuario?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/usuarioAjax.php?op=desactivar",
					type: 'post',
					data: {idUsuario : idUsuario},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblUsuario.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}

/** Mensajes de Acierto y Error. Aplica area de Categoria y Subcategoria */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}