<?php

require_once "../../controllers/categoriaControllers.php";
require_once "../../models/categoriaModels.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxCategoria{


	public function mostrarCategoriaAjax(){

		$respuesta = CategoriaController::mostrarCategoriaController();
		print json_encode($respuesta);
	}


	public function categoriaPorID(){

		$respuesta = CategoriaController::mostrarCategoriaIDController();
		print json_encode($respuesta);	
	}


	public function guardarCategoria(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CategoriaController::guardarCategoriaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Categoria registrada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al registrar categoria";
		}

		echo json_encode($salida);
	}


	public function editarCategoria(){
		
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CategoriaController::editarCategoriaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Categoria actualizada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar categoria";
		}

		echo json_encode($salida);

	}


	public function desactivarCategoria(){

		$salida = array('success' => false, 'mensaje' => array());

		$subcategoriaActiva = CategoriaController::tieneSubcategoriasController();

		if ($subcategoriaActiva == 0){

			$respuesta = CategoriaController::desactivarCategoriaController();

			if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Categoria desactivada correctamente";
			}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al desactivar categoria";
			}

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar categoria, verifica que no tenga subcategorias activas";
		}	

		echo json_encode($salida);
	}


	public function activarCategoria(){

		$salida = array('success' => false, 'mensaje' => array());		

		$respuesta = CategoriaController::activarCategoriaController();

		if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Categoria activada correctamente";
		}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al activar categoria";
		}		

		echo json_encode($salida);
	}



	// SUBCATEGORIA

	public function mostrarSubcategoriaAjax(){

		$respuesta = CategoriaController::mostrarSubcategoriaController();
		print json_encode($respuesta);

	}


	public function subcategoriaPorID(){

		$respuesta = CategoriaController::mostrarSubcategoriaIDController();
		print json_encode($respuesta);
	
	}


	public function guardarSubcategoria(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CategoriaController::guardarSubcategoriaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Subcategoria registrada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al registrar subcategoria";
		}

		echo json_encode($salida);
	}

	
	public function editarSubcategoria(){
		
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CategoriaController::editarSubcategoriaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Subcategoria actualizada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar Subcategoria";
		}

		echo json_encode($salida);

	}


	public function desactivarSubcategoria(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CategoriaController::desactivarSubcategoriaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Subcategoria desactivada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar subcategoria";
		}

		echo json_encode($salida);

	}


	public function activarSubcategoria(){

		$salida = array('success' => false, 'mensaje' => array());

		$categoriaActiva = CategoriaController::activaCategoriaController();

		if ($categoriaActiva[0] == 1){

			$respuesta = CategoriaController::activarSubcategoriaController();

			if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Subcategoria activada correctamente";
			}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al activar subcategoria";
			}

		} else{
			
			$salida['success'] = false;
			$salida['mensaje'] = "Error al activar subcategoria, verifica que la categoria asociado se encuentre activa";

		}	

		echo json_encode($salida);

	}


	public function mostrarSubcategoria(){
		$respuesta = CategoriaController::subcategoriaSeleccionController();

		echo json_encode($respuesta);
	}

}

if($opcion == 'getSubcategoria'){
	$getSub = new AjaxCategoria();
	$getSub -> mostrarSubcategoria();
}


if($opcion == 'mostrar'){
	$mostrar = new AjaxCategoria();
	$mostrar -> mostrarCategoriaAjax();
}

if($opcion == 'getID'){
	$idCategoria = new AjaxCategoria();
	$idCategoria -> categoriaPorID();
}

if($opcion == 'guardarCategoria'){
	$guardar = new AjaxCategoria();
	$guardar -> guardarCategoria();
}

if ($opcion == 'editar'){
	$editar = new AjaxCategoria();
	$editar -> editarCategoria();
}

if ($opcion == 'desactivarcategoria'){
	$desactivarCat = new AjaxCategoria();
	$desactivarCat -> desactivarCategoria();
}

if ($opcion == 'activarcategoria'){
	$desactivarCat = new AjaxCategoria();
	$desactivarCat -> activarCategoria();
}


// SUBCATEGORIA

if($opcion == 'mostrarsubcategoria'){
	$mostrar = new AjaxCategoria();
	$mostrar -> mostrarSubcategoriaAjax();
}

if($opcion == 'getIDSubcategoria'){
	$idSubcategoria = new AjaxCategoria();
	$idSubcategoria -> subcategoriaPorID();
}

if($opcion == 'guardarSubcategoria'){
	$guardar = new AjaxCategoria();
	$guardar -> guardarSubCategoria();
}

if($opcion == 'editarSubcategoria'){
	$editarSub = new AjaxCategoria();
	$editarSub -> editarSubcategoria();
}

if($opcion == 'desactivarSubcategoria'){
	$desactivarSub = new AjaxCategoria();
	$desactivarSub -> desactivarSubcategoria();
}

if($opcion == 'activarSubcategoria'){
	$desactivarSub = new AjaxCategoria();
	$desactivarSub -> activarSubcategoria();
}