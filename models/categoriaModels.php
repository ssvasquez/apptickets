<?php 

require_once "db.php";

class CategoriasModel{

	public static function mostrarCategoriaModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE activo = 1 ORDER BY categoriaDesc");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function mostrarCategoriaTodosModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY activo DESC");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function mostrarIDCategoriaModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE PK_idCategoria = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();
	}


	public static function guardarCategoriaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (categoriaDesc, activo) VALUES (:categoria, :activo)");

		$stmt -> bindParam(":categoria", $datosModel["categoria"], PDO::PARAM_STR);
		$stmt -> bindParam(":activo", $datosModel["activo"], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}


	public static function editarCategoriaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET categoriaDesc = :categoria WHERE PK_idCategoria = :id");	

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> bindParam(":categoria", $datosModel["categoria"], PDO::PARAM_STR);
		
		return $stmt->execute();

		$stmt->close();
	}


	public static function tieneSubcategoriaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT COUNT(subcategoriaDesc) FROM $tabla WHERE FK_idCategoria = :id and activo = 1");

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> execute();
            
		return $stmt -> fetchColumn();

		$stmt->close();
	}


	public static function desactivarCategoriaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET activo = 0 WHERE PK_idCategoria = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}


	public static function activarCategoriaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET activo = 1 WHERE PK_idCategoria = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}



	/*  ==================================================================
		Area para operaciones de subcategoria.... 
	==================================================================  */

	
    // Mostrar subcategorias.
    public static function mostrarSubcategoriaModel(){

        $stmt = Conexion::conectar()->prepare("CALL pro_leersubcategoria");

    	$stmt -> execute();

        return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function mostrarListaSubcategoriaModel($tabla, $id){

        $stmt = Conexion::conectar()->prepare("SELECT PK_idSubcategoria, subcategoriaDesc FROM $tabla where FK_idCategoria = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);
		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}
		

	public static function mostrarIDSubcategoriaModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE PK_idSubcategoria = :id");

		$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

	}


	public static function guardarSubcategoriaModel($datosModel, $tabla){

        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (FK_idCategoria, subcategoriaDesc, activo) VALUES (:id_categoria, :subcategoria, :activo)");

        $stmt -> bindParam(":id_categoria", $datosModel["id_categoria"], PDO::PARAM_INT);
		$stmt -> bindParam(":subcategoria", $datosModel["subcategoria"], PDO::PARAM_STR);
		$stmt -> bindParam(":activo", $datosModel["activo"], PDO::PARAM_INT);
        
        return $stmt->execute();

		$stmt->close();
    }

    // Editar subCategoria.
    public static function editarSubcategoriaModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("UPDATE  $tabla SET FK_idCategoria = :idCategoria, subcategoriaDesc = :subcategoria WHERE PK_idSubcategoria = :id");

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> bindParam(":idCategoria", $datosModel["idCategoria"], PDO::PARAM_INT);

		$stmt -> bindParam(":subcategoria", $datosModel["subcategoria"], PDO::PARAM_STR);

		return $stmt->execute();

		$stmt->close();

    }

    // Deshabilitar subcategoria.
    public static function desactivarSubcategoriaModel($datosModel, $tabla){

        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET activo = 0 WHERE PK_idSubcategoria = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}
	

	public static function activaCategoriaModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT categoria.activo FROM $tabla INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE subcategoria.PK_idSubcategoria = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

		$stmt->execute();

		return $stmt -> fetch();

		$stmt->close();
	}


	public static function activarSubcategoriaModel($datosModel, $tabla){

        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET activo = 1 WHERE PK_idSubcategoria = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}
	


}