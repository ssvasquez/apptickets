<?php

require_once "../../controllers/ticketsControllers.php";
require_once "../../models/ticketsModels.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxTickets{


	public function mostrarTicketsAdmin(){
		$respuesta = TicketsController::mostrarTicketAdminController();
		print json_encode($respuesta);
	}

	public function mostrarTicketsTecnicos(){
		$respuesta = TicketsController::mostrarTicketTecnicoController();
		print json_encode($respuesta);
	}


	public function mostrarTicketIdAdmin(){
		$respuesta = TicketsController::mostrarTicketIdAdminController();
		print json_encode($respuesta);
	}

	public function mostrarTicketResuelto(){
		$respuesta = TicketsController::ticketsResueltosAdminController();
		print json_encode($respuesta);
	}

	public function mostrarTicketCerrado(){
		$respuesta = TicketsController::ticketsCerradosAdminController();
		print json_encode($respuesta);
	}

	public function mostrarTicketCancelado(){
		$respuesta = TicketsController::ticketsCanceladosAdminController();
		print json_encode($respuesta);
	}

	public function heredarTickets(){
		$respuesta = TicketsController::heredarTicketController();
		print json_encode($respuesta);
	}

	public function guardarTicket(){

		$respuesta = TicketsController::guardarTicketController();

		echo json_encode($respuesta);
	}



	public function guardarArchivos(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = TicketsController::guardarArchivoController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Archivo(s) subido correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al subir archivo(s)";
		}

		echo json_encode($salida);
	}


	


	public function editarTicket(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = TicketsController::editarTicketController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Ticket actualizado correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar el ticket";
		}

		echo json_encode($salida);
	}


	public function crearEvento(){

		//$salida = array('success' => false, 'mensaje' => array());

		$respuesta = TicketsController::crearEventoController();

		echo json_encode($respuesta);
	}


	public function resolverTicket(){

		$respuesta = TicketsController::resolverTicketController();

		echo json_encode($respuesta);
	}


	public function cerrarTicket(){
		$respuesta = TicketsController::cerrarTicketController();

		echo json_encode($respuesta);
	}

	public function cancelarTicket(){
		$respuesta = TicketsController::cancelarTicketController();

		echo json_encode($respuesta);
	}

	public function reabrirTicket(){
		$respuesta = TicketsController::reabrirTicketController();

		echo json_encode($respuesta);
	}
}




if($opcion == 'archivos'){
	$archivos = new AjaxTickets();
	$archivos -> guardarArchivos();
}

if($opcion == 'reabrirTicket'){
	$reabrir = new AjaxTickets();
	$reabrir -> reabrirTicket();
}

if($opcion == 'cancelarTicket'){
	$cancelar = new AjaxTickets();
	$cancelar -> cancelarTicket();
}

if($opcion == 'cerrarTicket'){
	$cerrar = new AjaxTickets();
	$cerrar -> cerrarTicket();
}


if($opcion == 'resolverTicket'){
	$resolver = new AjaxTickets();
	$resolver -> resolverTicket();
}


if($opcion == 'crearEvento'){
	$evento = new AjaxTickets();
	$evento -> crearEvento();
}


if($opcion == 'heredar'){
	$heredar = new AjaxTickets();
	$heredar -> heredarTickets();
}


if($opcion == 'mostrar'){
	$mostrar = new AjaxTickets();
	$mostrar -> mostrarTicketsAdmin();
}

if($opcion == 'mostrartecnico'){
	$mostrartec = new AjaxTickets();
	$mostrartec -> mostrarTicketsTecnicos();
}

if($opcion == 'resueltos'){
	$resueltos = new AjaxTickets();
	$resueltos -> mostrarTicketResuelto();
}

if($opcion == 'cerrados'){
	$resueltos = new AjaxTickets();
	$resueltos -> mostrarTicketCerrado();
}

if($opcion == 'cancelados'){
	$resueltos = new AjaxTickets();
	$resueltos -> mostrarTicketCancelado();
}

if($opcion == 'getID'){
	$id = new AjaxTickets();
	$id -> mostrarTicketIdAdmin();
}

if ($opcion == 'editar'){
	$editar = new AjaxTickets();
	$editar -> editarTicket();
}

if($opcion == 'guardar'){
	$guardar = new AjaxTickets();
	$guardar -> guardarTicket();
}

/*

if ($opcion == 'desactivar'){
	$desactivar = new AjaxTickets();
	$desactivar -> desactivarUbicacion();
}

if ($opcion == 'activar'){
	$activar = new AjaxTickets();
	$activar -> activarUbicacion();
}*/