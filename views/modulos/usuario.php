<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}

// Si NO es Admin no mostrar este modulo, volver a inicio...
if($_SESSION["rol"] != 1){
	echo'<script type="text/javascript"> window.location.href="inicio";</script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Usuarios </title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/dataTables.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/responsive.bootstrap4.min.css" rel="stylesheet"/>
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		<main class="main">
			<div class="container-fluid">
				<?php 
					$usuario = new UsuarioController();
					//$usuario -> duplicadoUsuarioController();
					//duplicadoUsuarioController()
				?>

				<div class="animated fadeIn">
						<div class="row">
							<div class="col-lg-12">
								<div class="card">
									<div class="card-header d-flex justify-content-between">
										<span><i class="fa fa-user"></i> Usuarios</span>
										<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#registrarUsuario" id="btnAgregarUsuario">
											<i class="fa fa-plus-circle"></i> Usuario
										</button>
									</div>
									<div class="card-block">
										<table id="usuarios" class="table table-striped table-vertical" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>No</th>
													<th data-priority="1">Nombre</th>
													<th>Registro</th>
													<th>Correo</th>
													<th>Telefono</th>
													<th>Rol</th>
													<th>Ubicación</th>
													<th>Acciones</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="modal fade" id="registrarUsuario" tabindex="-1" role="dialog" aria-labelledby="registrarUsuarioModal" aria-hidden="true">
					<div class="modal-dialog modal-info" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="registrarUsuarioModal"><i class="fa fa-plus-circle"></i> Registrar usuario</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="POST" id="frmRegistrarUsuario">
								<div class="modal-body">
									<div class="messages"></div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Nombre</span>
											<input type="text" id="regNombre" name="regNombre" class="form-control" placeholder="Nombre">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Apellidos</span>
											<input type="text" id="regApellido" name="regApellido" class="form-control" placeholder="Apellido">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Correo</span>
											<input type="text" id="regCorreo" name="regCorreo" class="form-control" placeholder="Correo" onBlur="comprobarCorreo()">										
										</div>
									</div>
									<span class="text-right text-danger correoComprobar"></span>

									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Usuario</span>
											<input type="text" id="regUsuario" name="regUsuario" class="form-control" placeholder="Usuario" onBlur="comprobarUsuario()">										
										</div>
									</div>
									<span class="text-right text-danger usuarioComprobar"></span>
									

									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Ubicación</span>
											<select class="form-control" id="regUbicacion" name="regUbicacion">
												<option value="">Seleccione...</option>
												<?php
													$ubicacion = new UbicacionController();
													$ubicacion -> ubicacionSeleccionController();
												?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Telefono</span>
											<input type="text" id="regTelefono" name="regTelefono" class="form-control" placeholder="Telefono">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Rol</span>
											<select class="form-control" name="regRol" id="regRol">
												<option value="">Seleccione...</option>
												<option value="1">Administrador</option>
												<option value="2">Técnico</option>
												<option value="3">Usuario</option>
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div><!--Fin Modal registrar usuario -->


				<!-- Modal editar usuario -->
				<div class="modal fade" tabindex="-1" role="dialog" id="modalEditarUsuario">
					<div class="modal-dialog modal-primary" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><span class="fa fa-edit"></span> Editar usuario</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form action="" method="POST" id="frmEditarUsuario">
								<div class="modal-body">
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Nombre</span>
											<input type="hidden" id="editID" name="editID" class="form-control">
											<input type="text" id="editNombre" name="editNombre" class="form-control" placeholder="Nombre">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Apellidos</span>
											<input type="text" id="editApellido" name="editApellido" class="form-control" placeholder="Apellido">
										</div>
									</div>
									
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Correo</span>
											<input type="text" id="editCorreo" name="editCorreo" class="form-control" placeholder="Correo">
										</div>
									</div>
									
									<div class="form-group">
										<div class="input-group"> 
											<span class="input-group-addon">Ubicacion</span>
											<select class="form-control" name="editUbicacion" id="editUbicacion">
												<option value="">Seleccione...</option>
												<?php
													$ubicacion = new UbicacionController();
													$ubicacion -> ubicacionSeleccionController();
												?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Telefono</span>
											<input type="text" id="editTelefono" name="editTelefono" class="form-control" placeholder="Contraseña">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<span class="input-group-addon">Rol</span>
											<select class="form-control" name="editRol" id="editRol">
												<option value="0">Seleccione...</option>
												<option value="1">Administrador</option>
												<option value="2">Técnico</option>
												<option value="3">Usuario</option>
											</select>
										</div>
									</div>
								</div>
								<div class="modal-footer modaleditarUsuario">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
							</form>
						</div>
					</div>
				</div> <!-- Fin modal editar usuario -->
			</div>
		</main>
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>
	<script src="views/js/libs/jquery.dataTables.min.js"></script>	
	<script src="views/js/libs/dataTables.bootstrap4.min.js"></script>
	<script src="views/js/libs/dataTables.responsive.min.js"></script>
	<script src="views/js/libs/responsive.bootstrap4.min.js"></script>
	<script src="views/js/app.js"></script>
	<script src="views/js/usuario.js"></script>
 
</body>
</html>

