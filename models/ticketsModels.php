<?php
	//error_reporting(E_ALL);
	require_once "db.php";
	ini_set('date.timezone','America/Mexico_City');

	class TicketsModels{

		// Consulta para tickets por técnico -->OK
		public static function mostrarTicketsTecnicoModel($datosModel, $tabla){

			//$stmt = Conexion::conectar()->prepare("SELECT usuario.PK_idUsuario, usuario.nombre,ticket.PK_idTicket, ticket.refTicket, ticket.tituloTicket, ticket.descripcionTicket, ticket.statusTicket, ticket.prioridadTicket, ticket.actualizado, subcategoria.PK_idSubcategoria, subcategoria.subcategoriaDesc, categoria.PK_idCategoria, categoria.categoriaDesc FROM $tabla INNER JOIN ticket_usuario ON ticket_usuario.FK_idUsuario = usuario.PK_idUsuario INNER JOIN ticket ON ticket_usuario.FK_idTicket = ticket.PK_idTicket INNER JOIN subcategoria on ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN categoria on subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE ticket.statusTicket <> 4 AND ticket.statusTicket <> 5 AND ticket.statusTicket <> 6 AND usuario.PK_idUsuario = :idusuario AND ticket_usuario.status = 'ATENCION' ORDER by ticket.actualizado DESC");
			$stmt = Conexion::conectar()->prepare("SELECT vw_tickets.*, vw_usuario_ticket.nombre AS tecnico FROM vw_tickets INNER JOIN vw_usuario_ticket ON vw_tickets.PK_idTicket = vw_usuario_ticket.FK_idTicket WHERE vw_tickets.statusTicket <> 4 AND vw_tickets.statusTicket <> 5 AND vw_tickets.statusTicket <> 6 AND vw_usuario_ticket.PK_idUsuario = :idusuario and vw_usuario_ticket.estatus = 'ATENCION' ORDER BY vw_tickets.actualizado DESC ");

			$stmt -> bindParam(":idusuario", $datosModel['idusuario'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();
			
			$stmt -> close();
		}

		// Consulta para tickets por Administrador -- OK
		public static function mostrarTicketsAdminModel($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT vw_tickets.*, GROUP_CONCAT(vw_usuario_ticket.nombre SEPARATOR '| ') AS usuarios FROM $tabla INNER JOIN vw_usuario_ticket ON vw_tickets.PK_idTicket = vw_usuario_ticket.FK_idTicket WHERE vw_tickets.statusTicket <> 4 AND vw_tickets.statusTicket <> 5 AND vw_tickets.statusTicket <> 6 GROUP BY vw_tickets.PK_idTicket ORDER BY vw_tickets.actualizado DESC");

			$stmt -> execute();
		
			return $stmt -> fetchAll(PDO::FETCH_ASSOC);
			
			$stmt -> close();
			//$db->close();
		}

		// Mostrar ticket, detalles Ticket...OK
		public static function mostrarIDTicketModel($tabla, $id){

			$stmt = Conexion::conectar()->prepare("SELECT usuario.PK_idUsuario, usuario.nombre, ticket_usuario.status, ticket.PK_idTicket, ticket.tituloTicket, ticket.descripcionTicket, ticket.statusTicket, ticket.prioridadTicket,ticket.tipoTicket, ticket.escaladoTicket FROM usuario INNER JOIN ticket_usuario ON ticket_usuario.FK_idUsuario = usuario.PK_idUsuario INNER JOIN ticket ON ticket_usuario.FK_idTicket = ticket.PK_idTicket WHERE ticket.PK_idTicket =  :id AND ticket_usuario.status ='ATENCION'");
	
			$stmt -> bindParam(":id", $id, PDO::PARAM_INT);
	
			$stmt -> execute();
	
			return $stmt -> fetch();
	
			$stmt -> close();
		}

		// Detalles ticket
		public static function detalleTicketModel($tabla, $id){

			$stmt = Conexion::conectar()->prepare("SELECT ticket.PK_idTicket, ticket.tituloTicket, ticket.descripcionTicket, ticket.creado, ticket.actualizado, ticket.primeraRespuesta, ticket.cierre, ticket.statusTicket, ticket.prioridadTicket, ticket.alertaTicket, ticket.escaladoTicket, ticket.refTicket, ticket.tipoTicket, ticket.FK_idSubcategoria, ticket.FK_idUbicacion, subcategoria.subcategoriaDesc, categoria.categoriaDesc, ubicacion.PK_idUbicacion, ubicacion.ubicacionDesc FROM ticket INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN ubicacion ON ticket.FK_idUbicacion = ubicacion.PK_idUbicacion INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE PK_idTicket = :id");
	
			$stmt -> bindParam(":id", $id, PDO::PARAM_INT);
	
			$stmt -> execute();
	
			return $stmt -> fetch(PDO::FETCH_ASSOC);
	
			$stmt -> close();

		}

		//	Seleccionar tickets Resueltos, Cerrados y/o Cancelados ADMIN --OK
		public static function ticketsEstatusAdminModel($tabla, $id){
			//$stmt = Conexion::conectar()->prepare("SELECT ticket.PK_idTicket, ticket.tituloTicket, ticket.descripcionTicket,ticket.actualizado, ticket.statusTicket, ticket.prioridadTicket, ticket.refTicket, subcategoria.subcategoriaDesc, categoria.categoriaDesc FROM $tabla INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE ticket.statusTicket = :estatus ORDER BY actualizado DESC");
			$stmt = Conexion::conectar()->prepare("SELECT vw_tickets.*, GROUP_CONCAT(vw_usuario_ticket.nombre SEPARATOR '| ') AS usuarios FROM $tabla INNER JOIN vw_usuario_ticket ON vw_tickets.PK_idTicket = vw_usuario_ticket.FK_idTicket WHERE vw_tickets.statusTicket = :estatus GROUP BY vw_tickets.PK_idTicket");

			$stmt -> bindParam(':estatus', $id, PDO::PARAM_INT);
			$stmt -> execute();

			return $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stmt -> close();
		}

		//	Seleccionar tickets Resueltos, Cerrados y/o Cancelados TECNICOS --OK
		public static function ticketsEstatusTecnicoModel($datosModel, $tabla){
			//$stmt = Conexion::conectar()->prepare("SELECT ticket.PK_idTicket, ticket.tituloTicket, ticket.descripcionTicket,ticket.actualizado, ticket.statusTicket, ticket.prioridadTicket, ticket.refTicket, subcategoria.subcategoriaDesc, categoria.categoriaDesc FROM $tabla INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE ticket.statusTicket = :estatus ORDER BY actualizado DESC");
			$stmt = Conexion::conectar()->prepare("SELECT vw_tickets.*, vw_usuario_ticket.nombre AS tecnico FROM $tabla INNER JOIN vw_usuario_ticket ON vw_tickets.PK_idTicket = vw_usuario_ticket.FK_idTicket WHERE vw_tickets.statusTicket = :estatus AND vw_usuario_ticket.PK_idUsuario = :idusuario and vw_usuario_ticket.estatus = 'ATENCION' ORDER BY vw_tickets.actualizado DESC");

			$stmt -> bindParam(':estatus', $datosModel['estatus'], PDO::PARAM_INT);
			$stmt -> bindParam(':idusuario', $datosModel['idusuario'], PDO::PARAM_INT);
			$stmt -> execute();

			return $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$stmt -> close();
		}


		// Heredar Tickets -- OK
		public static function ticketsHeredarModel($tabla, $id){

			$stmt = Conexion::conectar()->prepare("SELECT ticket.PK_idTicket, ticket.esHijo, ticket.tituloTicket, ticket.descripcionTicket, ticket.statusTicket, ticket.prioridadTicket, ticket.escaladoTicket, ticket.refTicket, ticket.tipoTicket, ticket.FK_idSubcategoria, ticket.FK_idUbicacion, subcategoria.PK_idSubcategoria, subcategoria.subcategoriaDesc, categoria.PK_idCategoria, categoria.categoriaDesc, ubicacion.PK_idUbicacion, ubicacion.ubicacionDesc FROM $tabla INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN ubicacion ON ticket.FK_idUbicacion = ubicacion.PK_idUbicacion INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE PK_idTicket = :idTicket AND ticket.statusTicket <> 4 AND ticket.statusTicket <> 5 AND ticket.refTicket = 0");

			$stmt -> bindParam(':idTicket', $id, PDO::PARAM_INT);
			$stmt -> execute();

			return $stmt->fetch(PDO::FETCH_ASSOC);
			
			$stmt -> close();

		}


		//Registrar nuevo ticket  => Intentar Usuar Transaction 
		public static function guardarTicketModel($datosModel, $tabla){

			$pdo=Conexion::conectar();

			$pdo->beginTransaction();

			$validator = array('success' => false, 'messages' => array());
			
			try {

				//$ahora = date("Y-m-d H:i:s")
				$date = new DateTime();
				$date->modify('+1 minute');
				$ahora = $date->format('Y-m-d H:i:s');

				$stmt = $pdo->prepare("INSERT INTO $tabla (FK_idUbicacion, FK_idSubcategoria, tituloTicket, descripcionTicket, statusTicket, prioridadTicket, alertaTicket, escaladoTicket, tipoTicket, esHijo, refTicket) VALUES  (:ubicacion ,:subcategoria, :titulo, :descripcion, :estatus, :prioridad, :alerta, :escalado, :tipo, :hijo , :referencia)");
				$stmt -> bindParam(":ubicacion",	$datosModel['ubicacion'], PDO::PARAM_INT);
				$stmt -> bindParam(":subcategoria",	$datosModel['subcategoria'], PDO::PARAM_INT);
				$stmt -> bindParam(":titulo",		$datosModel['titulo'], PDO::PARAM_STR);
				$stmt -> bindParam(":descripcion",	$datosModel['descripcion'], PDO::PARAM_STR);
				$stmt -> bindParam(":estatus",		$datosModel['estatus'], PDO::PARAM_INT);
				$stmt -> bindParam(":prioridad",	$datosModel['prioridad'], PDO::PARAM_INT);
				$stmt -> bindParam(":alerta",		$datosModel['alerta'], PDO::PARAM_INT);
				$stmt -> bindParam(":escalado",		$datosModel['escalado'], PDO::PARAM_INT);
				$stmt -> bindParam(":tipo",			$datosModel['tipo'], PDO::PARAM_INT);
				$stmt -> bindParam(":hijo",			$datosModel['hijo'], PDO::PARAM_INT);
				$stmt -> bindParam(":referencia",	$datosModel['referencia'], PDO::PARAM_INT);
				$stmt->execute();

				// Obtener el ultimo registro...
				$ultimoID = $pdo->lastInsertId();

				// Si estatus es Cerrado al registrar ticket, actualizar el campo cierre.
				if($datosModel['estatus'] == 4){

					$stmt = $pdo->prepare("UPDATE ticket SET cierre = :horacerrado WHERE ticket.PK_idTicket = :idTicket");
					$stmt -> bindParam(":idTicket", $ultimoID, PDO::PARAM_INT);
					$stmt -> bindParam(":horacerrado", $ahora);
					$stmt->execute();
				}

				
				// Insertar en la tabla ticket_usuario. ALTA, ATENCION, CLIENTE
				$stmt = $pdo->prepare("INSERT INTO ticket_usuario(FK_idTicket, FK_idUsuario, `status`) VALUES  (:idTicket, :idUsuario, 'ALTA')");
				$stmt -> bindParam(":idTicket",		$ultimoID, PDO::PARAM_INT);
				$stmt -> bindParam(":idUsuario",	$datosModel['usuarioCreado'], PDO::PARAM_INT);
				$stmt->execute();

				$stmt = $pdo->prepare("INSERT INTO ticket_usuario(FK_idTicket, FK_idUsuario, `status`) VALUES  (:idTicket, :idUsuario, 'ATENCION')");
				$stmt -> bindParam(":idTicket",		$ultimoID, PDO::PARAM_INT);
				$stmt -> bindParam(":idUsuario",	$datosModel['usuarioTecnico'], PDO::PARAM_INT);
				$stmt->execute();

				$stmt = $pdo->prepare("INSERT INTO ticket_usuario(FK_idTicket, FK_idUsuario, `status`) VALUES  (:idTicket, :idUsuario, 'CLIENTE')");
				$stmt -> bindParam(":idTicket",		$ultimoID, PDO::PARAM_INT);
				$stmt -> bindParam(":idUsuario",	$datosModel['usuarioCliente'], PDO::PARAM_INT);
				$stmt->execute();
				

				$pdo->commit();
				//
				$validator['success'] = true;
				$validator['messages'] = "Ticket creado correctamente";


			}catch (PDOException $e) {

				$pdo->rollBack();

				$validator['success'] = false;
				$validator['messages'] = "Error al crear ticket ". $e->getMessage();
			}
			return $validator;
		}


		//Editar Ticket -- OK
		public static function editarTicketModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET tituloTicket = :titulo, descripcionTicket = :descripcion, statusTicket = :estatus, prioridadTicket = :prioridad, escaladoTicket = :escalado, tipoTicket = :tipo WHERE ticket.PK_idTicket = :id");
	
			$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
			$stmt -> bindParam(":titulo", $datosModel["titulo"], PDO::PARAM_STR);
			$stmt -> bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
			$stmt -> bindParam(":estatus", $datosModel["estatus"], PDO::PARAM_STR);
			$stmt -> bindParam(":prioridad", $datosModel["prioridad"], PDO::PARAM_STR);
			$stmt -> bindParam(":escalado", $datosModel["escalado"], PDO::PARAM_STR);
			$stmt -> bindParam(":tipo", $datosModel["tipo"], PDO::PARAM_STR);
			
			return $stmt->execute();
	
			$stmt->close();
		}


		// Crear Eventos. -- OK
		public static function crearEventoModel($datosModel, $tabla){

			$pdo=Conexion::conectar();

			$pdo->beginTransaction();

			$validator = array('success' => false, 'messages' => array());

			try {

				$ahora = date("Y-m-d H:i:s");

				$stmt = $pdo->prepare("INSERT INTO $tabla (FK_idTicket, FK_idUsuario, eventoDesc)VALUES  (:idTicket, :idUsuario, :evento)");

				$stmt -> bindParam(":idTicket", $datosModel['idTicket'], PDO::PARAM_INT);
				$stmt -> bindParam(":idUsuario", $datosModel['idUsuario'], PDO::PARAM_INT);
				$stmt -> bindParam(":evento", 	$datosModel['evento'], PDO::PARAM_STR);
				$stmt->execute();

				// Obtener el ultimo registro...
				//$id = $pdo->lastInsertId();
				//$stmt->close();
				$stmt = $pdo->prepare("SELECT primeraRespuesta FROM ticket WHERE PK_idTicket = :idTicket");
				$stmt -> bindParam(":idTicket", $datosModel['idTicket'], PDO::PARAM_INT);
				$stmt->execute();
				$primeraRespuesta = $stmt -> fetch(PDO::FETCH_ASSOC);

				//var_dump($primeraRespuesta['primeraRespuesta']);
				if ($primeraRespuesta['primeraRespuesta'] === "0000-00-00 00:00:00"){

					$stmt = $pdo->prepare("UPDATE ticket SET primeraRespuesta = :fecha WHERE PK_idTicket = :idTicket");
					$stmt -> bindParam(":fecha", $ahora);
					$stmt -> bindParam(":idTicket", $datosModel['idTicket'], PDO::PARAM_INT);
					$stmt->execute();
				}
				
				$stmt = $pdo->prepare("UPDATE ticket SET actualizado = :fecha WHERE PK_idTicket = :idTicket");
				$stmt -> bindParam(":fecha", $ahora);
				$stmt -> bindParam(":idTicket", $datosModel['idTicket'], PDO::PARAM_INT);
				$stmt->execute();

				$pdo->commit();

				$validator['success'] = true;
				$validator['messages'] = "Evento creado correctamente";

			}catch (PDOException $e) {

				$pdo->rollBack();

				$validator['success'] = false;
				$validator['messages'] = "Error al crear el evento ". $e->getMessage();
			}
			return $validator;
		}

		// Seleccionar Eventos de un Ticket ID()
		public static function eventosTicketModel($id, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT usuario.nombre, evento.PK_idEvento, evento.eventoDesc, evento.fechaEvento FROM $tabla INNER JOIN usuario on evento.FK_idUsuario = usuario.PK_idUsuario WHERE FK_idTicket = :idTicket");

			$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);

			$stmt->execute();

			return $stmt -> fetchAll();
	
			$stmt -> close();
		}



		public static function archivosTicketModel($id, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT archivoTicket FROM $tabla WHERE FK_idTicket = :idTicket ");

			$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);

			$stmt->execute();

			return $stmt -> fetchAll();
	
			$stmt -> close();
		}


		public static function archivosTicketsModel(){

			$stmt = Conexion::conectar()->prepare("SELECT archivoTicket, subcategoria.subcategoriaDesc, categoria.categoriaDesc FROM archivoTicket INNER JOIN ticket ON ticket.PK_idTicket = archivoticket.FK_idTicket INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria  WHERE FK_idTicket = :idTicket");

			$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);

			$stmt->execute();

			return $stmt -> fetchAll();
	
			$stmt -> close();
		}	

		
		// ticket_usuario, CLIENTE-- Funciona en detalleTicket.
		
		public static function usuarioDetalleTicketModel($datosModel, $tabla){

			//$stmt = Conexion::conectar()->prepare("SELECT ticket_usuario.FK_idTicket, ticket_usuario.FK_idUsuario, ticket_usuario.status, usuario.nombre FROM $tabla INNER JOIN usuario on ticket_usuario.FK_idUsuario = usuario.PK_idUsuario WHERE ticket_usuario.FK_idTicket = :id && ticket_usuario.status = :estatus");
			$stmt = Conexion::conectar()->prepare("SELECT usuario.nombre FROM $tabla INNER JOIN usuario on ticket_usuario.FK_idUsuario = usuario.PK_idUsuario WHERE ticket_usuario.FK_idTicket = :id && ticket_usuario.status = :estatus");

			$stmt -> bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
			$stmt -> bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();
		}


		// Agregar Archivos al ticket
		public static function guardarArchivosModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (FK_idTicket, archivoTicket) VALUES  (:idTicket, :archivo)");
			$stmt -> bindParam(":idTicket",	$datosModel['idTicket'], PDO::PARAM_INT);
			$stmt -> bindParam(":archivo",	$datosModel['archivo'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();
		}











		/**
		 *	OPERACIONES sobre los tickets 
		 * 	Cambio de estatus de los tickets.
		 */

		//	¿Es ticket hijo? Operacion: Cerrar --> COMPROBAR Abierto, Asignado, Pendiente, Resuelto y Reabierto 
		public static function esTicketHijoCerradoModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT COUNT(PK_idTicket) FROM $tabla WHERE ticket.refTicket = :id AND ticket.statusTicket != 4 AND ticket.statusTicket != 5 AND ticket.esHijo = 1 ");

			$stmt -> bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchColumn();

		}


		//	Operacion: Resuelto, COMPROBAR Abierto, Asignado, Pendiente y Reabierto
		public static function esTicketHijoResueltoModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT COUNT(PK_idTicket) FROM $tabla WHERE ticket.refTicket = :id  AND ticket.statusTicket <> 6 AND ticket.statusTicket <> 5 AND ticket.statusTicket <> 4 AND ticket.esHijo = 1 ");

			$stmt -> bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchColumn();

		}

		//	Cambiar estatus Ticket [Cancelar, Cerrar, Resuelto y Reabrir]
		public static function editarStatusTicketModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET statusTicket = :estatus WHERE ticket.PK_idTicket = :id");

			$stmt -> bindParam(":estatus", $datosModel["estatus"], PDO::PARAM_STR);
			$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
			
			return $stmt->execute();
	
			$stmt->close();
		}



		/**** OPERACIONES SOBRE LOS TICKETS- Cerrar, Cancelar  y Resolver */

		public static function resolverTicketModel($tabla, $id){

			$pdo=Conexion::conectar();

			$pdo->beginTransaction();

			$validator = array('success' => false, 'messages' => array());

			try {

				$stmt = $pdo->prepare("SELECT COUNT(PK_idTicket) FROM $tabla WHERE ticket.refTicket = :idTicket  AND ticket.statusTicket <> 6 AND ticket.statusTicket <> 5 AND ticket.statusTicket <> 4 AND ticket.esHijo = 1");

				$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
				$stmt->execute();

				$tieneHijos = (int) $stmt->fetchColumn(); 

				if($tieneHijos != 0){

					$validator['success'] = false;

					$validator['messages'] = "Error al cambiar status de este ticket, verificar que todos los tickets hijos estén resueltos.";
				} 
				else {
				
					$stmt = $pdo->prepare("UPDATE ticket SET statusTicket = 6 WHERE ticket.PK_idTicket = :idTicket");

					$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
					$stmt->execute();

					$validator['success'] = true;	
					$validator['messages'] = "Status actualizado correctamente";
				
				}

				$pdo->commit();

			}catch (PDOException $e) {

				$pdo->rollBack();

				$validator['success'] = false;
				$validator['messages'] = "Error al cambiar estatus ". $e->getMessage();
			}
			return $validator;
		}

		// Cerrado = 4
		public static function cerrarTicketModel($tabla, $id){

			$pdo=Conexion::conectar();

			$pdo->beginTransaction();

			$validator = array('success' => false, 'messages' => array());

			try {

				$ahora = date("Y-m-d H:i:s");

				$stmt = $pdo->prepare("SELECT COUNT(PK_idTicket) FROM $tabla WHERE ticket.refTicket = :idTicket AND ticket.statusTicket != 4 AND ticket.statusTicket != 5 AND ticket.esHijo = 1");

				$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
				$stmt->execute();

				$tieneHijos = (int) $stmt->fetchColumn(); 

				if($tieneHijos != 0){

					$validator['success'] = false;

					$validator['messages'] = "Error al cerrar el ticket, verificar que no tenga hijos abiertos";
				} 
				else {
				
					$stmt = $pdo->prepare("UPDATE ticket SET statusTicket = 4, cierre = :horacerrado WHERE ticket.PK_idTicket = :idTicket");
					$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
					$stmt -> bindParam(":horacerrado", $ahora);
					$stmt->execute();

					$validator['success'] = true;	
					$validator['messages'] = "Ticket cerrado correctamente";
				
				}

				$pdo->commit();

			}catch (PDOException $e) {

				$pdo->rollBack();

				$validator['success'] = false;
				$validator['messages'] = "Error al cerrar ticket ". $e->getMessage();
			}
			return $validator;
		}


		// Cancelar = 5
		public static function cancelarTicketModel($tabla, $id){

			$pdo=Conexion::conectar();

			$validator = array('success' => false, 'messages' => array());

			try {

				$stmt = $pdo->prepare("UPDATE ticket SET statusTicket = 5 WHERE ticket.PK_idTicket = :idTicket");
				$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
				$cerrar = $stmt->execute();

				if($cerrar === TRUE){
					$validator['success'] = true;
					$validator['messages'] = "Ticket cancelado correctamente";
				}else{

					$validator['success'] = false;
					$validator['messages'] = "Error al cancelar el ticket";
				}

			}catch (PDOException $e) {

				$validator['success'] = false;
				$validator['messages'] = "Error al cancelar ticket ". $e->getMessage();
			}
			return $validator;
		}

		
		// Reabrir = 7
		public static function reabrirTicketModel($tabla, $id){

			$pdo=Conexion::conectar();

			$pdo->beginTransaction();

			$validator = array('success' => false, 'messages' => array());

			try {

				$stmt = $pdo->prepare("SELECT ticket.esHijo, ticket.refTicket FROM ticket WHERE ticket.PK_idTicket = :idTicket");
				$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
				$stmt->execute();
				$fila =  $stmt -> fetchAll(PDO::FETCH_ASSOC);
				//var_dump($fila[0]['esHijo']);

				$var1 = $fila[0]['esHijo'];
				$var2 = $fila[0]['refTicket'];

				if($var2 == 0){

					$stmt = $pdo->prepare("UPDATE ticket SET statusTicket = 7 WHERE ticket.PK_idTicket = :idTicket");
					$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
					$stmt->execute();

					$validator['success'] = true;
					$validator['messages'] = "Ticket reabierto correctamente";
				}
				else
				{

					$stmt = $pdo->prepare("SELECT statusTicket FROM ticket WHERE ticket.PK_idTicket = :id");
					$stmt -> bindParam(":id", $var2, PDO::PARAM_INT);
					$stmt->execute();
					$fila =  $stmt -> fetchAll(PDO::FETCH_ASSOC);
					$var3 = $fila[0]['statusTicket'];

					if($var3 != 4){

						$stmt = $pdo->prepare("UPDATE ticket SET statusTicket = 7 WHERE ticket.PK_idTicket = :idTicket");
						$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);
						$stmt->execute();

						$validator['success'] = true;
						$validator['messages'] = "Ticket reabierto correctamente";

					}else{

						$validator['success'] = false;
						$validator['messages'] = "Error al reabrir el ticket, verifique que el ticket padre esté abierto.";

					}
				}

				$pdo->commit();

			}catch (PDOException $e) {

				$pdo->rollBack();

				$validator['success'] = false;
				$validator['messages'] = "Error al reabrir ticket ". $e->getMessage();
			}
			return $validator;
		}



        //  Resueltos = 6
        //  Cerrados = 4
        //  Cancelados = 5
		public static function TicketsStatusModel(){
			$estatus = 5;
			$id = 163;

			$stmt = Conexion::conectar()->prepare('CALL pro_tickets_estatus (?, ?)');

			$stmt -> bindParam(1, $estatus, PDO::PARAM_INT);
			$stmt -> bindParam(2, $id, PDO::PARAM_INT);
		
			$stmt -> execute();
			
			//return $stmt -> fetchAll();
			
			echo '<pre>';
				var_dump($stmt -> fetchAll());
			echo '</pre>';

			$stmt -> close();
		}


		public static function TicketsAbiertosTecnicoModel(){
			$id = 140;
			$stmt = Conexion::conectar()->prepare('CALL pro_ticketsabiertos_user (?)');

			$stmt -> bindParam(1, $id, PDO::PARAM_INT);
		
			$stmt -> execute();
			
			//return $stmt -> fetchAll();
			
			echo '<pre>';
				var_dump($stmt -> fetchAll());
			echo '</pre>';

			$stmt -> close();
		}


		public static function TicketsAbiertosAdminModel(){
			$id = 140;
			$stmt = Conexion::conectar()->prepare('CALL pro_ticketsabiertos_admin');
		
			$stmt -> execute();
			
			//return $stmt -> fetchAll();
			echo '<pre>';
				var_dump($stmt -> fetchAll());
			echo '</pre>';

			$stmt -> close();
		}





		//Deatalles() Contar Registros de una Tabla PDO
		// ticket, categoria, usuario, evento
		public static function ContarRegistrosTabla(){
			$id = 140;
			$stmt = Conexion::conectar()->prepare('SELECT * FROM evento');
		
			$stmt -> execute();
			
			
			//return $stmt -> fetchAll();
			echo '<pre>';
				var_dump($stmt -> rowCount());
			echo '</pre>';

			$stmt -> close();
		}



		public static function ContarTicketsPorAnio($mes){
			//$mes = 8;
			$anio = 2017;
			$stmt = Conexion::conectar()->prepare('SELECT COUNT(*) FROM ticket WHERE MONTH(creado) = :mes AND YEAR(creado) = :anio');
		
			$stmt -> bindParam("mes", $mes, PDO::PARAM_INT);
			$stmt -> bindParam("anio",  $anio, PDO::PARAM_INT);
			$stmt -> execute();
			
			
			//return $stmt -> fetchAll();
			echo '<pre>';
				var_dump($stmt -> fetchColumn());
			echo '</pre>';

			$stmt -> close();
		}



		public static function ContarTicketsEstatus(){
			//$mes = 8;
			$estatus = 5;
			$stmt = Conexion::conectar()->prepare('SELECT COUNT(*) FROM ticket WHERE statusTicket = :estatus');
		
			$stmt -> bindParam("estatus", $estatus, PDO::PARAM_INT);
			
			$stmt -> execute();
			
			//return $stmt -> fetchAll();
			echo '<pre>';
				var_dump($stmt -> fetchColumn());
			echo '</pre>';

			$stmt -> close();
		}


		public static function ContarTicketsPrioridad(){
			//$mes = 8;
			$prioridad = 1;
			$stmt = Conexion::conectar()->prepare('SELECT COUNT(*) FROM ticket WHERE prioridadTicket = :prioridad AND statusTicket <> 4 AND statusTicket <> 5 AND statusTicket <> 6' );
		
			$stmt -> bindParam("prioridad", $prioridad, PDO::PARAM_INT);
			
			$stmt -> execute();           
			
			//return $stmt -> fetchAll();
			echo '<pre>';
				var_dump($stmt -> fetchColumn());
			echo '</pre>';

			$stmt -> close();
		}


		//Registro Ticket-Usuario
		/*
		public static function guardarTicketUsuarioModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO  $tabla (FK_idTicket, FK_idUsuario, 'status') VALUES (:idTicket, :usuario, :descripcion)");

			$stmt -> bindParam(":idTicket", 	$datosModel['id'], PDO::PARAM_INT);
			$stmt -> bindParam(":usuario", 		$datosModel['usuario'], PDO::PARAM_STR);	//Creado, Tecnico, Cliente
			$stmt -> bindParam(":descripcion", 	$datosModel['descripcion'], PDO::PARAM_STR);	// ALTA, ATENCION, CLIENTE

			return $stmt->execute();

			$stmt->close();

		}*/


		// Obtener Eventos
		/*
		public static function contarEventosTickets($id, $tabla){

			$stmt = Conexion::conectar()->prepare("SELECT COUNT(eventoDesc) FROM $tabla WHERE FK_idTicket = :id");

			$stmt -> bindParam(":id", $id, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchColumn();

			//$stmt -> close();
		}*/


		//	Obtener Usuario PENDIENTE....
		/*
		public static function obtenerUsuario($id, $estatus){

			$stmt = Conexion::conectar()->prepare("SELECT usuario.nombre FROM ticket_usuario INNER JOIN usuario on ticket_usuario.FK_idUsuario = usuario.PK_idUsuario WHERE ticket_usuario.status = :estatus && ticket_usuario.FK_idTicket = :id");

			$stmt -> bindParam(":id", $id, PDO::PARAM_INT);
			$stmt -> bindParam(":estatus", $estatus, PDO::PARAM_STR);
			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();
		}*/
    }
?>