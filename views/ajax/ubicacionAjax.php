<?php

require_once "../../controllers/ubicacionControllers.php";
require_once "../../models/ubicacionModels.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxUbicacion{


	public function mostrarUbicacion(){

		$respuesta = UbicacionController::mostrarUbicacionController();
		print json_encode($respuesta);
	}


	public function ubicacionPorID(){
		                                  
		$respuesta = UbicacionController::mostrarIDUbicacionController();
		print json_encode($respuesta);	
	}


	public function guardarUbicacion(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = UbicacionController::guardarUbicacionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Ubicación registrada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al registrar ubicación";
		}

		echo json_encode($salida);
	}


	public function editarUbicacion(){
		
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = UbicacionController::editarUbicacionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Ubicación actualizada correctamente";
		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar ubicación";
		}

		echo json_encode($salida);

	}


	public function desactivarUbicacion(){

		$salida = array('success' => false, 'mensaje' => array());

		$tieneUsuariosActivos = UbicacionController::tieneUsuariosController();

		if ($tieneUsuariosActivos == 0){

			$respuesta = UbicacionController::desactivarUbicacionController();

			if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Ubicación desactivada correctamente";
			}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al desactivar ubicación";
			}

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar ubicación, verifica que no tenga usuarios activos";
		}	

		echo json_encode($salida);
	}


	public function activarUbicacion(){

		$salida = array('success' => false, 'mensaje' => array());		

		$respuesta = UbicacionController::activarUbicacionController();

		if ($respuesta === TRUE)
			{
				$salida['success'] = true;
				$salida['mensaje'] = "Ubicación activada correctamente";
		}else{
				$salida['success'] = false;
				$salida['mensaje'] = "Error al activar ubicación";
		}		

		echo json_encode($salida);
	}


	public function comboBoxUbicacion(){
		                                  
		$respuesta = UbicacionController::comboBoxUbicacionController();
		print json_encode($respuesta);	
	}


}

if($opcion == 'getUbicacion'){
	$getUbi = new AjaxUbicacion();
	$getUbi -> comboBoxUbicacion();
}

if($opcion == 'mostrar'){
	$mostrar = new AjaxUbicacion();
	$mostrar -> mostrarUbicacion();
}

if($opcion == 'getID'){
	$id = new AjaxUbicacion();
	$id -> ubicacionPorID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxUbicacion();
	$guardar -> guardarUbicacion();
}

if ($opcion == 'editar'){
	$editar = new AjaxUbicacion();
	$editar -> editarUbicacion();
}

if ($opcion == 'desactivar'){
	$desactivar = new AjaxUbicacion();
	$desactivar -> desactivarUbicacion();
}

if ($opcion == 'activar'){
	$activar = new AjaxUbicacion();
	$activar -> activarUbicacion();
}