var tblCategoria, tblSubcategoria;

/* =======================================
    AREA PARA OPERACIONES CATEGORIA.
=======================================*/

/*	=======	M O S T R A R Categorias	=======*/ 
tblCategoria = $("#categorias").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/categoriaAjax.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		},
		{
			responsivePriority: 2,
			targets: 0
		}
	]
});


/*	=======	A G R E G A R  Categoria	=======*/
$("#btnAgregarCategoria").on('click', function () {

	$("#frmRegistrarCategoria")[0].reset();

	$("#frmRegistrarCategoria").unbind('submit').bind('submit', function () {

		var form = $(this);
		var nuevaCategoria = $("#altaCategoria").val();

		if (nuevaCategoria){
			$.ajax({
				url: "views/ajax/categoriaAjax.php?op=guardarCategoria",
                type: form.attr('method'),
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
					if (response.success == true){
						$("#frmRegistrarCategoria")[0].reset();
						tblCategoria.ajax.reload(null, false);
						$("#agregarCategoria").modal('hide');
						successSwal(response.mensaje);
					}else{
						$("#agregarCategoria").modal('hide');
						errorSwal(response.mensaje);
					}
				}
			});
		}
		return false;
	});
});


/*	=======	E D I T A R Categoria	=======*/
function editarCategoria(idCategoria) {
	if (idCategoria) {
		
		// Buscar datos de la categoria
		$.ajax({
			url: "views/ajax/categoriaAjax.php?op=getID",
			type: 'POST',
			data: {
				idCategoria: idCategoria
			},
			dataType: 'json',
			success: function (response) {

				// Se almacena el valor del registro en input e // ID Categoria para tomar referencia 
				$("#categoriaEditar").val(response.categoriaDesc);
				$("#idCategoriaEditar").val(response.PK_idCategoria);

				$("#frmEditarCategoria").unbind('submit').bind('submit', function () {
					var form = $(this);
					// Capturamos lo que tiene la caja de texto.
					var categoriaEdicion = $("#categoriaEditar").val();

					if (categoriaEdicion) {
						$.ajax({
							url: "views/ajax/categoriaAjax.php?op=editar",
							type: form.attr('method'), // Es lo mismo a poner POST
							data: form.serialize(),
							dataType: 'json',
							success: function (response) {

								if (response.success == true) {

									tblCategoria.ajax.reload(null, false);
									$("#editar_categoria").modal('hide');				
									successSwal(response.mensaje);

								} else {
									//Cerrar ventana y mostrar mensaje de error.
									$("#editar_categoria").modal('hide');						
									errorSwal(response.mensaje);
								}
							}
						});
					}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	A C T I V A R  Categoria	=======*/
function activarCategoria(idCategoria = null){
	if(idCategoria){
		swal({
			title: "¿Realmente desea A C T I V A R esta categoria?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/categoriaAjax.php?op=activarcategoria",
					type: 'post',
					data: {idCategoria : idCategoria},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblCategoria.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Categoria	=======*/
function desactivarCategoria(idCategoria = null){
	if(idCategoria){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta categoria?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/categoriaAjax.php?op=desactivarcategoria",
					type: 'post',
					data: {idCategoria : idCategoria},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblCategoria.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}



/* =======================================
    AREA PARA OPERACIONES SUBCATEGORIA.
=======================================*/


/*	=======	M O S T R A R  Subcategorias	=======*/
tblSubcategoria = $("#subCategorias").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/categoriaAjax.php?op=mostrarsubcategoria",
	"order": [],
	"columns": [
		{	"orderable": false	},
			null,
		{	"orderable": false	},
		{	"orderable": false	}
	],
    responsive: true,
    "language": {
		"url": "views/js/json/Spanish.json"
    },
    columnDefs: [
        {
            responsivePriority: 1,
            targets: 0
        },
        {
            responsivePriority: 2,
            targets: 0
        }
	]
});


/*	=======	A G R E G A R  Subcategoria	=======*/
$("#btnAgregarSubcategoria").on('click', function () {
	
	$("#frmRegistrarSubcategoria")[0].reset();

	$("#frmRegistrarSubcategoria").unbind('submit').bind('submit', function () {

		var form = $(this);
		var nuevaSubcategoria = $("#altaSubcategoria").val();
		var deCategoria = $("#deCategoria").val();
		
		if (nuevaSubcategoria && deCategoria) {
			$.ajax({

				url: "views/ajax/categoriaAjax.php?op=guardarSubcategoria",
                type: form.attr('method'),
                data: form.serialize(),
                dataType: 'json',
                success: function (response) {
                //$(".form-group").removeClass('has-error').removeClass('has-success');
                	if (response.success == true) {
                            //Recargar tabla subcategoria, limpiar campos, cerrar ventanas y mostrar mensaje de éxito.
                    	$("#frmRegistrarSubcategoria")[0].reset();
                    	$("#agregarSubcategoria").modal('hide');
                    	tblSubcategoria.ajax.reload(null, false);
                   		successSwal(response.mensaje);
                	} else {
                            //Cerrar ventana y mostrar mensaje de error.
                    	$("#agregarSubcategoria").modal('hide');
                    	errorSwal(response.mensaje);
                    }
                }
			});
		}
		return false;
	});
});


/*	=======	E D I T A R  Subcategoria	=======*/
function editarSubcategoria(idSubcategoria = null){
	if (idSubcategoria){

		$.ajax({
			url: "views/ajax/categoriaAjax.php?op=getIDSubcategoria",
            type: 'POST',
            data: {
                idSubcategoria: idSubcategoria
			},
			
			dataType: 'json',
			success: function (response) {

				// Valores de referencia
				$("#subcategoriaEditar").val(response.subcategoriaDesc);
				$("#categoriaSubEditar").val(response.FK_idCategoria);
				$("#idSubcategoriaEditar").val(response.PK_idSubcategoria);
				
				$("#frmEditarSubcategoria").unbind('submit').bind('submit', function () {
					var form = $(this);
					var nombreSubcategoria = $("#subcategoriaEditar").val();
					var idCategoria = $("#categoriaSubEditar").val();

					if (nombreSubcategoria && idCategoria){
						$.ajax({

							url: "views/ajax/categoriaAjax.php?op=editarSubcategoria",

							type: form.attr('method'),
                            data: form.serialize(),
                            dataType: 'json',
                            success: function (response) {
                                if (response.success == true) {
                                    // Recargar tabla, cerrar ventana y mostrar mensaje de éxito
                                    tblSubcategoria.ajax.reload(null, false);
                                    $("#editar_subcategoria").modal('hide');
									successSwal(response.mensaje);

                                } else {
                                    //Cerrar ventana y mostrar mensaje de error.
                                    $("#editar_subcategoria").modal('hide');
									errorSwal(response.mensaje);
                                }
                            }

						});
					}
					return false;
				});
			}
		});
	}
}


/*	=======	D E S A C T I V A R  Subcategoria	=======*/
function desactivarSubcategoria(idSubcategoria = null){
	if(idSubcategoria){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta subcategoria?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/categoriaAjax.php?op=desactivarSubcategoria",
					type: 'post',
					data: {idSubcategoria : idSubcategoria},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblSubcategoria.ajax.reload(null, false);
							successSwal(response.mensaje);
							
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	A C T I V A R  Subcategoria	=======*/
function activarSubcategoria(idSubcategoria = null){
	if(idSubcategoria){
		swal({
			title: "¿Realmente desea A C T I V A R esta subcategoria?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/categoriaAjax.php?op=activarSubcategoria",
					type: 'post',
					data: {idSubcategoria : idSubcategoria},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblSubcategoria.ajax.reload(null, false);
							successSwal(response.mensaje);
							
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/** Mensajes de Acierto y Error. Aplica area de Categoria y Subcategoria */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}