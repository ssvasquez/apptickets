<?php 
if(!isset($_SESSION)){
session_start();
}
ini_set('date.timezone','America/Mexico_City');
require ('funciones.php');
//var_dump($_SESSION['u_usuario']['PK_idUsuario']);
class TicketsController{

	/**
	 * 	=================================================================================
	 * 		Area CRUD + Tickets
	 * =================================================================================
	 */

	public static function mostrarTicketAdminController(){

		// Mostrar datos para Administradores
		if ($_SESSION["rol"] == 1){

			$respuesta = TicketsModels::mostrarTicketsAdminModel("vw_tickets");

			$datos = array('data' => array());
		

			$actionButton = '';
			foreach ($respuesta as $item ){
				$actionButton ='
				<div class="btn-group">
					<button type="button" class="btn btn-secondary">Acciones</button>
					<button type="button" class="btn btn-secondary btn-sm dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">    
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalEditarTicket" onclick="editarTicket('.$item['PK_idTicket'].')"> <span class="fa fa-edit"></span> Editar</a>
						<a class="dropdown-item" href="#" onclick="cancelarTicket('.$item['PK_idTicket'].')"> <span class="fa fa-ban"></span> Cancelar </a>
						<a class="dropdown-item" href="#" onclick="cerrarTicket('.$item['PK_idTicket'].')"> <span class="fa fa-lock"></span> Cerrar </a>    
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#registrarEvento" id="btnCrearEvento" onclick="crearEvento('.$item['PK_idTicket'].')"> <span class="fa fa-reply"></span> Crear evento</a>
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#registrarArchivo" onclick="agregarArchivos('.$item['PK_idTicket'].')"> <span class="fa fa-file"></span> Subir Archivos</a>
						<a class="dropdown-item" href="#" onclick="resueltoTicket('.$item['PK_idTicket'].')"> <span class="fa fa-check"></span> Ticket resuelto</a>
					</div>
				</div>';

				$user = explode("|", $item['usuarios']);

				//	Almacenar los datos en el arreglo.
				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					estatus($item['statusTicket']),
					self::PrioridadBadge($item['prioridadTicket']),
					$user[0],
					$user[1],
					$actionButton
				);
			}
			//Mandar un array a AJAX;
			return $datos;
		}

		// Mostrar datos para tecnicos...
		elseif ($_SESSION["rol"] == 2){
			$datosController = array("idusuario" => $_SESSION["id"]);

			$respuesta = TicketsModels::mostrarTicketsTecnicoModel($datosController, "vw_tickets");

			$datos = array('data' => array());

			foreach ($respuesta as $row => $item){
				
				$btnEvento = '';
				$btnEvento ='
				<button data-toggle="modal" data-target="#registrarEvento" type="button" class="btn btn-info btn-sm" title="Crear Evento" onclick="crearEvento('.$item['PK_idTicket'].')"><i class="fa fa-reply"></i></button>
				<button type="button" class="btn btn-danger btn-sm" title="Cancelar Ticket" onclick="cancelarTicket('.$item['PK_idTicket'].')"><i class="fa fa-ban"></i></button>
				<button type="button" class="btn btn-success btn-sm" title="Ticket Resuelto" onclick="resueltoTicket('.$item['PK_idTicket'].')"><i class="fa fa-check"></i></button>
				<button type="button" class="btn btn-danger btn-sm" title=" Cerrar Ticket" onclick="cerrarTicket('.$item['PK_idTicket'].')"><i class="fa fa-lock"></i></button>
				<button type="button" data-toggle="modal" data-target="#registrarArchivo" class="btn btn-warning btn-sm" title=" Agregar Archivo" onclick="agregarArchivos('.$item['PK_idTicket'].')"><i class="fas fa-file"></i></button>
				
				';

				$datosAlta = array(	"id" => $item['PK_idTicket'], "estatus" => "ALTA");
				$alta = TicketsModels::usuarioDetalleTicketModel($datosAlta, "ticket_usuario");
				//	Almacenar los datos en el arreglo.
				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',	
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					estatus($item['statusTicket']),
					self::PrioridadBadge($item['prioridadTicket']),
					$alta["nombre"], //self::Creado($item['PK_idTicket']),
					$item['tecnico'], //self::Tecnico($item['PK_idTicket']),
					$btnEvento
				);
			}
			return $datos;
		}
	}


	public static function detalleTicketController($id){
		//$id = 438;
		$respuesta = TicketsModels::detalleTicketModel("ticket", $id);
		$datosAlta = array(	"id" => $id, "estatus" => "ALTA");
		$datosAsignado = array(	"id" => $id, "estatus" => "ATENCION");
		$datosCliente = array(	"id" => $id, "estatus" => "CLIENTE");

		//eventosTicketModel($datosModel, $tabla
		$alta = TicketsModels::usuarioDetalleTicketModel($datosAlta, "ticket_usuario");
		$asignado = TicketsModels::usuarioDetalleTicketModel($datosAsignado, "ticket_usuario");
		$cliente = TicketsModels::usuarioDetalleTicketModel($datosCliente, "ticket_usuario");
		
		//var_dump($alta);
		echo'
		<div class="col-md-8" id="contenedorDetalle">
			<div class="alert alert-success"><i class="fa fa-info-circle"></i> Detalles</div>

			<h2 class="card-title">#'.$respuesta['PK_idTicket'].' » 
				<span style="font-size:70%">'.$respuesta['tituloTicket'].'<span>
			</h2>

			<h4 class="blockquote-categoria" > 
				<small> Categoria: '.$respuesta['categoriaDesc'].' - '.$respuesta['subcategoriaDesc'].' </small>
			</h4>

			<blockquote class="blockquote detalleTicket">
				<p class="mb-0 text-muted"> '.$respuesta['descripcionTicket'].' </p>
			</blockquote>

			<div class="usuarios-ticket text-right pr-3"> 
				<small>Creado por: <strong>'.$alta['nombre'].'</strong> || Asignado a: <strong>'.$asignado['nombre'].'</strong></small> 
			</div>

			<div class="usuarios-ticket text-right pr-3">
				<small>Para: <strong>'.$cliente['nombre'].'</strong> de  '.$respuesta['ubicacionDesc'].' </small>
			</div>

			<div class="row mb-5">
				<h5 class="ml-1">
					<small class="text text-muted"> • Estatus: </small>'.Estatus($respuesta['statusTicket']).'
				</h5>
				<h5 class="ml-1">
					<small class="text text-muted"> • Prioridad: </small>'.Prioridad($respuesta['prioridadTicket']).'
				</h5>
			</div>';

			// Imprimir eventos (Si hay eventos imprimirlos, sino no hacer nada)...
			$eventos = TicketsModels:: eventosTicketModel($id, "evento");

			if (count($eventos) != 0 ){
				echo '<div class="mt-3 alert alert-success"><i class="fa fa-info-circle"></i> Eventos</div>
					<div class="card-block">';

					$i = 0;
					foreach ($eventos as $row => $item){
						$i++;
						echo '
						<section class="comments">
							<article class="comment">
								<div class="comment-body">
									<div class="text">
										<div><strong> # '.$i.' </strong> '.$item['eventoDesc'].'
											<p class="blockquote-categoria text-right">
												<small>
													'.$item['nombre'].'
													<span><i class="fa fa-clock-o"></i>
														'.FechaCreado(date_create($item['fechaEvento'])).'
													</span>
												</small>
											</p>
										</div>
									</div>
								</div>
							</article>
						</section>';
					}
					echo '</div>';
			}else{
				echo '';
			}
			//var_dump(count($eventos));
		echo'</div>';

		
		$archivos = TicketsModels:: archivosTicketModel($id, "archivoticket");
		
		echo'
		<div class="col-md-4" id="contenedorDetalle">	
			<div class="alert alert-success"><i class="fa fa-file"></i> Archivos</div>
			<div class="row">';
			
			foreach ($archivos as $row => $item){

				//$extension = explode(".", $item["archivoTicket"]);

				$extension = pathinfo($item["archivoTicket"], PATHINFO_EXTENSION);

				$archivo = substr($item["archivoTicket"], 3);
				//echo $archivo;

				// echo $extension;

				if ($extension != "pdf" && $extension != "PDF"){

					//echo '<br>';
					echo'
						<div class="col-sm-3">
						
							<a href="views/'.$archivo.'" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-4">
								<img src="views/'.$archivo.'" class="img-fluid">
							</a>
						
						</div>';
				}
			}
			
			echo'</div>';


			// Archivos PDF

			echo'<div class="row mt-1">';

			foreach ($archivos as $row => $item){

				$extension = pathinfo($item["archivoTicket"], PATHINFO_EXTENSION);
	
				$archivo = substr($item["archivoTicket"], 3);
	
				if ($extension == "pdf" || $extension == "PDF"){
	
					echo'
					<div class="col-12 col-sm-4">
						<div class="card" style="margin-bottom:3px">
							<div class="card-block">
								<div class="col-12 text-center">
								<a class="text-center" style="font-size:2rem" href="views/'.$archivo .'" target="_blank"><i class="fas fa-file-pdf" aria-hidden="true"></i> </a>
								<p class="" style="text-overflow: ellipsis;overflow: hidden">'.basename($item['archivoTicket']).'</p>
								</div>	
							</div>
						</div>
					</div>';
				}
	
			}
		echo'</div>
		</div>';
	}


	// FUNCIONANDO Alta Tickets
	public static function heredarTicketController(){

		$idTicket = $_POST['idTicket'];

		$respuesta = TicketsModels::ticketsHeredarModel("ticket", $idTicket);

		return $respuesta;
	}


	public static function guardarTicketController(){
		$datosController = array(
			"ubicacion"		=>	$_POST['regSucursal'],
			"subcategoria"	=>	$_POST["regSubcategoria"],
			"titulo"	=>	$_POST["regTema"],
			"descripcion"	=>	 $_POST["regDescripcion"],
			"estatus"	=>	$_POST["regStatus"],
			"prioridad"	=>	$_POST["regPrioridad"],
			"alerta"	=>	1,
			"escalado"	=>	1,
			"tipo"		=>	$_POST["regTipo"],
			"hijo"		=>	$_POST["regEshijo"],
			"usuarioCreado"	=> $_SESSION['id'],
			"usuarioTecnico"	=> $_POST["regTecnico"],
			"usuarioCliente"	=> $_POST["regCliente"],
			"referencia"	=>	(empty($_POST["regReferencia"])) ? 0 : $_POST["regReferencia"]
		);

		$respuesta = TicketsModels::guardarTicketModel($datosController, "ticket");

		return $respuesta;
	}


	public static function mostrarTicketIdAdminController(){
		$idTicket = $_POST['idTicket'];

		$respuesta = TicketsModels::mostrarIDTicketModel('ubicacion', $idTicket);
		return $respuesta;
	}


	public static function editarTicketController(){

		$datosController = array(
			"id" => 	$_POST['editID'],
			"titulo" => $_POST["editTema"],
			"descripcion" => $_POST["editDescripcion"],
			"estatus" => 	$_POST["editStatus"],
			"prioridad" => 	$_POST["editPrioridad"],
			"escalado" => 	$_POST["editEscalado"],
			"tipo" => 	$_POST["editTipo"]);

		$respuesta = TicketsModels::editarTicketModel($datosController, "ticket");

		return $respuesta;
	}
	

	// Tickets Resueltos...6
	public static function ticketsResueltosAdminController(){
		
		if($_SESSION['rol'] == 1){
			$datos = array('data' => array());

			$respuesta = TicketsModels::ticketsEstatusAdminModel("vw_tickets", 6);
			
			foreach ($respuesta as $row => $item){

				$user = explode("|", $item['usuarios']);

				$actionButton ='
				<button type="button" class="btn btn-success btn-sm" title="Reabrir Tickets" onclick="reabrirTicket('.$item['PK_idTicket'].')"><i class="fa fa-unlock"></i></button>
				<button type="button" class="btn btn-danger btn-sm" title="Cerrar Tickets" onclick="cerrarTicket('.$item['PK_idTicket'].')"><i class="fa fa-lock"></i></button>';

				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					'Resuelto',
					self::PrioridadBadge($item['prioridadTicket']),
					$user[0],
					$user[1],
					$actionButton
				);
			}		
			return $datos;
		}

		elseif($_SESSION["rol"] == 2){

			$datos = array('data' => array());

			$datosController = array("estatus" => 6, "idusuario" => $_SESSION["id"]);

			$respuesta = TicketsModels::ticketsEstatusTecnicoModel($datosController, "vw_tickets");

			//if (count($respuesta) > 0) {
			foreach ($respuesta as $row => $item){

				$actionButton ='
				<button type="button" class="btn btn-success btn-sm" title="Reabrir Tickets" onclick="reabrirTicket('.$item['PK_idTicket'].')"><i class="fa fa-unlock"></i></button>
				<button type="button" class="btn btn-danger btn-sm" title="Cerrar Tickets" onclick="cerrarTicket('.$item['PK_idTicket'].')"><i class="fa fa-lock"></i></button>';

				$datosAlta = array(	"id" => $item['PK_idTicket'], "estatus" => "ALTA");
				$alta = TicketsModels::usuarioDetalleTicketModel($datosAlta, "ticket_usuario");


				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					'<Resuelto',
					self::PrioridadBadge($item['prioridadTicket']),
					$alta["nombre"],
					$item['tecnico'],
					$actionButton
				);
			}
			return $datos;
		}
		
	}

	// Tickets Cerrados...4
	public static function ticketsCerradosAdminController(){

		if($_SESSION['rol'] == 1){
			$datos = array('data' => array());

			$respuesta = TicketsModels::ticketsEstatusAdminModel("vw_tickets", 4);

			foreach ($respuesta as $row => $item){

				$user = explode("|", $item['usuarios']);

				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					'<p class="text-success"><i class="fa fa-lock"></i> Cerrado</p>',
					self::PrioridadBadge($item['prioridadTicket']),
					$user[0],
					$user[1]
				);
			}
			return $datos;

		}

		elseif($_SESSION["rol"] == 2){

			$datos = array('data' => array());

			$datosController = array("estatus" => 4, "idusuario" => $_SESSION["id"]);

			$respuesta = TicketsModels::ticketsEstatusTecnicoModel($datosController, "vw_tickets");

			foreach ($respuesta as $row => $item){

				//$user = explode("|", $item['usuarios']);
				$datosAlta = array(	"id" => $item['PK_idTicket'], "estatus" => "ALTA");
				$alta = TicketsModels::usuarioDetalleTicketModel($datosAlta, "ticket_usuario");


				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					'<p class="text-success"><i class="fa fa-lock"></i> Cerrado</p>',
					self::PrioridadBadge($item['prioridadTicket']),
					$alta["nombre"],
					$item['tecnico']
				);
			}
			return $datos;
		}

		
	}

	// Tickets Cancelados...5
	public static function ticketsCanceladosAdminController(){

		if($_SESSION['rol'] == 1){

			$datos = array('data' => array());

			$respuesta = TicketsModels::ticketsEstatusAdminModel("vw_tickets", 5);

			foreach ($respuesta as $row => $item){

				$user = explode("|", $item['usuarios']);

				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					'<p class="text-danger"><i class="fa fa-ban"></i> Cancelado</p>',
					self::PrioridadBadge($item['prioridadTicket']),
					$user[0],
					$user[1]
				);
			}
			return $datos;
		}
		elseif($_SESSION["rol"] == 2){

			$datos = array('data' => array());

			$datosController = array("estatus" => 5, "idusuario" => $_SESSION["id"]);

			$respuesta = TicketsModels::ticketsEstatusTecnicoModel($datosController, "vw_tickets");

			foreach ($respuesta as $row => $item){

				$datosAlta = array(	"id" => $item['PK_idTicket'], "estatus" => "ALTA");
				$alta = TicketsModels::usuarioDetalleTicketModel($datosAlta, "ticket_usuario");

				$datos['data'][] = array(
					'<h1 class="badg badge-primary">'.$item['PK_idTicket'].'</h1>',
					Fecha($item['actualizado']),
					$item['refTicket'],
					self::Categoria($item['categoriaDesc'], $item['subcategoriaDesc']),
					'<a href="index.php?action=detalleticket&id='.$item['PK_idTicket'].'">'.self::Requerimiento($item['tituloTicket'], $item['descripcionTicket'],  $item['Eventos']).'</a>',
					'<p class="text-success"><i class="fa fa-lock"></i> Cerrado</p>',
					self::PrioridadBadge($item['prioridadTicket']),
					$alta["nombre"],
					$item['tecnico']
				);
			}
			return $datos;
		}
	}


	public static function crearEventoController(){

		$datosController = array(
			"idTicket"	=> $_POST['NoTicket'],
			"idUsuario" => $_SESSION['id'],
			"evento"	=> $_POST["eEvento"]);

		$respuesta = TicketsModels::crearEventoModel($datosController, "evento");

		return $respuesta;
	}

	public static function resolverTicketController(){
		$respuesta = TicketsModels::resolverTicketModel("ticket", $_POST['idTicket']);

		return $respuesta;
	}

	public static function cerrarTicketController(){
		$respuesta = TicketsModels::cerrarTicketModel("ticket", $_POST['idTicket']);

		return $respuesta;
	}

	public static function cancelarTicketController(){
		$respuesta = TicketsModels::cancelarTicketModel("ticket", $_POST['idTicket']);

		return $respuesta;
	}

	public static function reabrirTicketController(){
		$respuesta = TicketsModels::reabrirTicketModel("ticket", $_POST['idTicket']);

		return $respuesta;
	}


	static function Categoria($categoria, $subcategoria){
		return '<li>
			<strong>'.$categoria.'</strong>
			<ul>
				<li style="list-style-type:square">'.$subcategoria.'</li>
			</ul>
		</li>';
	}

	static function Requerimiento($titulo, $descripcion, $eventos){

		return '<li class="ticket">
			<span style="font-weight: bold">'.$titulo.':</span> 
			<span>'.$descripcion.'</span>
		</li>
		<small class="pull-right text-muted">'.$eventos. ' Evento(s)</small>';
	}

	
	static function PrioridadBadge($arg){
		switch($arg){
			case 1: 
				$prioridadb = '<span class="badge badge-critica">Critica</span>';
				break;
			case 2: 
				$prioridadb = '<span class="badge badge-urgente">Urgente</span>'; 
				break;
			case 3: 
				$prioridadb = '<span class="badge badge-alto">Alto</span>';
				break;
			case 4: 
				$prioridadb = '<span class="badge badge-medio">Medio</span>'; 
				break;
			case 5: 
				$prioridadb = '<span class="badge badge-bajo">Bajo</span>';
				break;
			default:
				$prioridadb = '<span class="badge badge-bajo"></span>';

		}
		return $prioridadb;
	}


	public static function guardarArchivoController(){

		//var_dump($_POST["idTicket"]);

		if (isset($_POST["idTicket"])){

			$respuesta = TicketsModels::detalleTicketModel("ticket", $_POST["idTicket"]);

			$raiz = "../archivos_tickets/". $respuesta['categoriaDesc'] ."/". $respuesta['subcategoriaDesc']."/";

			if (!file_exists($raiz)) {
				mkdir($raiz, 0777, true);
			}

			$files_arr = array();

			//$total = count($_FILES['archivosTicket']['name']);
			// Por la libreria 
			
			//$count = count($_FILES['files']['name']);

			
			if(isset($_FILES["archivosTicket"]["tmp_name"])){

				
				//echo '';
				$total = count($_FILES['archivosTicket']['name']);

				//$total = $total - ($total / 2);
				//var_dump($total);

				for( $i=0 ; $i < $total ; $i++ ) {
					//$archivo = $_FILES['archivoTicket']['name'][$i];
					//$ext = pathinfo($archivo, PATHINFO_EXTENSION);

					$name = basename($_FILES["archivosTicket"]["name"][$i]);

					list($base, $extension) = explode('.', $name);

					$nuevo = $_POST["idTicket"] ."_". date("Ymd")."-".mt_rand(100, 999).".".$extension;
				
					$archivo = $raiz.$nuevo;	

					if(move_uploaded_file($_FILES["archivosTicket"]["tmp_name"][$i], $archivo)){

						//$files_arr = array();

						$files_arr[] = $archivo;

					}
				}

				foreach ($files_arr as $archivo) 
				{
	
					$datosController = [
						"idTicket" => $_POST["idTicket"],
						"archivo" => $archivo
					];

					$respuesta = TicketsModels::guardarArchivosModel($datosController, "archivoticket");
				}

				return $respuesta;
			}
		}
	}
}