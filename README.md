# App Tickets

Sistema de Tickets para control de incidencias.

## Tecnologías
<ul>
  <li>HTML5</li>
  <li>CSS3</li>
  <li>JavaScript</li>
  <li>PHP</li>
  <li>JSON</li>
  <li>AJAX</li>
  <li>jQuery</li>
  <li>MySQL</li>
</ul> 


## Librerías
<ul>
  <li>Bootstrap v4</li>
  <li>SweetAlert</li>
  <li>DataTables</li>
  <li>FileInput</li>
</ul> 


