<?php

   // error_reporting(E_ALL);
    require_once "db.php";

    class calendarioModel{

        public static function mostrarCalendarioModel($tabla){

            $stmt = Conexion::conectar()->prepare("SELECT PK_idTicket, tituloTicket, creado, cierre FROM $tabla" );

            $stmt -> execute();

            return $stmt -> fetchAll();

		    $stmt -> close();
             
        }
    }