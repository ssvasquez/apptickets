var tblKB;
$(document).ready(function() {
	//Lenguaje para los parametros de datatables.
	tblKB = $("#kb").DataTable({
		"ajax": "views/ajax/kbAjax.php?op=mostrarkb",
		"order": [],
		responsive: true,
		"language":
		{
			"url": "views/js/json/Spanish.json"
		},
		columnDefs: [
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: 0 }
		]		
	});
});



$("#pdfKB").fileinput({
	theme:'fas',
	browseClass: "btn btn-secondary btn-block",
	allowedFileExtensions: ['PDF'],
	maxFilesNum: 1,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false,
	browseLabel:"Examinar PDF..."
});


function agregarPDF(idKB) {
	$("#frmRegistrarPDF")[0].reset();
	if (idKB) {

		$.ajax({
			url: "views/ajax/kbAjax.php?op=getID",
			type: 'POST',
			data: {
				idKB: idKB
			},
			dataType: 'json',
			success: function (response) {

				$("#idKBPDF").val(response.PK_idKB);
				//$("#marcaProductoPDF").val(response.marca_prod);

				$("#frmRegistrarPDF").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmRegistrarPDF"));
					var pdfProducto = $('#pdfKB')[0].files[0];
					formData.append('pdf', pdfProducto);
					$.ajax({
						url: "views/ajax/kbAjax.php?op=pdf",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblKB.ajax.reload(null, false);
								$("#registrarPDF").modal('hide');
								successSwal(response.mensaje);
							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#registrarPDF").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}



// Agregar nuevo KB
$("#btnagregarKB").on('click', function() {
	$("#frmregistrarKB")[0].reset();
	$(".form-group").removeClass('has-error').removeClass('has-success');
	$(".messages").html("");
	
	$("#frmregistrarKB").on("submit", function(e){
		e.preventDefault();
		var formData = new FormData(document.getElementById("frmregistrarKB"));
		var ruta = "views/ajax/kbAjax.php?op=guardar";
		$.ajax({
			url: ruta,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			dataType : 'json',
			success: function(response)
			{
			   if(response.success == true) {
							// Limpiar los campos
					$("#frmregistrarKB")[0].reset();
					$("#registrarKB").modal('hide');
					// Recargar la tabla
					tblKB.ajax.reload(null, false);
					successSwal(response.mensaje);
					//Error	
				} else {
					errorSwal(response.mensaje);
				}
			}
		});
	});
});


// Eliminar
function desactivarKB(idKB = null){
	if(idKB){
		swal({
			title: "¿Realmente desea E L I M I N A R esta categoria?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/KBAjax.php?op=eliminar",
					type: 'post',
					data: {idKB : idKB},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblKB.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}




// Editar ///
function editarKB(PK_idKB) {
	if (PK_idKB) {		
		// Buscar datos del usuario
		$.ajax({
			url: "views/ajax/kbAjax.php?op=getID",
			type: 'POST',
			data: {
				idKB : PK_idKB
			},
			dataType: 'json',
			success: function (response) {

				$("#editSolucion").val(response.solucionKB);

				$(".modaleditarKB").append('<input type="hidden" name="idKB" id="idKB" value="'+response.PK_idKB+'"/>');
				$("#frmeditarKB").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmeditarKB"));
					
					$.ajax({
						url: "views/ajax/kbAjax.php?op=editarkb",
						type: 'POST',
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {
							if (response.success == true) {
								tblKB.ajax.reload(null, false);
								$("#modaleditarKB").modal('hide');
								successSwal(response.mensaje);
							} else {
								//Cerrar ventana y mostrar mensaje de error.
								//$("#modaleditarKB").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
					return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}

/*
function editarKB(PK_idKB = null) {
	if(PK_idKB) {
		$(".form-group").removeClass('has-error').removeClass('has-success');
		$(".text-danger").remove();
		$(".edit-messages").html("");
		$("#idKB").remove();
		
		// Buscar datos del Ticket
		$.ajax({
			url:"crudKB.php?op=getID",
			type: 'POST',
			data: {idKB : PK_idKB},
			dataType: 'json',
			success:function(response) {
				//Se obtiene el valor del registro

				$("#editSolucion").val(response.solucionKB);

				// ID categoria
				$(".modaleditarKB").append('<input type="hidden" name="idKB" id="idKB" value="'+response.PK_idKB+'"/>');
				$("#frmeditarKB").unbind('submit').bind('submit', function() {
					$(".text-danger").remove();
					var form = $(this);
					
					var editSolucion = $("#editSolucion").val();
					
					if(editSolucion == "") {
						$("#editSolucion").closest('.form-group').addClass('has-error');
						$("#editSolucion").after('<p class="text-danger">Campo necesario</p>');
					} else {
						$("#editSolucion").closest('.form-group').removeClass('has-error');
						$("#editSolucion").closest('.form-group').addClass('has-success');
					}					
				
				
					if(editSolucion){
						$.ajax({
							url:"crudKB.php?op=update",
							type: 'POST',
							data: form.serialize(),
							dataType: 'json',
							success:function(response) {
								if(response.success == true) {
									$(".edit-messages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
									'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									'<strong> <span class="fa fa-check"></span> </strong>'+response.messages+
									'</div>');

									// Recargar tabla
									tblKB.ajax.reload(null, false);
									$(".form-group").removeClass('has-success').removeClass('has-error');
									$(".text-danger").remove();
									
								} else {
									$(".edit-messages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
									'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									'<strong> <span class="fa fa-exclamation-triangle"></span> </strong>'+response.messages+
									'</div>')
								}
							} 
						}); 
					}
				
					return false;
				});
		
			} 
		}); 

	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}
*/

$('#idSubcategoria_').hide(); 
$("#regCategoria").change(function () {
	$("#regCategoria option:selected").each(function () {
		$('#idSubcategoria_').show(); 
		PK_idCategoria = $(this).val();
		if (PK_idCategoria == ""){
			$('#idSubcategoria_').hide(); 
		}
		$.post("views/ajax/categoriaAjax.php?op=getSubcategoria", { PK_idCategoria: PK_idCategoria }, function(data){
			$("#regSubcategoria").html(data);
		});
	});
});





$("#heredar").on('click', function() {
	var ref = $("#regID").val();
	$.ajax({
        url: "views/ajax/kbAjax.php?op=referencia",
		type: "POST",
        data: {idTicket: ref},
        dataType: "json",
        success: function(data) {
			//alert("ento");
			$("#idSubcategoria_").show();
			$("#regCategoria").val(data.PK_idCategoria);
			//$("#regSubcategoria").val(data.FK_idSubcategoria);
			$("#regCaso").val(data.tituloTicket);
			$("#regDescripcion").val(data.descripcionTicket);
			$.post("views/ajax/categoriaAjax.php?op=getSubcategoria", {PK_idCategoria: data.PK_idCategoria}, function(data){		
				$("#regSubcategoria").html(data);	
				$("#regSubcategoria").val(data.FK_idSubcategoria);
			});
		
        }       
    });
});


/*
$("#heredar").on('click', function() {
	var ref = $("#regReferencia").val();
	$.ajax({
        url: "views/ajax/ticketsAjax.php?op=heredar", 
		type: "POST",
        data: {idTicket: ref},
        dataType: "json",
        success: function(response) {
			if(!response || response.length == 0){
				$("#frmregistrarTicket")[0].reset();
				$("#referencia").hide();
				$("#idSubcategoria_").hide();
				
				errorSwal("Este ticket no se puede heredar, razones: \n Verificar que NO está CERRADO o CANCELADO. \n Verificar que NO sea un TICKET HIJO.");
			}else{
			$("#idSubcategoria_").show();
			$("#regCategoria").val(response.PK_idCategoria);
			$("#regTema").val(response.tituloTicket);
			$("#regTipo").val(response.tipoTicket);
			$("#regStatus").val(response.statusTicket);
			$("#regPrioridad").val(response.prioridadTicket);
			$.post("views/ajax/categoriaAjax.php?op=getSubcategoria", {PK_idCategoria: response.PK_idCategoria}, function(data){		
					$("#regSubcategoria").html(data);	
					$("#regSubcategoria").val(response.FK_idSubcategoria);
				});
			}
		}
	});
});
*/


/*
$('#idSubcategoria_').hide(); 
$("#regCategoria").change(function () {
	$("#regCategoria option:selected").each(function () {
		/**	Si la categoría cambia de ITEM, entonces el combo subcategoría se muestra en el formulario.	*
		$('#idSubcategoria_').show(); 
		
		PK_idCategoria = $(this).val();		
		/**	 Si en el combo “Categoría” se selecciona el valor 0, el combo subcategoría se oculta
		if (PK_idCategoria == ""){
			$('#idSubcategoria_').hide(); 
		}
		$.post("views/ajax/categoriaAjax.php?op=getSubcategoria", { PK_idCategoria: PK_idCategoria }, function(data){
			$("#regSubcategoria").html(data);
		});
	});
});
*/
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}
