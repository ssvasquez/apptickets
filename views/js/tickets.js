/*	=======	M O S T R A R Tickets	=======*/ 
var tblTickets;
tblTickets = $('#tickets').DataTable({
	responsive: true,
	"columns": [
		{	"orderable": false	},
		null,
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
	],
	"ajax": "views/ajax/ticketsAjax.php?op=mostrar",
	"order": [],
	"bInfo": false,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		}
	],

	/**	Función para filtrar el nombre del tecnico en la tabla Tickets.	**/
	initComplete: function () {
		tblTickets.columns([8]).every(function () {
			var column = this;
			var select = $('<select><option value="" >Default</option></select>').appendTo($(column.header()).empty()).on('change', function () {
					column.search(this.value).draw();
				});
			column.data().unique().sort().each(function (d) {
				select.append('<option value="' + d + '">' + d + '</option>')
			});
		});
	}
});


// Archivos
$("#archivosTicket").fileinput({
	theme:'fas',
	browseClass: "btn btn-secondary btn-block",
	allowedFileExtensions: ['PDF', 'JPG', 'PNG'],
	//maxFilesNum: 1,
	minFileCount: 1,
    maxFileCount: 3,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false,
	browseLabel:"Examinar Archivo(s)..."
});

function agregarArchivos(idTicket = null) {
	$("#frmRegistrarArchivos")[0].reset();
	if (idTicket) {

		$.ajax({
			url: "views/ajax/ticketsAjax.php?op=getID",
			type: 'POST',
			data: {
				idTicket: idTicket
			},
			dataType: 'json',
			success: function (response) {

				$("#idTicket").val(response.PK_idTicket);
				//$("#marcaProductoPDF").val(response.marca_prod);

				$("#frmRegistrarArchivos").unbind('submit').bind('submit', function () {

					//var formData = new FormData();

					var formData = new FormData(document.getElementById("frmRegistrarArchivos"));
					//formData = document.getElementById("frmRegistrarArchivos");

					var numArchivos = document.getElementById("archivosTicket").files.length;

					for (var index = 0; index < numArchivos; index++) {
						formData.append("archivosTicket[]", document.getElementById("archivosTicket").files[index]);
					}

					//var archivosTicket = $('#archivosTicket')[0].files[0];
					//formData.append('archivo[]', archivosTicket);
					$.ajax({
						url: "views/ajax/ticketsAjax.php?op=archivos",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						//cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblTickets.ajax.reload(null, false);
								$("#registrarArchivo").modal('hide');
								successSwal(response.mensaje);
							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#registrarArchivo").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}




/*	=======	A G R E G A R  Ticket	=======*/
/**	Función para agregar Ticket	**/

$("#btnAgregarTicket").click(function() {
	$('#referencia').hide(); 
	$("#frmregistrarTicket")[0].reset();
	$('#idSubcategoria_').hide(); 
});


$("#frmregistrarTicket").on("submit", function(e){
	$('#btnGuardar_').attr("disabled", true);
	e.preventDefault();
	var formData = new FormData(document.getElementById("frmregistrarTicket"));
	var ruta = "views/ajax/ticketsAjax.php?op=guardar";
	$.ajax({
		type: "POST",
		url: ruta,
		data: formData,
		contentType: false,
		processData: false,
		dataType : 'json',
		success: function(response)
		{
			if(response.success == true) {
				// Recargar la tabla, Limpiar los campos, Cerrar modal y Mostrar mensaje de éxito.
				tblTickets.ajax.reload();
				$("#frmregistrarTicket")[0].reset();
				$("#agregarTicket").modal('hide');
				successSwal(response.messages);
				$('#btnGuardar_').attr("disabled", false);
			} else {
				// Mostrar mensaje de error.
				$("#agregarTicket").modal('hide');
				errorSwal(response.messages);
				$('#btnGuardar_').attr("disabled", false);
			}
		}
	});
});


/*	=======	E D I T A R Ticket	=======*/
function editarTicket(idTicket) {
	if (idTicket) {		
		// Buscar datos del usuario
		$.ajax({
			url: "views/ajax/ticketsAjax.php?op=getID",
			type: 'POST',
			data: {
				idTicket: idTicket
			},
			dataType: 'json',
			success: function (response) {
				$("#editTema").val(response.tituloTicket);
				$("#editDescripcion").val(response.descripcionTicket);
				$("#editTipo").val(response.tipoTicket);
				$("#editEscalado").val(response.escaladoTicket);
				$("#editStatus").val(response.statusTicket);
				$("#editPrioridad").val(response.prioridadTicket);
				$("#editAsignado").val(response.PK_idUsuario);
				$("#editID").val(response.PK_idTicket);

				$("#frmEditarTicket").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarTicket"));
					
					$.ajax({
						url: "views/ajax/ticketsAjax.php?op=editar",
						type: 'POST',
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {
							if (response.success == true) {
								tblTickets.ajax.reload(null, false);
								$("#modalEditarTicket").modal('hide');
								successSwal(response.mensaje);
							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#modalEditarTicket").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
					return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}



/**	Función mostrar campo referencia dependiendo de la selección del <<select>> ¿Es hijo? .	**/
$(function() {
	$('#referencia').hide(); 
	$('#regEshijo').change(function(){
		if($('#regEshijo').val() == 1){
			$('#referencia').show(); 
		} else {
			$('#referencia').hide();
			$("#frmregistrarTicket")[0].reset();
			$('#idSubcategoria_').hide();
		//$('#regSubcategoria').hide();
		} 
	});
});


/**	Funcion para crear un evento	**/
function crearEvento(PK_idTicket = null) {
	if(PK_idTicket) {
		/**	Restaurar valores por default.	**/
		$('#btnGuardarEvento_').attr("disabled", false);
		$(".form-group").removeClass('has-error').removeClass('has-success');
		$(".text-danger").remove();
		$("#idTicket").remove();
		
		/**	Obtener los datos del Ticket.	**/
		$.ajax({
			url:"views/ajax/ticketsAjax.php?op=getID",
			type: 'POST',
			data: {idTicket : PK_idTicket},
			dataType: 'json',
			success:function(response) {
				$("#nxy").html("Ticket No "+response.PK_idTicket);
				$("#requerimiento").html("<div class='req'><span class='h6'><strong>"+response.tituloTicket+":" +"</strong></span>"+ "<span class='h6'> "+ response.descripcionTicket+"</span></div>" );
				$("#NoTicket").val(response.PK_idTicket);
				$("#eTema").val(response.tituloTicket);
				$("#eDescripcion").val(response.descripcionTicket);

				/**	ID Ticket [HIDDEN]	**/
				$(".registrarEvento").append('<input type="hidden" name="idTicket" id="idTicket" value="'+response.PK_idTicket+'"/>');
				$("#frmRegistrarEvento").unbind('submit').bind('submit', function(e){

					$('#btnGuardarEvento_').attr("disabled", true);
					e.preventDefault();
					var formData = new FormData(document.getElementById("frmRegistrarEvento"));
					var ruta = "views/ajax/ticketsAjax.php?op=crearEvento";
					$.ajax({
						type: "POST",
						url: ruta,
						data: formData,
						contentType: false,
						processData: false,
						dataType : 'json',
						success: function(response)
						{
						   if(response.success == true) {
							   $(".form-group").removeClass('has-success').removeClass('has-error');
								$(".text-danger").remove();	
								tblTickets.ajax.reload(null, false);
								$("#frmRegistrarEvento")[0].reset();
								$("#registrarEvento").modal('hide');
								successSwal(response.messages);
							} else {
								$("#registrarEvento").modal('hide');
								errorSwal(response.messages);
							}
						}
					});
				});
			} 
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/**	Función para seleccionar la SUBCATEGORIA correspondiente de una CATEGORIA
**	Indicar que el combo subcategoria NO este visible al cargar el formulario
**/
$('#idSubcategoria_').hide(); 
$("#regCategoria").change(function () {
	$("#regCategoria option:selected").each(function () {
		/**	Si la categoría cambia de ITEM, entonces el combo subcategoría se muestra en el formulario.	**/
		$('#idSubcategoria_').show(); 
		
		PK_idCategoria = $(this).val();		
		/**	 Si en el combo “Categoría” se selecciona el valor 0, el combo subcategoría se oculta.	**/
		if (PK_idCategoria == ""){
			$('#idSubcategoria_').hide(); 
		}
		$.post("views/ajax/categoriaAjax.php?op=getSubcategoria", { PK_idCategoria: PK_idCategoria }, function(data){
			$("#regSubcategoria").html(data);
		});
	});
});


/**	Indicar que el combo sucursal NO este visible al cargar el formulario.	**/
$('#idSucursal_').hide(); 
$("#regCliente").change(function () {
	$("#regCliente option:selected").each(function () {
		/**	Si la categoría cambia de ITEM, entonces el combo subcategoría se muestra en el formulario.	**/
		$('#idSucursal_').show(); 
		PK_idUsuario = $(this).val();		
		/**	Si en el combo “Cliente” se selecciona el valor 0, el combo sucursal se oculta.	**/
		if (PK_idUsuario == ""){
			$('#idSucursal_').hide(); 
		}
		$.post("views/ajax/ubicacionAjax.php?op=getUbicacion", { PK_idUsuario: PK_idUsuario }, function(data){
			$("#regSucursal").html(data);
		});
	});
});



/**
**	Heredar valores al crear un ticket hijo, 
**	SI un ticket ya es HIJO, ya NO se le puede crear un hijo o 
**	si un ticket está CERRADO [4] o CANCELADO [5] tampoco se puede crear hijos.
**/
$("#heredar").on('click', function() {
	var ref = $("#regReferencia").val();
	$.ajax({
        url: "views/ajax/ticketsAjax.php?op=heredar", 
		type: "POST",
        data: {idTicket: ref},
        dataType: "json",
        success: function(response) {
			if(!response || response.length == 0){
				$("#frmregistrarTicket")[0].reset();
				$("#referencia").hide();
				$("#idSubcategoria_").hide();
				
				errorSwal("Este ticket no se puede heredar, razones: \n Verificar que NO está CERRADO o CANCELADO. \n Verificar que NO sea un TICKET HIJO.");
			}else{
			$("#idSubcategoria_").show();
			$("#regCategoria").val(response.PK_idCategoria);
			$("#regTema").val(response.tituloTicket);
			$("#regTipo").val(response.tipoTicket);
			$("#regStatus").val(response.statusTicket);
			$("#regPrioridad").val(response.prioridadTicket);
			$.post("views/ajax/categoriaAjax.php?op=getSubcategoria", {PK_idCategoria: response.PK_idCategoria}, function(data){		
					$("#regSubcategoria").html(data);	
					$("#regSubcategoria").val(response.FK_idSubcategoria);
				});
			}
		}
	});
});



function resueltoTicket(PK_idTicket = null){
	if(PK_idTicket){
		swal({
			title: "¿Realmente desea cambiar a estatus RESUELTO este ticket?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ticketsAjax.php?op=resolverTicket",
					type: 'post',
					data: {idTicket : PK_idTicket},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblTickets.ajax.reload(null, false);
							successSwal(response.messages);
						} else {
							errorSwal(response.messages);
						}
					}
				});
			} 
		});
	}
}


function cerrarTicket(PK_idTicket = null){
	if(PK_idTicket){
		swal({
			title: "¿Realmente desea cerrar este ticket?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ticketsAjax.php?op=cerrarTicket",
					type: 'post',
					data: {idTicket : PK_idTicket},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblTickets.ajax.reload(null, false);
							successSwal(response.messages);
						} else {
							errorSwal(response.messages);
						}
					}
				});
			} 
		});
	}
}


function cancelarTicket(PK_idTicket = null){
	if(PK_idTicket){
		swal({
			title: "¿Realmente desea cancelar este ticket?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ticketsAjax.php?op=cancelarTicket",
					type: 'post',
					data: {idTicket : PK_idTicket},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblTickets.ajax.reload(null, false);
							successSwal(response.messages);
						} else {
							errorSwal(response.messages);
						}
					}
				});
			} 
		});
	}
}


function reabrirTicket(PK_idTicket = null){
	if(PK_idTicket){
		swal({
			title: "¿Realmente desea reabrir este ticket?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ticketsAjax.php?op=reabrirTicket",
					type: 'post',
					data: {idTicket : PK_idTicket},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblTickets.ajax.reload(null, false);
							successSwal(response.messages);
						} else {
							errorSwal(response.messages);
						}
					}
				});
			} 
		});
	}
}

/** Mensajes de Acierto y Error. Aplica area de Categoria y Subcategoria */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}