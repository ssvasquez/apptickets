<?php 
if(!isset($_SESSION)){
session_start();
}
if(!$_SESSION["validar"]){
	//header("location:ingreso");
	echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
	exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sistema de Tickets - Usuarios </title>
	<!--<link href="views/css/font-awesome.min.css" rel="stylesheet">-->
	<link rel="stylesheet" href="views/css/all.min.css">
	<link href="views/css/bootstrap.css" rel="stylesheet">
	<link href="views/css/sweetalert.css" rel="stylesheet">
	<link href="views/css/style.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed">
	<?php include "views/modulos/cabezote.php"; ?>
	<div class="app-body">
		<?php include "views/modulos/botonera.php"; ?>
		
		<main class="main">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6">
						<div class="card">
							<div class="card-header">
								<i class="fa fa-user"></i> Mi perfil
							</div>
								
							<div class="card-block">
								<div class="card car">
									<div class="card-header card-header-inverse card-header-info">
										<div class="font-weight-bold">
											<span>NOMBRE</span>
											<p>
												<small class="text-uppercase"><?php echo $_SESSION['nombre'].' '.$_SESSION['apellidos']?></small>
											</p>
										</div>
											
										<div class="font-weight-bold">
											<span class="">Tickets [Abiertos, Asignados, Pendientes y Reabiertos]</span>
											<h3 class="float-right">
												<?php //echo $numTicks; ?>
											</h3>

										</div>
									</div>
									<div class="card-block py-0 px-2 b-t-1">
										<div class="row">
											<div class="col-4 b-r-1 py-1">
												<div class="font-weight-bold"><?php //echo //$numAltas;?></div>
												<div class="text-muted">
													<small>Creados</small>
												</div>
											</div>
											<div class="col-4 py-1">
												<div class="font-weight-bold"><?php //echo $numResueltos;?></div>
												<div class="text-muted">
													<small>Resueltos</small>
												</div>
											</div>
											<div class="col-4 b-r-1 py-1">
												<div class="font-weight-bold"><?php //echo $numCerrados;?></div>
												<div class="text-muted">
													<small>Cerrados</small>
												</div>
											</div>
										</div>
										<br>
										<br>
										<br>
										<div class="font-weight-bold">
											<span>ROL</span>
											<?php $rol = $_SESSION['rol'];
												switch($rol){
													case 1:
														$rol="ADMINISTRADOR";
													break;
													
													case 2:
														$rol="TÉCNICO";
													break;
													
													case 3:
														$rol="USUARIO";
													break;
												}
											?>
											<p>
												<small class=""><?php echo $rol ?></small>
											</p>
										</div>
										<div class="font-weight-bold">
											<span>NOMBRE USUARIO</span>
											<p>
												<small class=""><?php echo $_SESSION['usuario']?></small>
											</p>
										</div>
										<div class="font-weight-bold">
											<span>TELEFONO</span>
											<p>
												<small class=""><?php echo $_SESSION['telefono']?></small>
											</p>
										</div>
										<div class="pull-right">
											<a href="#" id="zyx" data-toggle="modal" data-target="#cambiarContrasena" title="Cambiar contraseña">Cambiar contraseña	</a>
											<br>
										</div>
									</div>
									<br>
									<br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>

		<!--Modal cambio de contraseña -->
		<div class="modal fade" id="cambiarContrasena" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-info" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-exchange"></i> Cambiar contraseña</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="" method="post" id="frmcambioContrasena" enctype="multipart/form-data">
						<div class="card-group">
							<div class="card-block" id="">
								<div class=" alert alert-warning text-muted"><i>* Para cambiar su contraseña ingrese su contraseña actual, después su nueva contraseña y al último confirme su nueva contraseña.</i></div>
								<div class="input-group mb-1">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" id="pswdActual" name ="pswdActual" class="form-control" placeholder="Contraseña Actual" required>
								</div>
								<div class="input-group mb-1">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" id="pswdNueva" name="pswdNueva" class="form-control" placeholder="Nueva contraseña" required>
								</div>
								<div class="input-group mb-1">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" id="pswdConfirmarNueva" name="pswdConfirmarNueva" class="form-control" placeholder="Confirmar nueva contraseña" required>
								</div>
								<div class="modal-footer">
									<button type="reset" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
									<button type="submit" class="btn btn-primary" id="cambiarPsw">
										<span class="fa fa-sign-in"></span> &nbsp; Aceptar
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div><!--Fin Modal cambio de contraseña -->
	</div>

	<?php include "views/modulos/footer.php"; ?>

	<script src="views/js/libs/jquery.min.js"></script>
	<script src="views/js/libs/tether.min.js"></script>
	<script src="views/js/libs/bootstrap.min.js"></script>
	<script src="views/js/libs/sweetalert.min.js"></script>	
	<script src="views/js/app.js"></script>
	<script src="views/js/contrasena.js"></script> 
</body>
</html>