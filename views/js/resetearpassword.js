//Variable para manipular la tabla de Usuario
var tblSistemas;
$(document).ready(function() {
	//Lenguaje para los parametros de datatables.
	tblSistemas = $("#sistemas").DataTable({
		responsive: true,
		"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
		"ajax": "views/ajax/usuarioAjax.php?op=sistemas",
		"order": [],
		"columns": [
			{"orderable": false },
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},	
			{"orderable": false}	
		],		
		"language":
		{
			"url": "views/js/json/Spanish.json"
		},
		columnDefs: [
			{ responsivePriority: 1, targets: 0 }
		]
	});	
});


//Reset password usuario.

function resetearPassword(PK_idUsuario = null){
	if(PK_idUsuario){
		swal({
			title: "¿Realmente desea resetear la contraseña de este usuario?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/usuarioAjax.php?op=contrasena",
					type: 'post',
					data: {idUsuario : PK_idUsuario},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblSistemas.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/** Mensajes de Acierto y Error. Aplica area de Categoria y Subcategoria */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}

/*
function resetearPassword(PK_idUsuario = null) {
	if(PK_idUsuario) {
		$("#btnResetear").unbind('click').bind('click', function() {
			$.ajax({
				url:"views/ajax/usuarioAjax.php?op=contrasena",
				type: 'post',
				data: {idUsuario : PK_idUsuario},
				dataType: 'json',
				success:function(response) {
					if(response.success == true) {
						$(".removeMessages").html('<div class="alert alert-success alert-dismissible" role="alert">'+
							  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
							  '<strong> <span class="fa fa-check"></span> </strong>'+response.messages+
							'</div>');

						// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
						tblUsuario.ajax.reload(null, false);
						$("#modalresetPassword").modal('hide');

					} else {
						$(".removeMessages").html('<div class="alert alert-warning alert-dismissible" role="alert">'+
							  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
							  '<strong> <span class="fa fa-exclamation-triangle"></span> </strong>'+response.messages+
							'</div>');
					}
				}
			});
		});
	} else {
		alert('Error: Recarga la página de nuevo');
	}
}
*/