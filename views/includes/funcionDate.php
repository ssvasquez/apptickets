<?php
class Cokidoo_DateTime extends DateTime {
    
    protected $strings = array(
        'y' => array('Hace 1 año', 'Hace %d años'),
        'm' => array('Hace 1 mes', 'Hace %d meses'),
        'd' => array('Hace 1 día', 'Hace %d dias'),
        'h' => array('Hace 1 hora', 'Hace %d horas'),
        'i' => array('Hace 1 minuto', 'Hace %d minutos'),
        's' => array('Ahora', 'Hace %d segundos'),
    );
    
    /**
     * Devuelve la diferencia de la fecha actual en el formato Hace X tiempo
     * @return string
     */
    public function __toString() {
        $now = new DateTime('now');
        $diff = $this->diff($now);
        
        foreach($this->strings as $key => $value){
            if( ($text = $this->getDiffText($key, $diff)) ){
                return $text;
            }
        }
        return '';
    }
    
    /**
	 * Construir el texto de diferencia de tiempo con la clave de intervalo especificada
     * @param string $intervalKey un valor de: [y,m,d,h,i,s]
     * @param DateInterval $diff
     * @return string|null
     */
    protected function getDiffText($intervalKey, $diff){
        $pluralKey = 1;
        $value = $diff->$intervalKey;
        if($value > 0){
            if($value < 2){
                $pluralKey = 0;
            }
            return sprintf($this->strings[$intervalKey][$pluralKey], $value);
        }
        return null;
    }
}
?>
