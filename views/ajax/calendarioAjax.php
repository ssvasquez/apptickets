<?php
    require_once "../../controllers/calendarioControllers.php";
    require_once "../../models/calendarioModels.php";

    //$opcion = isset($_GET['op'])?$_GET['op']:'';

    class AjaxCalendario{

        public function calendarioAjax(){

            $respuesta = calendarioController::mostrarCalendarioController();

            $data = array();

            foreach ($respuesta as $row => $item){

                $data[] = array(
                    'id'    => $item["PK_idTicket"],
                    'title' => $item["tituloTicket"],
                    'start' => $item["creado"]
                );
            }

            print json_encode($data, JSON_NUMERIC_CHECK);
        }
    }

    // Imprimir objetos.
    $calendario = new AjaxCalendario();
    $calendario -> calendarioAjax();
    
?>