<?php 

require_once "db.php";

class KBModel{

	public static function mostrarKBModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT kb.PK_idKB, kb.FK_idTicket, kb.FK_subcategoriaKB, kb.casoKB, kb.solucionKB, kb.archivo, kb.link, subcategoria.subcategoriaDesc, categoria.categoriaDesc FROM $tabla INNER JOIN subcategoria ON subcategoria.PK_idSubcategoria = kb.FK_subcategoriaKB INNER JOIN categoria on categoria.PK_idCategoria = subcategoria.FK_idCategoria WHERE kb.estatus = 1");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}

	public static function referenciaModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT ticket.PK_idTicket, ticket.esHijo, ticket.tituloTicket, ticket.descripcionTicket, ticket.statusTicket, ticket.prioridadTicket, ticket.escaladoTicket, ticket.refTicket, ticket.tipoTicket, ticket.FK_idSubcategoria, ticket.FK_idUbicacion, subcategoria.PK_idSubcategoria, subcategoria.subcategoriaDesc, categoria.PK_idCategoria, categoria.categoriaDesc, ubicacion.PK_idUbicacion, ubicacion.ubicacionDesc FROM $tabla INNER JOIN subcategoria ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria INNER JOIN ubicacion ON ticket.FK_idUbicacion = ubicacion.PK_idUbicacion INNER JOIN categoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria WHERE PK_idTicket = :idTicket");

		$stmt -> bindParam(":idTicket", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();
	}


	public static function getIDModel($tabla, $id){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE kb.PK_idKB = :idKB");

		$stmt -> bindParam(":idKB", $id, PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();
	}

	// Guardar nuevo ,,,
	public static function guardarKBModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (FK_idTicket, FK_subcategoriaKB, casoKB, solucionKB, link, estatus)VALUES(:idTicket, :subcategoriaKB, :casoKB, :solucionKB, :link, 1)");
		
		$stmt -> bindParam(":idTicket", $datosModel["idTicket"], PDO::PARAM_INT);
		$stmt -> bindParam(":subcategoriaKB", $datosModel["subcategoriaKB"], PDO::PARAM_INT);
		$stmt -> bindParam(":casoKB", $datosModel["casoKB"], PDO::PARAM_STR);
		$stmt -> bindParam(":solucionKB", $datosModel["solucionKB"], PDO::PARAM_STR);
		$stmt -> bindParam(":link", $datosModel["link"], PDO::PARAM_STR);
		//$stmt -> bindParam(":estatus", 1, PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}

	// Editar KB
	public static function editarKBModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET solucionKB = :solucion WHERE kb.PK_idKB = :idKB ");	

		$stmt -> bindParam(":idKB", $datosModel["id"], PDO::PARAM_INT);

		$stmt -> bindParam(":solucion", $datosModel["solucion"], PDO::PARAM_STR);
		
		return $stmt->execute();

		$stmt->close();
	}


	public static function desactivarKBModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estatus = 0 WHERE PK_idKB = :id");

		$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

		return $stmt->execute();

		$stmt->close();
	}

	public static function guardarPDF($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET archivo = :archivo WHERE kb.PK_idKB = :idKB");

		$stmt->bindParam(":idKB", $datosModel['id'], PDO::PARAM_INT);
		$stmt->bindParam(":archivo", $datosModel['archivo'], PDO::PARAM_STR);

		return $stmt->execute();

		$stmt->close();
	}
	
}