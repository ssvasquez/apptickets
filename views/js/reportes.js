$('.input-daterange').datepicker({
	todayBtn:'linked',
	format: "yyyy-mm-dd",
	autoclose: true
});



$('#search').click(function(){
	let start_date = $('#start_date').val();
	let end_date = $('#end_date').val();
	$.ajax({
		type: 'POST', 
		url: 'views/ajax/reportesAjax.php?op=reporte1', 
		data:{	start_date:start_date, 
				end_date:end_date
		}
	}).done(function(data) {

		$('.response').html(data);

	});
});


$('#search1').click(function(){
	var start_date = $('#start_date').val();
	var end_date = $('#end_date').val(); 
	$.ajax({
		type: 'POST', 
		url: 'views/ajax/reportesAjax.php?op=reporte2', 
		data:{start_date:start_date, end_date:end_date}
	}).done(function(data) {          
		$('.response').html(data);          
	});  
});


$('#reportesData').DataTable({
	dom: 'Bfrtip',
	"language": 
	{
		"url": "views/js/json/Spanish.json"
	},
	buttons: [
		{
			extend: 'pdfHtml5',
			orientation: 'landscape',
			pageSize: 'LEGAL',
			title: 'Reportes'
		},
		{
			extend: 'excelHtml5',
			title: 'Reportes'
		},
		{
			extend: 'copy',
			text: 'Copiar'
		}
	]
});
