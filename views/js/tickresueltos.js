
/*	=======	M O S T R A R Tickets Resueltos	=======*/
var tblTicketResuelto;
$(document).ready(function() {
	tblTicketResuelto = $('#tickresueltos').DataTable({
		//"ordering": false,
		responsive: true,
		"columns": [
			{"orderable": false },
			null,
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false},
			{"orderable": false}
		],
		"ajax": "views/ajax/ticketsAjax.php?op=resueltos",
		"order": [],
		"bInfo": false,
		"language": {
			"url": "views/js/json/Spanish.json"
		},
		columnDefs: [
			{
				responsivePriority: 1,
				targets: 0
			}
		],

		/**	Función para filtrar el nombre del tecnico en la tabla Tickets.	**/
		initComplete: function () {
			tblTicketResuelto.columns([8]).every(function () {
				var column = this;
				var select = $('<select><option value="" >Default</option></select>')
					.appendTo($(column.header()).empty())
					.on('change', function () {
						column
							.search(this.value)
							.draw();
					});
				column.data().unique().sort().each(function (d) {
					select.append('<option value="' + d + '">' + d + '</option>')
				});
			});
		}
	});
});


function cerrarTicket(PK_idTicket = null){
	if(PK_idTicket){
		swal({
			title: "¿Realmente desea cerrar este ticket?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ticketsAjax.php?op=cerrarTicket",
					type: 'post',
					data: {idTicket : PK_idTicket},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblTicketResuelto.ajax.reload(null, false);
							successSwal(response.messages);
						} else {
							errorSwal(response.messages);
						}
					}
				});
			} 
		});
	}
}


function reabrirTicket(PK_idTicket = null){
	if(PK_idTicket){
		swal({
			title: "¿Realmente desea reabrir este ticket?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/ticketsAjax.php?op=reabrirTicket",
					type: 'post',
					data: {idTicket : PK_idTicket},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblTicketResuelto.ajax.reload(null, false);
							successSwal(response.messages);
						} else {
							errorSwal(response.messages);
						}
					}
				});
			} 
		});
	}
}

function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}