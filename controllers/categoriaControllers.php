<?php 

class CategoriaController{

	public static function mostrarCategoriaController(){

		$respuesta = CategoriasModel::mostrarCategoriaTodosModel("categoria");
		
		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['activo'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editar_categoria" onclick="editarCategoria('.$item['PK_idCategoria'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modaldesactivarCategoria" onclick="desactivarCategoria('.$item['PK_idCategoria'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
            }else{
				
				$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalactivarCategoria" onclick="activarCategoria('.$item['PK_idCategoria'].')"> <span class="fa fa-toggle-off"></span> A c t i v a r</button>';
			}
			
			$datos['data'][] = array(
				$i, 
				$item['categoriaDesc'],
				$eventos
			);

			$i++;
		}
		
		//Mandar un array a AJAX;
		return $datos;
    }


    public static function mostrarCategoriaIDController(){

        $idCategoria = $_POST['idCategoria'];

        $respuesta = CategoriasModel::mostrarIDCategoriaModel('categoria', $idCategoria);

        return $respuesta;
    }


	public static function guardarCategoriaController(){

		if(isset($_POST["altaCategoria"])){

			$datosController = array("categoria"=>$_POST["altaCategoria"],
								"activo"=> '1' );

			$respuesta = CategoriasModel::guardarCategoriaModel($datosController, "categoria");
		
			return $respuesta;

		}		
    }


	public static function editarCategoriaController(){

		$datosController = array("id" => $_POST["idCategoriaEditar"],
								"categoria" => $_POST["categoriaEditar"]);

		$respuesta = CategoriasModel::editarCategoriaModel($datosController, "categoria");

		return $respuesta;
	}

	public static function desactivarCategoriaController(){
        //if(isset($_GET["idDesactivar"])){
			
            $datosController = array("id" => $_POST['idCategoria']);

            $respuesta = CategoriasModel::desactivarCategoriaModel($datosController, "categoria");

			return $respuesta; 

        //}

    }
	
	
    public static function tieneSubcategoriasController(){

		$datosController = array("id" => $_POST['idCategoria']);

		$respuesta = CategoriasModel::tieneSubcategoriaModel($datosController, "subcategoria");

		return ($respuesta);

	}
	

	public static function activarCategoriaController(){

		$datosController = array("id" => $_POST['idCategoria']);

		$respuesta = CategoriasModel::activarCategoriaModel($datosController, "categoria");

		return $respuesta;
	}


	#   Mostrar categorias en un input type select <option>
    public static function categoriaSeleccionController(){

        $respuesta = CategoriasModel::mostrarCategoriaModel("categoria");

        foreach ($respuesta as $row => $item){

            echo '<option value="'.$item["PK_idCategoria"].'">'.$item["categoriaDesc"].'</option>';

        }

    }


    /*  =======================================================  
        Area para S U B C A T E G O R I A
    =====================================================    */

    #   funcion para guardar una nueva subcategoria.
    public static function guardarSubcategoriaController(){
        
        if(isset($_POST["altaSubcategoria"])){

            $datosController = array("id_categoria" => $_POST["deCategoria"],
									 "subcategoria" => $_POST["altaSubcategoria"],
									 "activo" => '1');
            
            $respuesta = CategoriasModel::guardarSubcategoriaModel($datosController, "subcategoria");

            return $respuesta;
        }
    }



    public static function mostrarSubcategoriaController(){

        $respuesta = CategoriasModel::mostrarSubcategoriaModel();
		$i = 1;
		
		$datos = array('data' => array());

        foreach ($respuesta as $row => $item){

			if($item['activo'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editar_subcategoria" onclick="editarSubcategoria('.$item['PK_idSubcategoria'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="desactivarSubcategoria('.$item['PK_idSubcategoria'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
            }else{
				
				$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalactivarSubcategoria" onclick="activarSubcategoria('.$item['PK_idSubcategoria'].')"> <span class="fa fa-toggle-off"></span> A c t i v a r</button>';
			}
			
			$datos['data'][] = array(
				$i, 
				$item['categoriaDesc'],
				$item["subcategoriaDesc"],
				$eventos
			);

			$i++;
		}
		//Mandar un array a AJAX;
		return $datos;
    }


	public static function mostrarSubcategoriaIDController(){

        $idSubcategoria = $_POST['idSubcategoria'];

        $respuesta = CategoriasModel::mostrarIDSubcategoriaModel('subcategoria', $idSubcategoria);

        return $respuesta;
	}
	
	public static function editarSubcategoriaController(){

		$datosController = array("id" => $_POST["idSubcategoriaEditar"],
								"idCategoria" => $_POST["categoriaSubEditar"],
								"subcategoria" => $_POST["subcategoriaEditar"]);

		$respuesta = CategoriasModel::editarSubcategoriaModel($datosController, "subcategoria");

		return $respuesta;
	}

    public static function desactivarSubcategoriaController(){
        //if(isset($_GET["idDesactivar"])){					
			
            $datosController = array("id" => $_POST['idSubcategoria']);

            $respuesta = CategoriasModel::desactivarSubcategoriaModel($datosController, "subcategoria");

            return $respuesta;

        //}

	}
	
	
	public static function activaCategoriaController(){
        //if(isset($_GET["idDesactivar"])){			
            $datosController = array("id" => $_POST['idSubcategoria']);

            $respuesta = CategoriasModel::activaCategoriaModel($datosController, "subcategoria");

            return $respuesta;

        //}

	}
	

	public static function activarSubcategoriaController(){
        //if(isset($_GET["idDesactivar"])){					
			
            $datosController = array("id" => $_POST['idSubcategoria']);

            $respuesta = CategoriasModel::activarSubcategoriaModel($datosController, "subcategoria");

            return $respuesta;

        //}
	}
	

	public static function subcategoriaSeleccionController(){

        $respuesta = CategoriasModel::mostrarListaSubcategoriaModel("subcategoria", $_POST['PK_idCategoria']);

		$html = "<option value=''>Seleccione...</option>";

        foreach ($respuesta as $row => $item){

            $html .= '<option value="'.$item["PK_idSubcategoria"].'">'.$item["subcategoriaDesc"].'</option>';

		}		
		echo $html;
    }
}