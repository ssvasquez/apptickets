<?php

	require_once "db.php";

	class GraficaModels{

		//  Contar Registros de una Tabla PDO
		//  ticket, categoria, usuario, evento
		public static function ContarRegistrosTabla($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> rowCount();

			$stmt -> close();
		}


		// Tickets por AÑO
		public static function ContarTicketsPorAnio($mes, $tabla){

            $anio=  date("Y");
			$stmt = Conexion::conectar()->prepare("SELECT COUNT(*) FROM $tabla WHERE MONTH(creado) = :mes AND YEAR(creado) = :anio");
		
			$stmt -> bindParam("mes", $mes, PDO::PARAM_INT);
			$stmt -> bindParam("anio",  $anio, PDO::PARAM_INT);
			
			$stmt -> execute();

			return $stmt -> fetchColumn();

			$stmt -> close();

		}

        // Tickets por Status
        public static function ContarTicketsEstatus($estatus, $tabla){

            $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) FROM $tabla WHERE statusTicket = :estatus");
		
            $stmt -> bindParam("estatus", $estatus, PDO::PARAM_INT);
           
            $stmt -> execute();
            
            return $stmt -> fetchColumn();

		    $stmt -> close();
             
        }

        // Prioridad de Tickets
        public static function ContarTicketsPrioridad($prioridad, $tabla){

            $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) FROM $tabla WHERE prioridadTicket = :prioridad AND statusTicket <> 4 AND statusTicket <> 5 AND statusTicket <> 6");
		
            $stmt -> bindParam("prioridad", $prioridad, PDO::PARAM_INT);
           
            $stmt -> execute();
            
            return $stmt -> fetchColumn();

            $stmt -> close();
        }


        public static function ContarTicketsTipo($tipo, $tabla){

            $stmt = Conexion::conectar()->prepare("SELECT COUNT(*) FROM $tabla WHERE tipoTicket = :tipo AND statusTicket <> 4 AND statusTicket <> 5 AND statusTicket <> 6");
		
            $stmt -> bindParam("tipo", $tipo, PDO::PARAM_INT);
           
            $stmt -> execute();
            
            return $stmt -> fetchColumn();

            $stmt -> close();
        }


        public static function ContarTicketsSubcategoria(){

            $stmt = Conexion::conectar()->prepare("SELECT categoria.categoriaDesc, COUNT(*) AS CantidadTickes FROM categoria INNER JOIN subcategoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria INNER JOIN ticket ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria WHERE ticket.statusTicket <> 4 AND ticket.statusTicket <> 5 AND ticket.statusTicket <> 6 GROUP BY categoria.PK_idCategoria");

            $stmt -> execute();
            
            return $stmt -> fetchAll();

            $stmt -> close();
        }

         
        public static function estadisticasTecnicoModel(){

            $stmt = Conexion::conectar()->prepare('CALL estadisticasTecnico');
		
		    $stmt -> execute();
            
            return $stmt -> fetchAll();

		    $stmt -> close();
        }


        public static function atendidosTecnicoModel($id){

            $stmt = Conexion::conectar()->prepare("SELECT COUNT(ticket.PK_idTicket) as Total FROM usuario INNER JOIN ticket_usuario ON ticket_usuario.FK_idUsuario = usuario.PK_idUsuario INNER JOIN ticket ON ticket_usuario.FK_idTicket = ticket.PK_idTicket WHERE (ticket.statusTicket = 4 OR ticket.statusTicket = 5 OR ticket.statusTicket = 6) AND usuario.PK_idUsuario = $id AND status = 'ATENCION'");
		
		    $stmt -> execute();
            
            return $stmt -> fetchColumn();

            $stmt -> close();
        }


        // Tablas Primeros 5 registros.
        public static function ticketsPorUsuario(){

            $stmt = Conexion::conectar()->prepare("SELECT nombre, apellidos, count(ticket_usuario.FK_idTicket) AS Total FROM usuario INNER JOIN ticket_usuario ON ticket_usuario.FK_idUsuario = usuario.PK_idUsuario WHERE ticket_usuario.status = 'CLIENTE' GROUP BY usuario.PK_idUsuario ORDER BY Total DESC LIMIT 5");
		
            $stmt -> execute();
            
            return $stmt -> fetchAll();

            $stmt -> close();
        }


        public static function ticketsPorSucursal(){
            $stmt = Conexion::conectar()->prepare("SELECT ubicacion.ubicacionDesc, COUNT(*) As Total FROM ubicacion INNER JOIN usuario on usuario.FK_idUbicacion = ubicacion.PK_idUbicacion INNER JOIN ticket_usuario on ticket_usuario.FK_idUsuario = usuario.PK_idUsuario WHERE ticket_usuario.status = 'CLIENTE' GROUP BY ubicacion.PK_idUbicacion ORDER BY Total DESC LIMIT 5");
		
            $stmt -> execute();
            
            return $stmt -> fetchAll();

            $stmt -> close();
            
        }


        public static function ticketsPorCategoria(){
            $stmt = Conexion::conectar()->prepare("SELECT categoria.categoriaDesc, COUNT(*) AS CantidadTickes FROM categoria INNER JOIN subcategoria ON subcategoria.FK_idCategoria = categoria.PK_idCategoria INNER JOIN ticket ON ticket.FK_idSubcategoria = subcategoria.PK_idSubcategoria GROUP BY categoria.PK_idCategoria ORDER BY CantidadTickes DESC LIMIT 5");
		
            $stmt -> execute();
            
            return $stmt -> fetchAll();

            $stmt -> close();
            
        }
    }
?>